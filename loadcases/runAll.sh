echo buoy
cd buoy
../../bin/OCN-SIM-FLEX example.conf
cd ..


echo catenary
cd catenary
../../bin/OCN-SIM-FLEX example.conf
cd ..


echo example
cd example
../../bin/OCN-SIM-FLEX example.conf
cd ..

echo fourPointMooring
cd fourPointMooring
../../bin/OCN-SIM-FLEX example.conf
cd ..

echo otterBoard
cd otterBoard
../../bin/OCN-SIM-FLEX example.conf
cd ..


echo pendulum
cd pendulum
../../bin/OCN-SIM-FLEX example.conf
cd ..

echo rawData__rhombicNetting
cd rawData__rhombicNetting
../../bin/OCN-SIM-FLEX example.conf
cd ..
