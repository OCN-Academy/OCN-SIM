################################################################################
# GNU makefile for the whole project
################################################################################


# --- Configuration ---
# Operating system dependent variables
#osVar = ./src/linux/osVar.f03
osVar = ./src/win/osVar.f03

# Use lapack from source or link against precompiled library. If deactivated,
# requirers Lapack to be compiled and setup to be found by fortran, so that the
# compiler flag '-llapack' is going to work properly.
#	1: compile from source,
# 	0: use precompiled installation of lapack
laSRC = 1


# --- Compiler and flags ---
gfort   = gfortran -cpp -c -O0 -g -Wall -Wimplicit-procedure -J ./bin/
gfortla = gfortran -c -O0 -g -Wall -Wimplicit-procedure -J ./bin/lapack/
gfortbl = gfortran -c -O0 -g -Wall -Wimplicit-procedure -J ./bin/blas/


# --- Source files ---
# Main files
objects	= ./bin/osVar.o \
		  ./bin/liboe__Conf.o ./bin/liboe__Types.o ./bin/liboe__Tools.o ./bin/liboe__File.o ./bin/liboe__Err.o  \
		  ./bin/flm__Types.o  ./bin/flm__Data.o    ./bin/flm__Dyn.o \
		  ./bin/mbs__Types.o  ./bin/mbs__Tools.o   ./bin/mbs__Data.o    ./bin/mbs__Dyn.o \
		  ./bin/rdr__RopeSys.o \
		  ./bin/sol__RRF.o    ./bin/sol__JAC.o     ./bin/sol__LAGR.o\
		  ./bin/sys__Data.o   ./bin/sys__Tools.o   ./bin/sys__Dyn.o \
		  ./bin/main.o

# Lapack
objLapack = ./bin/lapack/dgesv.o  ./bin/lapack/dgetf2.o ./bin/lapack/dgetrf.o ./bin/lapack/dgetrs.o \
			./bin/lapack/dlamch.o ./bin/lapack/dlaswp.o ./bin/lapack/ieeeck.o ./bin/lapack/ilaenv.o\
			./bin/lapack/iparmq.o ./bin/lapack/lsame.o  ./bin/lapack/xerbla.o ./bin/lapack/dgetrf2.o\
			./bin/lapack/dgels.o ./bin/lapack/dlaset.o ./bin/lapack/dlabad.o ./bin/lapack/dlange.o\
			./bin/lapack/dlascl.o ./bin/lapack/dgeqrf.o ./bin/lapack/dormqr.o ./bin/lapack/dtrtrs.o\
			./bin/lapack/dgelqf.o ./bin/lapack/dormlq.o ./bin/lapack/disnan.o ./bin/lapack/dgeqr2.o\
			./bin/lapack/dlarft.o ./bin/lapack/dlarfb.o ./bin/lapack/dlassq.o ./bin/lapack/dorm2r.o\
			./bin/lapack/dgelq2.o ./bin/lapack/dorml2.o ./bin/lapack/dlaisnan.o ./bin/lapack/dlarfg.o\
			./bin/lapack/dlarf.o ./bin/lapack/dlapy2.o ./bin/lapack/iladlc.o ./bin/lapack/iladlr.o

# BLAS
objBlas = ./bin/blas/dswap.o ./bin/blas/idamax.o ./bin/blas/dscal.o ./bin/blas/dger.o \
		  ./bin/blas/dtrsm.o ./bin/blas/dgemm.o ./bin/blas/dgemv.o ./bin/blas/dtrmv.o\
		  ./bin/blas/dcopy.o ./bin/blas/dtrmm.o ./bin/blas/dnrm2.o 


# --- Main executable ---

ifeq ($(laSRC),1)

# Main executable, lapack and blas from source
./bin/OCN-SIM-FLEX: $(objects) $(objLapack) $(objBlas)
	gfortran -o ./bin/OCN-SIM-FLEX $(objects) $(objLapack) $(objBlas)

else

# Main executable, externally linked lapack and blas
./bin/OCN-SIM-FLEX: $(objects)
	gfortran -o ./bin/OCN-SIM-FLEX $(objects) -llapack

endif

# --- Generic rules ---
# General rule for compiling object files
./bin/%.o: ./src/%.f03
	mkdir -p $(@D)
	$(gfort) -o $@ $<

# General rule for compiling lapack
./bin/lapack/%.o: ./src/lapack/%.f
	mkdir -p $(@D)
	$(gfortla) -o $@ $<

# General rule for compiling lapack
./bin/blas/%.o: ./src/blas/%.f
	mkdir -p $(@D)
	$(gfortbl) -o $@ $<


# --- Dependency rules ---
./bin/liboe__Conf.o:  ./bin/osVar.o
./bin/liboe__Types.o: ./bin/liboe__Conf.o
./bin/liboe__Err.o:   ./bin/liboe__Types.o
./bin/liboe__Tools.o: ./bin/liboe__Err.o
./bin/liboe__File.o:  ./bin/liboe__Types.o


./bin/flm__Types.o:   ./bin/liboe__Conf.o
./bin/flm__Data.o:	  ./bin/flm__Types.o ./bin/liboe__File.o ./bin/mbs__Data.o
./bin/flm__Dyn.o:     ./bin/flm__Data.o ./bin/sys__Data.o

./bin/mbs__Types.o:	  ./bin/liboe__Conf.o
./bin/mbs__Tools.o:   ./bin/liboe__Types.o
./bin/mbs__Data.o:    ./bin/mbs__Types.o ./bin/mbs__Tools.o ./bin/liboe__File.o
./bin/mbs__Dyn.o:	  ./bin/mbs__Tools.o ./bin/mbs__Types.o

./bin/sol__RRF.o:	  ./bin/mbs__Types.o
./bin/sol__JAC.o:	  ./bin/mbs__Dyn.o
./bin/sol__LAGR.o:	  ./bin/mbs__Types.o

./bin/sys__Dyn.o: 	  ./bin/mbs__Dyn.o ./bin/mbs__Data.o ./bin/sol__RRF.o \
					  ./bin/sol__JAC.o ./bin/sol__LAGR.o ./bin/flm__Dyn.o
./bin/sys__Tools.o:	  ./bin/mbs__Types.o ./bin/liboe__Types.o
./bin/sys__Data.o:	  ./bin/mbs__Data.o ./bin/flm__Data.o ./bin/sol__JAC.o ./bin/rdr__RopeSys.o

./bin/rdr__RopeSys.o: ./bin/mbs__Data.o ./bin/mbs__Tools.o

./bin/main.o: 		  ./bin/sys__Dyn.o

./bin/osVar.o: $(osVar)
	mkdir -p $(@D)
	$(gfort) -o ./bin/osVar.o $(osVar)


# --- Clean target ---
clean:
	rm -r ./bin/*
