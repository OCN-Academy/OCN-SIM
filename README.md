# OCN-SIM-FLEX

OCN-SIM-FLEX is a tool for the approximate dynamic simulation of fully submerged 
flexible maritime systems published by [OCN Academy] (http://www.ocnacademy.org).


# Licenses

OCNS-SIM FLEX is released under the GPLv3 (compare file `LICENSE`). Moreover, it contains the libraries
BLAS and LAPACK. The licenses of these libraries are contained in the files `LICENSE_LAPACK.txt` as well
as `LICENSE_BLAS.txt`.

# Building

## Linux

### Prerequisits
1. GNU compiler collection (gfortran)
2. GNU make
2. Python for postprocessing plus Python packages `numpy` and `vtk`

### Compiling
After having installed the above create a new directory for the project. Then:
1. Clone or download this repository from 
    `https://gitlab.com/OCN-Academy/OCN-SIM.git`
    and navigate to it's root directory (the
    one containing the folders `src`, `loadcases`, `post`, etc.)
2. From the root directory of the repository type `make` to build the binaries.

## Windows

### Prerequisits
1. Download and install [MinGW] (http://www.mingw.org)
2. Download and install Python for postprocessing. There are many good distributions
    out there, which I will not dare compare, however, from my personal experience, I
    can very much recommend the [Anaconda] (https://www.continuum.io) package.
    **Pay attention to download the 2.x version of python, as the `vtk` package
    required by this software is not yet fully supported in python 3.x**
3. For Python, at least install the `numpy` and `vtk` packages.

### Compiling
After having installed the above create a new directory for the project. Then:
1. Clone or download this repository from 
    `https://gitlab.com/OCN-Academy/OCN-SIM.git`
2. Open MSYS from MinGW and navigate to the repo's root directory (the
    one containing the folders `src`, `loadcases`, `post`, etc.)
3. Configure the contained makefile to compile for windows and not use prebuilt
    Lapack library (compare comments in the makefile) -- unless you do have a
    precompiled version of Lapack installed
4. From the root directory of the repository type `make` to build the binaries.

### Possible problems

#### You forgot to switch configuration to Windows prior to first build
When compiling the source without prior switch to windows configuration in the makefile,
the osVar.o object file does not get recompiled when switching configuration to windows.
The reason for this is, that the compiled object file already exists and is up to date
and thus not recompiled by make. In conclusion, the binary won't work correctly
due to inconsistent path seperators (`\` vs `/`) and other misfits.

To correct this issue, call `make clean` to clean th build output and then rebuild
the project issuing `make`.


# Basic usage and example loadcases

## Prerequisits

It is vitally important, that the `OCN-SIM-FLEX.conf` file provided in the
loadcases directory is always present in the *current working directory*
(i.e. the directory you're in, when you're launching the program) and configured
properly. The file contains information about the root directory of the
installation and is vital to postprocessing the data.

## Basic usage

After compilation, you'll find the OCN-SIM-FLEX binary in the bin directory.
If you're on GNU/Linux, the command to run OCN-SIM-FLEX is

    OCN-SIM-FLEX loadcaseFile
    
and if you're on Microsoft Windows, it's

    OCN-SIM-FLEX.exe loadcaseFile

where `loadcasFile` is the name of the loadcase main file. The typical suffix for these is
*.conf. Consequently, we also follow this convention for the example loadcase files located
in the `loadcases` directory included in the repository.

## Post processing

For post processing, [Paraview](https://www.paraview.org/) is the recommended way to go.
For a brief primer on postprocessing the results, refer to
[this tutorial](https://gitlab.com/OCN-Academy/OCN-ED/blob/master/OCN-SIM-FLEX-POST/postprocessing.md).

## Running the examples and testing the build

Navigate to the `loadcases/example/` directory and type
    
    ../../bin/OCN-SIM-FLEX example.conf (Linux)
    
    ..\..\bin\OCN-SIM-FLEX.exe example.conf (Windows)

You should get an output similar to

    ~/Dokumente/OCN-SIM/loadcases$ ../bin/OCN-SIM-FLEX example.conf
    
    Step:       0 Time per Step:  0.800E-04s
    Step:     100 Time per Step:  0.160E-03s
    ...
    Step:    3900 Time per Step:  0.200E-03s
    Step:    4000 Time per Step:  0.200E-03s
    Done in   0.80399999999999994      s
    Converting results to vtk file timeseries. Executing command:
    python /home/christoph/Dokumente/OCN-SIM//post/timeSeriesToVTK.py ./example.out/common
    Python: Timeseries was succesfully converted to series of vtk files
    Done

You should then get a folder called `example.out`. In it, you will find
timestep files in csv format as well as a subfolder called `vtk` containing
the output converted to vtk-timesiereis files readable by ParaView.

If not, something went seriously wrong. Feel free to contact me:
    [christoph@ocnacademy.org] (mailto:christoph@ocnacademy.org)


