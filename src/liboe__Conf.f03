!*******************************************************************************
!   OCN-SIM Flex
!   Copyright (C) 2017 by Christoph Otto
!   christoph@ocnacademy.org
!   www.ocnacademy.org
!
!
!   MODULE :: Global configuration
!
!   PURPOSE
!   Floating point precision, string lengths, etc.
!
!   
!     ___   ____ _   _    ____ ___ __  __    _____ _     _______  __
!    / _ \ / ___| \ | |  / ___|_ _|  \/  |  |  ___| |   | ____\ \/ /
!   | | | | |   |  \| |  \___ \| || |\/| |  | |_  | |   |  _|  \  / 
!   | |_| | |___| |\  |   ___) | || |  | |  |  _| | |___| |___ /  \
!    \___/ \____|_| \_|  |____/___|_|  |_|  |_|   |_____|_____/_/\_\
!
!
!   LICENSE
!   This file is part of OCN-SIM Flex.
!
!   OCN-SIM Flex is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   OCN-SIM Flex is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with OCN-SIM Flex. If not, see <http://www.gnu.org/licenses/>.
!
!*******************************************************************************

MODULE liboe__Conf
    
    !***************************************************************************
    ! Configuration                                                            !
    !***************************************************************************
    
    ! Attention, you also have to change sgesv/dgesv solver in sysDyn__slvAcc
    ! in sys__Dyn.f03, if you modify this one
    integer, parameter :: int8 = selected_int_kind(3)   ! 8 bit integer
    integer, parameter :: dp   = kind(1.d0)             ! Double precision
    
    integer, parameter :: prec = dp                     ! Solver precision
    
    
    logical, parameter :: calcInternalForces       = .true. ! Switch internal forces calculation on or off
    logical, parameter :: calcConstraintsCross     = .true. ! Cross-wise projection of secondary constraints
    logical, parameter :: calcQuadVelocityTerms    = .true. ! Only used for resulting accelerations so far, not for dynamics
    logical, parameter :: RRF__mergeSecConstraints = .true. ! Merge nodes involved in secondary constraints
    
    !***************************************************************************
    ! String lengths                                                           !
    !***************************************************************************
    ! Maximum length of numbers converted to strings
    integer, parameter :: MAX_NUM2STR_LENGTH   = 64
    
    ! Maximum length of error headers
    integer, parameter :: MAXSTRL_ERR__HEAD    = 256
    
    ! Maximum length of routine names for errors
    integer, parameter :: MAXSTRL_ERR__ROUNAME = 128
    
    ! Maximum length of module names for errors
    integer, parameter :: MAXSTRL_ERR__MODNAME = 128
    
    ! Maximum length of module names for errors
    integer, parameter :: MAXSTRL_FILE__FNAME  = 2048
    
    ! Maximum length of identifiers being read from files
    integer, parameter :: MAX_STRIDENT_LENGTH  = 4
    
    ! Maximum length of strings being read from files
    integer, parameter :: MAX_STRINGVAL_LENGTH = 256
    
    ! Maximum line length in input files
    integer, parameter :: MAX_FILE_LINE_LENGTH = 256
        
    
END MODULE liboe__Conf
