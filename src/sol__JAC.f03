!*******************************************************************************
!   OCN-SIM Flex
!   Copyright (C) 2017 by Christoph Otto
!   christoph@ocnacademy.org
!   www.ocnacademy.org
!
!
!   MODULE :: Lagrangian solver using Jacobian-matrix of minimal coordinates 
!
!   PURPOSE
!   Lagrangian solver using Jacobian-matrix in terms of minimal coordinates.
!
!   
!     ___   ____ _   _    ____ ___ __  __    _____ _     _______  __
!    / _ \ / ___| \ | |  / ___|_ _|  \/  |  |  ___| |   | ____\ \/ /
!   | | | | |   |  \| |  \___ \| || |\/| |  | |_  | |   |  _|  \  / 
!   | |_| | |___| |\  |   ___) | || |  | |  |  _| | |___| |___ /  \
!    \___/ \____|_| \_|  |____/___|_|  |_|  |_|   |_____|_____/_/\_\
!
!
!   LICENSE
!   This file is part of OCN-SIM Flex.
!
!   OCN-SIM Flex is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   OCN-SIM Flex is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with OCN-SIM Flex. If not, see <http://www.gnu.org/licenses/>.
!
!*******************************************************************************

MODULE sol__JAC
    
    USE liboe__Conf
    USE liboe__Types
    USE liboe__Tools
    USE mbs__Tools
    USE mbs__Types
    USE mbs__Dyn
    
    implicit none
    
    PUBLIC :: solVars__JAC
    
    PUBLIC :: slvJAC__initSolVars, slvJAC__cleanupSolVars
    PUBLIC :: slvJAC__updateSolVars
    PUBLIC :: slvJAC__slvAccFB
    PUBLIC :: slvJAC__calcCartAccFB

PRIVATE

    TYPE :: solVars__JAC
        real(prec), allocatable :: MGen(:,:)     ! Generalised mass matrix in terms of minimal coordinates
        real(prec), allocatable :: MPhys(:)      ! Physical mass matrix of flexible bodies. Diagonal matrix stored as a vector. Dimension(3*nNodes)
        real(prec), allocatable :: JAC(:,:)      ! Jacobian
        real(prec), allocatable :: JAC_T(:,:)    ! Transpose of Jacobian
        real(prec), allocatable :: JAC_d(:,:)    ! Time derivative of Jacobian
        
        real(prec), allocatable :: T(:,:,:)      ! Relative transformation matrices of elements wrt predecessor element (dimensions (3,3,mbd%nNodes))
        real(prec), allocatable :: T_AL(:,:,:)   ! Derivatives of relative transformation matrices wrt alpha (dimensions (3,3,mbd%nNodes))
        real(prec), allocatable :: T_BE(:,:,:)   ! Derivatives of relative transformation matrices wrt beta (dimensions (3,3,mbd%nNodes))
        real(prec), allocatable :: T_ZE(:,:,:)   ! Transformation matrices of elements wrt CSYS_0 (dimensions(3,3,mbd%nNodes))
        real(prec), allocatable :: T_AL_d(:,:,:) ! Time-derivative of T_AL
        real(prec), allocatable :: T_BE_d(:,:,:) ! Time-derivative of T_BE
        real(prec), allocatable :: T_ZE_d(:,:,:) ! Time-derivative of T_ZE
        
        real(prec), allocatable :: q(:)          ! Vector of relative joint coordinates. Dimension (2*nNodes)
        real(prec), allocatable :: v(:)          ! Vector of relative joint velocities. Dimension (2*nNodes)
        real(prec), allocatable :: a(:)          ! Vector of relative joint accelerations. Dimension (2*nNodes)
        
        real(prec), allocatable :: ipiv(:)       ! Pivoting vector obtained from factorisation of mass matrix (LAPACK)
        
        type(sysElem__ConstrExpl),   allocatable :: constrExpl(:)
        integer                                  :: nConstrExpl
        
        logical,                     allocatable :: ri_depOn_qj(:,:) ! Matrix with flags indicating, whether position of node i is dependent on joint j. Dimensions (nNodes, nConstr)
        
        type(rtData__ConstraintSec), allocatable :: rtc(:)   ! Constraint runtime data type
        real(prec),                  allocatable :: fCo(:,:) ! Correctional force representing implicit, loop-closing constraint (currently implemented as PID-controllers). Dimensions (3,mbd%nNodes)
    END TYPE

CONTAINS
    !***************************************************************************
    ! Initialise solver specific variables
    !***************************************************************************
    PURE SUBROUTINE slvJAC__initSolVars ( this, mFB, nNodes, constrExpl, nConstrExpl, &
                                          q0, v0, nConstrSec )
        implicit none
        
        type(solVars__JAC),        intent(inout) :: this
        
        real(prec),                intent(in)    :: mFB(nNodes)  ! Node masses
        integer,                   intent(in)    :: nNodes       ! Total number of nodes
        
        type(sysElem__ConstrExpl), intent(in)    :: constrExpl(nConstrExpl)
        integer,                   intent(in)    :: nConstrExpl

        real(prec),                intent(in)    :: q0(2*nNodes) ! Initial joint positions
        real(prec),                intent(in)    :: v0(2*nNodes) ! Initial joint velocities
        
        integer,                   intent(in)    :: nConstrSec   ! Total number of secondary constraints
        
        integer :: iNo ! Current node index
        integer :: iCo ! Current constrain
        integer :: j   ! Counting index
        
        
        ! Allocations
        allocate (this%q(2*nNodes), this%v(2*nNodes), this%a(2*nNodes))
        allocate (this%T(3,3,nNodes), this%T_AL(3,3,nNodes), this%T_BE(3,3,nNodes))
        allocate (this%T_ZE(3,3,nNodes))
        
        allocate (this%JAC(3*nNodes,2*nNodes))
        allocate (this%JAC_T(2*nNodes,3*nNodes))
        allocate (this%MPhys(3*nNodes))
        allocate (this%MGen(2*nNodes,2*nNodes))
        allocate (this%ipiv(2*nNodes))
        
        allocate (this%rtc(nConstrSec))
        allocate (this%fCo(3,nNodes))
        
        if (calcQuadVelocityTerms) then
            allocate (this%T_AL_d(3,3,nNodes), this%T_BE_d(3,3,nNodes))
            allocate (this%T_ZE_d(3,3,nNodes))
            allocate (this%JAC_d(3*nNodes,2*nNodes))
        end if
        
        ! Initial joint coordinates
        this%q = q0
        this%v = v0
        
        do iNo = 1,nNodes
            this%MPhys(3*iNo-2:3*iNo) = mFB(iNo)
        end do
        
        allocate(this%constrExpl(nConstrExpl))
        this%constrExpl(:) = constrExpl(:)
        
        
        ! Determine dependencies of node positions on joints____________________
        !   Determine, whether position of node i is dependent on joint j. This
        !   is done by simply iterating down over all predecessors of each node
        !   iNo. If the predecessor is <= 0, it is a marker on CSYS0. In this
        !   case the iteration is stopped and the iteration over the nodes iNo
        !   is continued.
        ! ______________________________________________________________________
        ! Allocate memory and initialise all values to .false.
        allocate (this%ri_depOn_qj(nNodes, nConstrExpl))
        this%ri_depOn_qj(:,:) = .false.

        do iNo = 1,nConstrExpl
            ! Current node iNo depends on its own joint
            this%ri_depOn_qj(iNo,iNo) = .true.
            
            ! Iterate over the predecessors of node iNo
            j = iNo
            iterpred: do
                if (constrExpl(j)%predRT /= MTYPE__FLEX) exit iterpred
                this%ri_depOn_qj(iNo,constrExpl(j)%pred) = .true.
                j = constrExpl(j)%pred
            end do iterpred
        end do
        
        
        ! --- Secondary constraints ---
        do iCo = 1,nConstrSec
            this%rtc(iCo)%errI(:)     = ZE
            this%rtc(iCo)%errI_M(:)   = ZE
        end do
        
        this%fCo(:,:) = ZE
        
    END SUBROUTINE slvJAC__initSolVars
    
    
    !***************************************************************************
    ! Cleanup solver specific variables
    !***************************************************************************
    PURE SUBROUTINE slvJAC__cleanupSolVars(this)
        implicit none
        
        type(solVars__JAC), intent(inout) :: this
        
        deallocate (this%q,this%v,this%a)
        deallocate (this%T, this%T_AL, this%T_BE, this%T_ZE)
        
        if (calcQuadVelocityTerms) &
            deallocate (this%T_AL_d, this%T_BE_d, this%T_ZE_d, this%JAC_d)
        
        deallocate (this%JAC, this%JAC_T)
        deallocate (this%MPhys)
        deallocate (this%MGen)
        deallocate (this%ipiv)
        
        deallocate (this%constrExpl)
        deallocate (this%ri_depOn_qj)
        
        deallocate (this%rtc)
        deallocate (this%fCo)
        
    END SUBROUTINE slvJAC__cleanupSolVars
    
    
    !***************************************************************************
    ! Update state variables after integration step
    !***************************************************************************
    PURE SUBROUTINE slvJAC__updateSolVars( this, rMaFB, rMaFB_d, nNodes, &
                                           rMaCS, nMaCS, constrSec, nConstrSec, &
                                           kp, ki, kd, tStep )
        implicit none
        
        type(solVars__JAC), intent(inout) :: this
        
        real(prec),                  intent(out) :: rMaFB(3,nNodes)              ! Marker positions: Flexible body nodes. Dimension(3,nNodes)
        real(prec),                  intent(out) :: rMaFB_d(3,nNodes)            ! Marker velocities: Flexible body nodes. Dimension(3,nNodes)
        
        integer,                     intent(in)  :: nNodes                       ! Total number of nodes
                
        real(prec),                  intent(in)  :: rMaCS(3,nMaCS)               ! Marker positions: Reference coordinate systems. Dimension(3,nMarkersCS)
        integer,                     intent(in)  :: nMaCS                        ! Number of reference markers
        
        type(sysElem__ConstrSec),    intent(in)  :: constrSec(nConstrSec)        ! Custom data type describing the multibody data of the system 
        integer,                     intent(in)  :: nConstrSec                   ! Number of secondary constraints
        
        real(prec),                  intent(in)  :: kp, ki, kd, tStep            ! PID parameters and timestep for secondary constraints
        
        integer :: iNode ! Current node ID
        
        
        call mbsDyn__calcTransMatsFB( this%T, this%T_AL, this%T_BE, this%T_ZE, &
                                      this%q, this%constrExpl, nNodes )
        
        call mbsDyn__calcPositionsFB( rMaFB, rMaCS, this%T_ZE, this%constrExpl, &
                                      nMaCS, nNodes )
        
        call slvJAC__calcJacobian( this, rMaCS, rMaFB, nNodes, nMaCS )
        
        this%JAC_T = transpose(this%JAC)
        
        do iNode = 1,nNodes
            rMaFB_d(:,iNode) = matmul(this%JAC(3*iNode-2:3*iNode,:),this%v)
        end do
        
        this%MGen = matmul(matmulDiagRight(this%JAC_T, this%MPhys,2*nNodes,3*nNodes), this%JAC)
        
        call mbsDyn__calcConstraintForces( this%rtc, this%fCo, constrSec, nConstrSec, &
                                           this%constrExpl, rMaFB, rMaFB_d, kp, ki, kd, tStep )
        
    END SUBROUTINE
    
    
    !***************************************************************************
    ! Solves for flexible body accelerations
    !***************************************************************************
    SUBROUTINE slvJAC__slvAccFB(this, rhs, nNodes)
        implicit none
        
        type(solVars__JAC), intent(inout) :: this
        
        real(prec), intent(in)    :: rhs(3,nNodes)            ! Right hand side
        integer,    intent(in)    :: nNodes                   ! Number of nodes
        
        integer :: info  ! Solver status variable
        integer :: iNode ! Node ID
        
        
        ! Calculate right hand side. Do not be confused: vector acc enters
        ! dgesv as right hand side vector and is then transformed to
        ! actual solution vector acc. Refer to LAPACK documentation
        this%a = ZE
        do iNode = 1,nNodes
            this%a = this%a + matmul(this%JAC_T(:,3*iNode-2:3*iNode), rhs(:,iNode))
        end do

        ! Attention, TODO: Adapt to precision accordingly. No conditionals
        ! for this, in order to improve performance
        call dgesv(2*nNodes, 1, this%MGen, 2*nNodes, this%ipiv, this%a, 2*nNodes, info)

        ! Primitive error handling. TODO: improve
        if (info > 0) then
            print *, 'Error in SGESV/DGESV: ', info
        end if

    END SUBROUTINE slvJAC__slvAccFB
    
    
    !***************************************************************************
    ! Solves for flexible body Cartesian accelerations
    !***************************************************************************
    SUBROUTINE slvJAC__calcCartAccFB( this, rMaFB_dd, rMaFB, nNodes, rMaCS, nMaCS )
        implicit none
        
        type(solVars__JAC),        intent(inout) :: this
        
        real(prec),                intent(out)   :: rMaFB_dd(3,nNodes)           ! Marker velocities: Flexible body nodes. Dimension(3,nNodes)
        real(prec),                intent(in)    :: rMaFB(3,nNodes)              ! Marker positions including markers in reference coordinate system CSYS0. Markers in CSYS0 count downwards from index (:,0).
        integer,                   intent(in)    :: nNodes                       ! Total number of nodes
                
        real(prec),                intent(in)    :: rMaCS(3,nMaCS)               ! Marker positions: Reference coordinate systems. Dimension(3,nMarkersCS)
        integer,                   intent(in)    :: nMaCS                        ! Number of reference markers
        
        
        integer :: iNode
        
        if (calcQuadVelocityTerms) then
            call mbsDyn__calcTransMatsFB_timeDeriv( this%T_AL_d, this%T_BE_d, this%T_ZE_d, &
                this%T, this%T_AL, this%T_BE, this%T_ZE, this%v, this%constrExpl, nNodes )
            
            call slvJAC__calcJacobian_timeDeriv( this, rMaCS, rMaFB, nNodes, nMaCS )
            
            do iNode = 1,nNodes
                rMaFB_dd(:,iNode) = matmul(this%JAC  (3*iNode-2:3*iNode,:), this%a) + &
                                    matmul(this%JAC_d(3*iNode-2:3*iNode,:), this%v)
            end do
        else
            do iNode = 1,nNodes
                rMaFB_dd(:,iNode) = matmul(this%JAC(3*iNode-2:3*iNode,:), this%a)
            end do
        end if
    END SUBROUTINE
    
    
    !***************************************************************************
    ! Calculate JACOBIAN-matrix
    !***************************************************************************
    PURE SUBROUTINE slvJAC__calcJacobian( this, rMaCS, rMaFB, nNodes, nMaCS )
        ! Calculates Jacobian matrix of nodes of flexible bodies.
        implicit none
        
        type(solVars__JAC), intent(inout) :: this
        
        real(prec),                intent(in) :: rMaCS(3,nMaCS)  ! Marker positions including markers in reference coordinate system CSYS0. Markers in CSYS0 count downwards from index (:,0).
        real(prec),                intent(in) :: rMaFB(3,nNodes) ! Marker positions including markers in reference coordinate system CSYS0. Markers in CSYS0 count downwards from index (:,0).
        integer,                   intent(in) :: nNodes          ! Number of nodes
        integer,                   intent(in) :: nMaCS           ! Number of markers in CSYS 0
        
        real(prec) :: T_pj(3,3) ! Transformation matrix of predecessor of j
        real(prec) :: d_pji(3)  ! Vector pointing from the predecessor of node j to coordinates of node i in reference system j
        integer    :: i,j       ! Indices
        
        this%JAC(:,:) = ZE
        
        
        do j = 1,nNodes
            do i = j,nNodes
                if (this%ri_depOn_qj(i,j)) then
                    
                    if (this%constrExpl(j)%predRT==MTYPE__FLEX) then
                        d_pji = matmul( transpose(this%T_ZE(:,:,j)), rMaFB(:,i)-rMaFB(:,this%constrExpl(j)%pred) )
                        T_pj  = this%T_ZE(:,:,this%constrExpl(j)%pred)
                    else
                        d_pji = matmul( transpose(this%T_ZE(:,:,j)), rMaFB(:,i)-rMaCS(:,this%constrExpl(j)%pred) )
                        T_pj  = I3
                    end if 
                    
                    ! Jacobian of node i with respect to alpha_j
                    this%JAC(3*i-2 : 3*i, 2*j-1) = matmul( T_pj, matmul(this%T_AL(:,:,j), d_pji) )
                    
                    ! Jacobian of node i with respect to beta_j
                    this%JAC(3*i-2 : 3*i, 2*j)   = matmul( T_pj, matmul(this%T_BE(:,:,j), d_pji) )
                    
                end if
            end do
        end do
    END SUBROUTINE
    
    
    !***************************************************************************
    ! Calculate time derivative of JACOBIAN-matrix
    !***************************************************************************
    PURE SUBROUTINE slvJAC__calcJacobian_timeDeriv( this, rMaCS, rMaFB, nNodes, nMaCS )
        ! Calculates time derivative of Jacobian matrix of nodes of flexible bodies
        implicit none
        
        type(solVars__JAC), intent(inout) :: this
        
        real(prec),                intent(in) :: rMaCS(3,nMaCS)             ! Marker positions including markers in reference coordinate system CSYS0. Markers in CSYS0 count downwards from index (:,0).
        real(prec),                intent(in) :: rMaFB(3,nNodes)            ! Marker positions including markers in reference coordinate system CSYS0. Markers in CSYS0 count downwards from index (:,0).
        integer,                   intent(in) :: nNodes                     ! Number of nodes
        integer,                   intent(in) :: nMaCS                      ! Number of markers in CSYS 0
        
        real(prec) :: T_pj(3,3)   ! Transformation matrix of predecessor of j
        real(prec) :: T_pj_d(3,3) ! Time derivative of transformation matrix of predecessor of j
        real(prec) :: d_pji(3)    ! Vector pointing from the predecessor of node j to coordinates of node i in reference system j
        integer    :: i,j         ! Indices
        
        this%JAC_d(:,:) = ZE
        
        
        do j = 1,nNodes
            do i = j,nNodes
                if (this%ri_depOn_qj(i,j)) then
                    
                    if (this%constrExpl(j)%predRT==MTYPE__FLEX) then
                        d_pji  = matmul( transpose(this%T_ZE(:,:,j)), rMaFB(:,i)-rMaFB(:,this%constrExpl(j)%pred) )
                        T_pj   = this%T_ZE  (:,:,this%constrExpl(j)%pred)
                        T_pj_d = this%T_ZE_d(:,:,this%constrExpl(j)%pred)
                    else
                        d_pji       = matmul( transpose(this%T_ZE(:,:,j)), rMaFB(:,i)-rMaCS(:,this%constrExpl(j)%pred) )
                        T_pj        = I3
                        T_pj_d(:,:) = ZE
                    end if 
                    
                    ! Jacobian of node i with respect to alpha_j
                    this%JAC_d(3*i-2 : 3*i, 2*j-1) = matmul( T_pj_d, matmul(this%T_AL(:,:,j),   d_pji) ) + &
                                                     matmul( T_pj,   matmul(this%T_AL_d(:,:,j), d_pji) )
                    
                    ! Jacobian of node i with respect to beta_j
                    this%JAC_d(3*i-2 : 3*i, 2*j)   = matmul( T_pj_d, matmul(this%T_BE(:,:,j),   d_pji) ) + &
                                                     matmul( T_pj,   matmul(this%T_BE_d(:,:,j), d_pji) )
                    
                end if
            end do
        end do
    END SUBROUTINE
    
    
END MODULE 
