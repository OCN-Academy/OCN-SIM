!*******************************************************************************
!   OCN-SIM Flex
!   Copyright (C) 2017 by Christoph Otto
!   christoph@ocnacademy.org
!   www.ocnacademy.org
!
!
!   MODULE :: Error handling
!
!   PURPOSE
!   Custom data types and associated routines for error handling and 
!   data validation
!
!   
!     ___   ____ _   _    ____ ___ __  __    _____ _     _______  __
!    / _ \ / ___| \ | |  / ___|_ _|  \/  |  |  ___| |   | ____\ \/ /
!   | | | | |   |  \| |  \___ \| || |\/| |  | |_  | |   |  _|  \  / 
!   | |_| | |___| |\  |   ___) | || |  | |  |  _| | |___| |___ /  \
!    \___/ \____|_| \_|  |____/___|_|  |_|  |_|   |_____|_____/_/\_\
!
!
!   LICENSE
!   This file is part of OCN-SIM Flex.
!
!   OCN-SIM Flex is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   OCN-SIM Flex is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with OCN-SIM Flex. If not, see <http://www.gnu.org/licenses/>.
!
!*******************************************************************************

MODULE liboe__Err
    
    USE liboe__Conf
    USE liboe__Types
    
    implicit none
    
    
    !***************************************************************************
    ! Public entities                                                          !
    !***************************************************************************
    PUBLIC :: loeErr
    PUBLIC :: loeErr__init, loeErr__print
    PUBLIC :: datCHK
    PUBLIC :: datCHK__init, datCHK__checkVal, datCHK__genErr
    
    
PRIVATE
    
    
    !***************************************************************************
    ! General error handling class                                             !
    !***************************************************************************
    TYPE :: loeErr
        character(len=MAXSTRL_ERR__HEAD)    :: headStr
        character(len=MAXSTRL_ERR__ROUNAME) :: rouName
        character(len=MAXSTRL_ERR__MODNAME) :: modName
    END TYPE loeErr
    
    
    !***************************************************************************
    ! Class for checking multiple data for validity                            !
    !***************************************************************************
    TYPE :: datCHK
        TYPE(loeErr) :: lErr
        integer      :: nErr
    END TYPE datCHK
    
    ! Overloaded subroutine interface
    INTERFACE datCHK__checkVal
        MODULE PROCEDURE datCHK__checkReal, datCHK__checkInt
    END INTERFACE


CONTAINS
    
    !***************************************************************************
    ! Set error header string                                                  !
    !***************************************************************************
    type(loeErr) FUNCTION loeErr__init(nameOfRoutine, nameOfModule)
        implicit none
        character(len=*), intent(in) :: nameOfRoutine
        character(len=*), intent(in) :: nameOfModule
        
        loeErr__init%rouName = trim(nameOfRoutine)
        loeErr__init%modName = trim(nameOfModule)
        loeErr__init%headStr = '--- Error in ' // trim(nameOfRoutine) // ' in module '// trim(nameOfModule) // ' ---'
    END FUNCTION loeErr__init
    
    
    !***************************************************************************
    ! Print error                                                              !
    !***************************************************************************
    SUBROUTINE loeErr__print(this, errString)
        implicit none
        type(loeErr), intent(inout) :: this
        character(*)                :: errString
        
        print *, trim(this%headStr) // endl // ' ' // errString // endl
    END SUBROUTINE loeErr__print
    
    
    !***************************************************************************
    ! Initialise error checking class                                          !
    !***************************************************************************
    type(datCHK) FUNCTION datCHK__init(nameOfRoutine, nameOfModule)
        implicit none
        
        ! Parameters____________________________________________________________
        ! ______________________________________________________________________
        character(len=*), intent(in) :: nameOfRoutine
        character(len=*), intent(in) :: nameOfModule
        
        datCHK__init%lErr = loeErr__init(trim(nameOfRoutine), trim(nameOfModule))
        datCHK__init%nErr = 0
    END FUNCTION datCHK__init
    
    
    !***************************************************************************
    ! Check single integer value                                               !
    !***************************************************************************
    SUBROUTINE datCHK__checkInt(this, valName, iVal, condition, refVal)
        
        ! Parameters____________________________________________________________
        ! ______________________________________________________________________
        type(datCHK),     intent(inout) :: this
        character(len=*), intent(in)    :: valName
        integer,          intent(in)    :: iVal
        character(len=*), intent(in)    :: condition
        integer,          intent(in)    :: refVal
        character(MAX_NUM2STR_LENGTH)   :: valStr
        
        ! Local variables
        logical :: conditionIsMet ! Flag indicating, whether required logical expression is true
        
        ! Main part
        conditionIsMet = .true.
        select case(condition)
            case ('>');  if(.not. (iVal >  refVal))  conditionIsMet = .false.
            case ('>='); if(.not. (iVal >= refVal)) conditionIsMet = .false.
            case ('<');  if(.not. (iVal <  refVal))  conditionIsMet = .false.
            case ('<='); if(.not. (iVal <= refVal)) conditionIsMet = .false.
            
            ! TODO default value for invalid usage!
        end select
        
        ! Not using num2Str function here in order to avoid circular dependency
        write(valStr,'(i0)') refVal
        if (.not. conditionIsMet) call datCHK__genErr(this, valName // ' must be ' // condition // ' ' // trim(valStr))
        
    END SUBROUTINE datCHK__checkInt
    
    
    !***************************************************************************
    ! Check single real value                                                  !
    !***************************************************************************
    SUBROUTINE datCHK__checkReal(this, valName, rVal, condition, refVal)
        
        ! Parameters____________________________________________________________
        ! ______________________________________________________________________
        type(datCHK),     intent(inout) :: this
        character(len=*), intent(in)    :: valName
        real(kind=prec),  intent(in)    :: rVal
        character(len=*), intent(in)    :: condition
        real(kind=prec),  intent(in)    :: refVal
        character(MAX_NUM2STR_LENGTH)   :: valStr
        
        ! Local variables
        logical            :: conditionIsMet ! Flag indicating, whether required logical expression is true
        
        ! Main part
        conditionIsMet = .true.
        select case(condition)
            case ('>');  if(.not. (rVal >  refVal)) conditionIsMet = .false.
            case ('>='); if(.not. (rVal >= refVal)) conditionIsMet = .false.
            case ('<');  if(.not. (rVal <  refVal)) conditionIsMet = .false.
            case ('<='); if(.not. (rVal <= refVal)) conditionIsMet = .false.
            
            ! TODO default value for invalid usage!
        end select
        
        ! Not using num2Str function here in order to avoid circular dependency
        write(valStr,'(e10.3)') refVal
        if (.not. conditionIsMet) call datCHK__genErr(this, valName // ' must be ' // condition // ' ' // trim(valStr))
        
    END SUBROUTINE datCHK__checkReal
    
    
    !***************************************************************************
    ! Generate general error and increase error counter                        !
    !***************************************************************************
    SUBROUTINE datCHK__genErr(this, errString)
        implicit none
        
        ! Parameters____________________________________________________________
        ! ______________________________________________________________________
        type(datCHK), intent(inout)  :: this
        character(len=*), intent(in) :: errString
        
        this%nErr = this%nErr+1
        call loeErr__print(this%lErr, trim(errString))
        
    END SUBROUTINE datCHK__genErr
    
END MODULE liboe__Err
