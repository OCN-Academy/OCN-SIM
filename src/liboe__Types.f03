!*******************************************************************************
!   OCN-SIM Flex
!   Copyright (C) 2017 by Christoph Otto
!   christoph@ocnacademy.org
!   www.ocnacademy.org
!
!
!   MODULE :: General custom data types
!
!   PURPOSE
!   General purpose custom data types.
!
!   
!     ___   ____ _   _    ____ ___ __  __    _____ _     _______  __
!    / _ \ / ___| \ | |  / ___|_ _|  \/  |  |  ___| |   | ____\ \/ /
!   | | | | |   |  \| |  \___ \| || |\/| |  | |_  | |   |  _|  \  / 
!   | |_| | |___| |\  |   ___) | || |  | |  |  _| | |___| |___ /  \
!    \___/ \____|_| \_|  |____/___|_|  |_|  |_|   |_____|_____/_/\_\
!
!
!   LICENSE
!   This file is part of OCN-SIM Flex.
!
!   OCN-SIM Flex is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   OCN-SIM Flex is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with OCN-SIM Flex. If not, see <http://www.gnu.org/licenses/>.
!
!*******************************************************************************

MODULE liboe__Types
    
    USE liboe__Conf
    
    implicit none
    

    !***************************************************************************
    ! Global constants
    !***************************************************************************
    ! Various constants
    real(prec), parameter :: PI   = 3.14159265359 ! Guess what :)
    real(prec), parameter :: ZE   = 0.0_prec      ! Zero value depending on selected precision. Useful as a parameter for function calls or in array assignments.
    
    real(prec), parameter :: I6(6,6) = reshape([1.0_prec, ZE, ZE, ZE, ZE, ZE,&
                                                ZE, 1.0_prec, ZE, ZE, ZE, ZE,&
                                                ZE, ZE, 1.0_prec, ZE, ZE, ZE,&
                                                ZE, ZE, ZE, 1.0_prec, ZE, ZE,&
                                                ZE, ZE, ZE, ZE, 1.0_prec, ZE,&
                                                ZE, ZE, ZE, ZE, ZE, 1.0_prec], [6,6])
    
    real(prec), parameter :: I3(3,3) = reshape([1.0_prec, ZE, ZE,&
                                                ZE, 1.0_prec, ZE,&
                                                ZE, ZE, 1.0_prec], [3,3])
    
    ! String constants
    character(*), parameter :: endl = new_line('a') ! Newline character
    character, parameter    :: quo  = "'"           ! Quotation mark
    
    
    !***************************************************************************
    ! Error strings
    ! TODO: too specific, move somewhere else
    !***************************************************************************
    character(*), parameter :: errStr_FloatingPoint = &
        'Floating point values may only contain digits, signs and one dot as a &
        &numerical comma. Letters and other characters are not allowed.'
    
    character(*), parameter :: errStr_Identifier = &
        'Identifiers are case-sensitive, i.e. it is distinguished between capital and non-capital letters.'
    
    
END MODULE liboe__Types
