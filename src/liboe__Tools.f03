!*******************************************************************************
!   OCN-SIM Flex
!   Copyright (C) 2017 by Christoph Otto
!   christoph@ocnacademy.org
!   www.ocnacademy.org
!
!
!   MODULE :: General tools
!
!   PURPOSE
!   General purpose tools.
!
!   
!     ___   ____ _   _    ____ ___ __  __    _____ _     _______  __
!    / _ \ / ___| \ | |  / ___|_ _|  \/  |  |  ___| |   | ____\ \/ /
!   | | | | |   |  \| |  \___ \| || |\/| |  | |_  | |   |  _|  \  / 
!   | |_| | |___| |\  |   ___) | || |  | |  |  _| | |___| |___ /  \
!    \___/ \____|_| \_|  |____/___|_|  |_|  |_|   |_____|_____/_/\_\
!
!
!   LICENSE
!   This file is part of OCN-SIM Flex.
!
!   OCN-SIM Flex is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   OCN-SIM Flex is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with OCN-SIM Flex. If not, see <http://www.gnu.org/licenses/>.
!
!*******************************************************************************

MODULE liboe__Tools
    
    USE liboe__Conf
    USE liboe__Types
    USE liboe__Err
    
    implicit none
    
    
    !***************************************************************************
    ! Module description                                                       !
    !***************************************************************************
    character(*), parameter, private :: modName = 'liboe__Tools'
    
    
    !***************************************************************************
    ! Public entities                                                          !
    !***************************************************************************
    PUBLIC :: matmulDiagLeft, matmulDiagRight
    
    PUBLIC :: invertMatr3, invertMatr2, vec2ScewMatr
    PUBLIC :: normaliseVec3, angVecN, crossProduct
    
    PUBLIC :: bisecSearch
    
    PUBLIC :: printVec
    
    PUBLIC :: num2Str, str2Num
    PUBLIC :: castPtr__MatrReal
    
    
PRIVATE

    
    !***************************************************************************
    ! Interface to number conversion functions
    !***************************************************************************
    INTERFACE num2Str
        MODULE PROCEDURE int2Str, int2StrF, real2Str, real2StrF
    END INTERFACE
    
    INTERFACE printVec
        MODULE PROCEDURE printShortIntVec, printIntVec, printRealVec
    END INTERFACE
    
    INTERFACE str2Num
        MODULE PROCEDURE str2Int, str2Real
    END INTERFACE
    
CONTAINS
    
    
    !***************************************************************************
    ! Multiplies a matrix with a diagonal matrix from the left
    !***************************************************************************
    PURE FUNCTION matmulDiagLeft (mat, diagMatVec, nRows, nCols) RESULT(M)
        ! Description
        !   M = diag(diagMatVec)*mat
        !   Multiplication of an (m,n)-matrix mat from the left side with a
        !   diagonal matrix diagMatVec. The diagonal elements of diagMatVec are
        !   given as a vector. Compare e.g.:
        !   https://en.wikipedia.org/wiki/Diagonal_matrix#Matrix_operations
        implicit none
        
        real(prec)             :: M(nRows,nCols)    ! Result of matrix product
        
        real(prec), intent(in) :: mat(nRows,nCols)  ! Input matrix
        real(prec), intent(in) :: diagMatVec(nRows) ! Elements of the diagonal matrix stored as a vector
        integer,    intent(in) :: nRows             ! Number of rows of input matrix
        integer,    intent(in) :: nCols             ! Number of columns of input matrix
        
        integer :: i  ! Row index
        
        
        do i = 1,nRows
            M(i,:) = mat(i,:)*diagMatVec(i)
        end do
        
    END FUNCTION matmulDiagLeft
    
    
    !***************************************************************************
    ! Multiplies a matrix with a diagonal matrix from the right
    !***************************************************************************
    PURE FUNCTION matmulDiagRight (mat, diagMatVec, nRows, nCols) RESULT(M)
        ! Description
        !   M = mat*diag(diagMatVec)
        !   Multiplication of an (m,n)-matrix mat from the right side with a
        !   diagonal matrix diagMatVec. The diagonal elements of diagMatVec are
        !   given as a vector. Compare e.g.:
        !   https://en.wikipedia.org/wiki/Diagonal_matrix#Matrix_operations
        implicit none
        
        real(prec)             :: M(nRows,nCols)    ! Result of matrix product
        
        real(prec), intent(in) :: mat(nRows,nCols)  ! Input matrix
        real(prec), intent(in) :: diagMatVec(nCols) ! Elements of the diagonal matrix stored as a vector
        integer,    intent(in) :: nRows             ! Number of rows of input matrix
        integer,    intent(in) :: nCols             ! Number of columns of input matrix
        
        integer :: j  ! Column index
        
        
        do j = 1,nCols
            M(:,j) = mat(:,j)*diagMatVec(j)
        end do
        
    END FUNCTION matmulDiagRight
    
    
    !***************************************************************************
    ! Direct calculation of inverse of a (3,3) matrix
    !***************************************************************************
    PURE FUNCTION invertMatr3(matr) RESULT(mInv)
        ! Description
        !   Direct calculation of inverse of a (3,3) matrix. Refer to e.g.:
        !   http://www.cg.info.hiroshima-cu.ac.jp/~miyazaki/knowledge/teche23.html
        !   http://mathworld.wolfram.com/MatrixInverse.html
        implicit none
        
        real(prec)             :: mInv(3,3) ! Inverse matrix
        
        real(prec), intent(in) :: matr(3,3) ! Matrix
        
        real(prec)             :: detinv    ! 1/determinant of the matrix
        
        
        detinv = 1.0_prec/&
                 (  matr(1,1)*matr(2,2)*matr(3,3) &
                  + matr(1,2)*matr(2,3)*matr(3,1) &
                  + matr(1,3)*matr(2,1)*matr(3,2) &
                  - matr(1,1)*matr(2,3)*matr(3,2) &
                  - matr(1,2)*matr(2,1)*matr(3,3) &
                  - matr(1,3)*matr(2,2)*matr(3,1) )

        mInv(1,1) = detinv*( matr(2,2)*matr(3,3) - matr(2,3)*matr(3,2) )
        mInv(2,1) = detinv*( matr(2,3)*matr(3,1) - matr(2,1)*matr(3,3) )
        mInv(3,1) = detinv*( matr(2,1)*matr(3,2) - matr(2,2)*matr(3,1) )
        
        mInv(1,2) = detinv*( matr(1,3)*matr(3,2) - matr(1,2)*matr(3,3) )
        mInv(2,2) = detinv*( matr(1,1)*matr(3,3) - matr(1,3)*matr(3,1) )
        mInv(3,2) = detinv*( matr(1,2)*matr(3,1) - matr(1,1)*matr(3,2) )
        
        mInv(1,3) = detinv*( matr(1,2)*matr(2,3) - matr(1,3)*matr(2,2) )
        mInv(2,3) = detinv*( matr(1,3)*matr(2,1) - matr(1,1)*matr(2,3) )
        mInv(3,3) = detinv*( matr(1,1)*matr(2,2) - matr(1,2)*matr(2,1) )
    
    END FUNCTION
  

    !***************************************************************************
    ! Direct calculation of inverse of a (2,2) matrix
    !***************************************************************************    
    PURE FUNCTION invertMatr2(matr) RESULT(mInv)
        ! Description
        !   Direct calculation of inverse of a (3,3) matrix. Refer to e.g.:
        !   http://www.cg.info.hiroshima-cu.ac.jp/~miyazaki/knowledge/teche23.html
        !   http://mathworld.wolfram.com/MatrixInverse.html
        implicit none
        
        real(prec)             :: mInv(2,2) ! Inverse matrix
        real(prec), intent(in) :: matr(2,2) ! Matrix
        real(prec)             :: detinv    ! 1/determinant of the matrix
        
        
        detinv = 1.0_prec/(matr(1,1)*matr(2,2)-matr(1,2)*matr(2,1))
        
        mInv(:,1) = detinv*[matr(2,2), -matr(2,1)]
        mInv(:,2) = detinv*[-matr(1,2), matr(1,1)]
    
    END FUNCTION
    
    
    !***************************************************************************
    ! Create skew symmetric matrix from (3)-vector
    !***************************************************************************
    PURE FUNCTION vec2ScewMatr(vec)
        ! Description
        !   Creates scew symmetric matrix from (3)-vector. Refer e.g. to
        !   https://en.wikipedia.org/wiki/Skew-symmetric_matrix#Cross_product
        implicit none
        
        real(prec)            :: vec2ScewMatr(3,3) ! Resulting matrix
        
        real(prec),intent(in) :: vec(3)            ! Input vector
        
        
        vec2ScewMatr(:,1) = [ ZE,      vec(3), -vec(2)]
        vec2ScewMatr(:,2) = [-vec(3),  ZE,      vec(1)]
        vec2ScewMatr(:,3) = [ vec(2), -vec(1),  ZE    ]
        
    END FUNCTION
    
    
    !***************************************************************************
    ! Normalise a vector (scale to magnitude 1)
    !***************************************************************************
    PURE FUNCTION normaliseVec3(vec) RESULT(u)
        implicit none
        
        real(prec)             :: u(3)      ! Result: normalised vector
        real(prec), intent(in) :: vec(3) ! Vector to be normalised
        
        u = (vec)/sqrt(sum((vec)**2))
    END FUNCTION
    
    !***************************************************************************
    ! Determine angle between two normalised (3)-vectors
    !***************************************************************************
    PURE FUNCTION angVecN(vec1,vec2)
        ! Description
        !   Determines the angle between 2 normalised vectors vec1 and vec2.
        !   Vec1 and vec2 must be scaled to unity, i.e. |vec1| = |vec2| = 1.
        !   
        !   NOTE: If we get near 0° or 180°, the dot product may become
        !   GREATER than 1 due to numerical errors. Thus we only use
        !   the dot product for angles between 45°...135°. Otherwise,
        !   the cross product is used to determine the angle.
        implicit none
        
        real(prec)            :: angVecN
        
        real(prec),intent(in) :: vec1(3), vec2(3)
        
        real(prec) :: dotProd, croProd(3)
        
        
        dotProd = dot_product(vec1,vec2)
        
        if (dotProd>-0.707 .and. dotProd<0.707) then
            angVecN  = acos(dotProd)
        else
            croProd = crossProduct(vec1,vec2)
            
            if (dotProd>0.0) then
                angVecN = asin(sqrt(sum(croProd**2)))
            else
                angVecN = PI - asin(sqrt(sum(croProd**2)))
            end if
        end if
        
    END FUNCTION
    
    
    !***************************************************************************
    ! Bisectional search algorithm
    !***************************************************************************
    logical FUNCTION bisecSearch(iLo,iHi,vals,targ)
        implicit none
        
        integer,    intent(out) :: iLo     ! Index of lower boundary in value table
        integer,    intent(out) :: iHi     ! Index of upper boundary in value table
        real(prec), intent(in)  :: vals(:) ! Index of section in value table
        real(prec), intent(in)  :: targ    ! Target value to be found
        
        integer :: iSe ! Index of section in value table
        
        bisecSearch = .false.
        
        
        ! Set initial boundaries for bisection search and check, whether target
        ! is included in value table
        iLo = 1
        iHi = size(vals)
        
        if (vals(1) > targ .or. targ > vals(iHi)) return
        
        
        ! Find bounding value pair
        findBounds: do
            ! Check whether value at upper bound holds the target value
            if (targ == vals(iLo)) then
                iHi = iLo
                exit findBounds
            end if
            
            ! Check whether value at lower bound holds the target value
            if (targ == vals(iHi)) then
                iLo = iHi
                exit findBounds
            end if
            
            ! Intersect current interval in the middle
            iSe = (iLo+iHi)/2
            
            ! If target value targ is in the lower half, set the upper
            ! boundary of the new interval to the middle of the the current
            ! interval (iSe). Otherwise set the lower boundary to iSe.
            if (targ <= vals(iSe)) then
                iHi = iSe
            else
                iLo = iSe
            end if
            
            ! If there's only one value difference between the lower and
            ! upper boundary, we found the values between which the target
            ! value is located.
            if (iHi-iLo<2) exit findBounds
        end do findBounds
        
        bisecSearch = .true.
        
    END FUNCTION bisecSearch
    
    
    !***************************************************************************
    ! Cross product of two (3)-vectors
    !***************************************************************************
    PURE FUNCTION crossProduct (factor1, factor2)
        implicit none
        real(prec)            :: crossProduct(3)
        real(prec),intent(in) :: factor1(3), factor2(3)
        
        crossProduct(1) = factor1(2)*factor2(3) - factor1(3)*factor2(2)
        crossProduct(2) = factor1(3)*factor2(1) - factor1(1)*factor2(3)
        crossProduct(3) = factor1(1)*factor2(2) - factor1(2)*factor2(1)
    END FUNCTION crossProduct
    
    
    SUBROUTINE printIntVec (vec)
        implicit none
        
        integer :: vec(:)
        integer :: i,m
        
        m = size(vec)
        
        print *, ( i, ': ', vec(i), endl, i = 1, m )
    END SUBROUTINE
    
    SUBROUTINE printShortIntVec (vec)
        implicit none
        
        integer(int8) :: vec(:)
        integer       :: i,m
        
        m = size(vec)
        
        print *, ( i, ': ', vec(i), endl, i = 1, m )
    END SUBROUTINE
    
    SUBROUTINE printRealVec (vec)
        implicit none
        
        real(prec) :: vec(:)
        integer    :: i,m
        
        m = size(vec)
        
        print *, ( i, ': ', vec(i), endl, i = 1, m )
    END SUBROUTINE
    
    
    !***************************************************************************
    ! Convert integer to string
    !***************************************************************************
    PURE FUNCTION int2Str(iVal)
        implicit none
        
        character(MAX_NUM2STR_LENGTH) :: int2Str
        integer, intent(in)           :: iVal
        
        write(int2Str,'(i0)')  iVal
        
    END FUNCTION int2Str
    
    PURE FUNCTION int2StrF(iVal, fmtStr)
        implicit none
        
        character(MAX_NUM2STR_LENGTH) :: int2StrF
        integer, intent(in)           :: iVal
        character(*), intent(in)      :: fmtStr
        
        write(int2StrF,trim(fmtStr)) iVal
        
    END FUNCTION int2StrF
    
    
    !***************************************************************************
    ! Convert real to string
    !***************************************************************************
    PURE FUNCTION real2Str(rVal)
        implicit none
        
        character(MAX_NUM2STR_LENGTH) :: real2Str
        real(prec), intent(in)        :: rVal
        
        write(real2Str,'(e10.3)') rVal
        
    END FUNCTION real2Str
    
    PURE FUNCTION real2StrF(rVal, fmtStr)
        implicit none
        
        character(MAX_NUM2STR_LENGTH) :: real2StrF
        real(prec),   intent(in)      :: rVal
        character(*), intent(in)      :: fmtStr
        
        write(real2StrF,trim(fmtStr)) rVal
        
    END FUNCTION real2StrF
    

    !***************************************************************************
    ! Convert string to integer
    !***************************************************************************
    logical FUNCTION str2Int(strVal, iVal)
        implicit none
        
        character(*), intent(in)  :: strVal
        integer,      intent(out) :: iVal
        
        type(loeErr)              :: err
        integer                   :: ioErr
        
        err = loeErr__init('str2Int', modName)
        
        
        read (strVal,*, iostat = ioErr) iVal
        if (ioErr /= 0) then
            call loeErr__print(err, 'Cannot convert ' //quo// trim(strVal) //quo// ' to an integer value.' // endl // &
                ' Integer values may only contain digits and signs. Letters and other characters are ' // &
                'not allowed, except for exponentials. Error code: ' // trim(num2Str(ioErr)))
            str2Int = .false.
            return
        end if
        
        str2Int = .true.
    END FUNCTION
    
    
    !***************************************************************************
    ! Convert string to real
    !***************************************************************************
    logical FUNCTION str2Real(strVal, rVal)
        implicit none
        
        character(*), intent(in)  :: strVal
        real(prec),   intent(out) :: rVal
        
        type(loeErr)              :: err
        integer                   :: ioErr
        
        err = loeErr__init('str2Real', modName)
        
        
        read (strVal,*, iostat = ioErr) rVal
        if (ioErr /= 0) then
            call loeErr__print(err, 'Cannot convert ' //quo// trim(strVal) //quo// ' to a floating point value.' // endl // &
                ' Floating point values may only contain digits, signs and one dot as a numerical comma. Letters and ' // &
                'other characters are not allowed, except for exponentials. Error code: ' // trim(num2Str(ioErr)))
            str2Real = .false.
            return
        end if
        
        str2Real = .true.
    END FUNCTION
    
    
    !***************************************************************************
    ! Set pointer to matrix without 'target' attribute. WELCOME TO THE DARK SIDE
    !***************************************************************************
    SUBROUTINE castPtr__MatrReal(castPtr,targ)
        ! TODO rm or keep it up, though it is dangerous and evil
        real(prec), pointer, intent(inout) :: castPtr(:,:)
        real(prec), target,  intent(in)    :: targ(:,:)
        
        castPtr => targ
    END SUBROUTINE
    
END MODULE liboe__Tools
