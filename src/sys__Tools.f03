!*******************************************************************************
!   OCN-SIM Flex
!   Copyright (C) 2017 by Christoph Otto
!   christoph@ocnacademy.org
!   www.ocnacademy.org
!
!
!   MODULE :: System dynamics
!
!   PURPOSE
!   This module contains the main routines for time integration of the
!   combined system of flexible and rigid bodies under the influence of
!   hydrodynamic loads. For this purpose, it also contains the glue code
!   calculating external loads, hydrodynamic loads as well as the
!   interaction of flexible and rigid bodies.
!
!
!     ___   ____ _   _    ____ ___ __  __    _____ _     _______  __
!    / _ \ / ___| \ | |  / ___|_ _|  \/  |  |  ___| |   | ____\ \/ /
!   | | | | |   |  \| |  \___ \| || |\/| |  | |_  | |   |  _|  \  /
!   | |_| | |___| |\  |   ___) | || |  | |  |  _| | |___| |___ /  \
!    \___/ \____|_| \_|  |____/___|_|  |_|  |_|   |_____|_____/_/\_\
!
!
!   LICENSE
!   This file is part of OCN-SIM Flex.
!
!   OCN-SIM Flex is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   OCN-SIM Flex is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with OCN-SIM Flex. If not, see <http://www.gnu.org/licenses/>.
!
!*******************************************************************************
MODULE sys__Tools

    USE liboe__Conf
    USE liboe__Types
    USE mbs__Types

    implicit none


    !***************************************************************************
    ! Module description                                                       !
    !***************************************************************************
    character(*), parameter, private :: modName = 'sys__Tools'


    !***************************************************************************
    ! Public functions
    !***************************************************************************
    PUBLIC :: sysTools__allocIntVars
    PUBLIC :: sysTools__initGravityAndBuoyancyFB
    PUBLIC :: sysTools__initGravityAndBuoyancyRB


PRIVATE

CONTAINS

    !***************************************************************************
    ! Allocate matrices and vectors needed for integration
    !***************************************************************************
    SUBROUTINE sysTools__allocIntVars( rMaRB, rMaRB_d, qRB_d, qRB_dd, &
                                       omRB, TRB, PRB, PRB_d, &
                                       uRelFB, uFB, uRelRB, uRB, &
                                       fEFB, fGFB, fBFB, fERB, fERBMa, &
                                       fGRB, fBRB, fHyd, fHydRB, &
                                       nNodes, nRigids, nMarkersRB )
        implicit none
        
        real(prec), allocatable, intent(inout) :: rMaRB(:,:)  ! Dimension(3,nMarkersRB)
        real(prec), allocatable, intent(inout) :: rMaRB_d(:,:)! Dimension(3,nMarkersRB)
        
        real(prec), allocatable, intent(inout) :: qRB_d(:,:)   ! Rigid body velocities. Dimension(7, mbd%nRigids).
        real(prec), allocatable, intent(inout) :: qRB_dd(:,:)  ! Rigid body accelerationsDimension(7, mbd%nRigids).
        
        real(prec), allocatable, intent(inout) :: omRB(:,:)    ! Rigid body angular velocities with respect to CSYS_0
        real(prec), allocatable, intent(inout) :: TRB(:,:,:)   ! Rigid body transformation matrices
        real(prec), allocatable, intent(inout) :: PRB(:,:,:)   ! Rigid body velocity projection matrices
        real(prec), allocatable, intent(inout) :: PRB_d(:,:,:) ! Rigid body velocity projection matrices, time derivative
        
        real(prec), allocatable, intent(inout) :: uFB(:,:)    ! Flexible body flow velocity at positions of nodes
        real(prec), allocatable, intent(inout) :: uRelFB(:,:) ! Flexible body relative flow velocity with respect to nodes
        
        real(prec), allocatable, intent(inout) :: uRB(:,:)    ! Flow velocity at positions of rigid bodies
        real(prec), allocatable, intent(inout) :: uRelRB(:,:) ! Rigid body relative flow velocities
        
        ! --- External Loads ---
        real(prec), allocatable, intent(inout) :: fEFB(:,:)   ! Flexible body external loads
        real(prec), allocatable, intent(inout) :: fGFB(:,:)   ! Flexible body gravitational loads
        real(prec), allocatable, intent(inout) :: fBFB(:,:)   ! Flexible body buoyancy
        real(prec), allocatable, intent(inout) :: fERB(:,:)   ! Rigid body external forces and torques vector
        real(prec), allocatable, intent(inout) :: fERBMa(:,:) ! Rigid body marker external forces and torques vector
        real(prec), allocatable, intent(inout) :: fGRB(:,:)   ! Rigid body gravitational loads forces and torques vector
        real(prec), allocatable, intent(inout) :: fBRB(:,:)   ! Rigid body buoyancy forces and torques vector
        real(prec), allocatable, intent(inout) :: fHyd(:,:)   ! Flexible body hydrodynamic forces vector
        real(prec), allocatable, intent(inout) :: fHydRB(:,:) ! Rigid body hydrodynamic forces and torques vector
        
        integer,                 intent(in)    :: nNodes      ! Total number of nodes
        integer,                 intent(in)    :: nRigids     ! Total number of rigid bodies
        integer,                 intent(in)    :: nMarkersRB  ! Total number of markers on rigid bodies
        
        
        ! --- Positions, velocities ---
        allocate (rMaRB(  3, nMarkersRB))
        allocate (rMaRB_d(3, nMarkersRB))
        
        
        ! Rigid bodies
        allocate(qRB_d(7,nRigids))
        allocate(qRB_dd(7,nRigids))
        qRB_d(:,:)  = ZE
        qRB_dd(:,:) = ZE
        
        allocate(omRB(3,nRigids))
        allocate(TRB(3,3,nRigids))
        allocate(PRB(4,3,nRigids))
        allocate(PRB_d(4,3,nRigids))
        
        
        ! Flow velocities
        allocate(uRelFB(3,nNodes),uFB(3,nNodes))
        uRelFB(:,:) = ZE
        uFB(:,:)    = ZE
        
        allocate(uRelRB(3,nRigids),uRB(3,nRigids))
        uRelRB(:,:) = ZE
        uRB(:,:)    = ZE
        
        
        ! --- External forces ---
        ! Flexible body external loads
        allocate(fEFB(3, nNodes))
        fEFB(:,:) = ZE
        
        ! Gravitational forces and buoyancy on flexible bodies (constant)
        allocate(fGFB(3,nNodes),fBFB(3,nNodes))
        
        ! Hydrodynamic forces on flexible bodies
        allocate(fHyd(3,nNodes))
        fHyd(:,:) = ZE
        
        ! External forces and torques on rigid bodies
        allocate(fERB(6,nRigids))
        fERB(:,:) = ZE
        
        ! External forces and torques on rigid body markers
        allocate(fERBMa(3, nMarkersRB))
        fERBMa(:,:) = ZE

        ! Gravitational forces and buoyancy on rigid bodies (constant)
        allocate(fGRB(3,nRigids),fBRB(3,nRigids))
        
        ! Hydrodynamic forces and torques on rigid bodies
        allocate(fHydRB(6,nRigids))
        fHydRB(:,:) = 0.0
    END SUBROUTINE
    
    
    !***************************************************************************
    ! Initialise flexible body gravity and buoyancy vectors
    !***************************************************************************
    SUBROUTINE sysTools__initGravityAndBuoyancyFB( fGFB, fBFB, mFB, vFB, nNodes, &
                                                 rho, gVec )
        implicit none
        
        real(prec), intent(inout) :: fGFB(3,nNodes) ! Flexible body gravitational loads
        real(prec), intent(inout) :: fBFB(3,nNodes) ! Flexible body buoyancy
        
        real(prec), intent(in)    :: mFB(nNodes)    ! Node masses
        real(prec), intent(in)    :: vFB(nNodes)    ! Node volumes
        integer,    intent(in)    :: nNodes         ! Total number of ropes
        
        real(prec), intent(in)    :: rho            ! Fluid density
        real(prec), intent(in)    :: gVec(3)        ! Gravity vector
        
        integer :: iNode
        
        do iNode=1,nNodes
            fGFB(:,iNode) =  mFB(iNode)*gVec
            fBFB(:,iNode) = -vFB(iNode)*rho*gVec
        end do
    END SUBROUTINE
    
    
    !***************************************************************************
    ! Initialise rigid body gravity and buoyancy vectors
    !***************************************************************************
    SUBROUTINE sysTools__initGravityAndBuoyancyRB( fGRB, fBRB, rigids, nRigids, &
                                                 rho, gVec )
        implicit none
        
        real(prec),              intent(inout) :: fGRB(3,nRigids) ! Rigid body gravitational loads
        real(prec),              intent(inout) :: fBRB(3,nRigids) ! Rigid body buoyancy
        
        type(structElem__Rigid), intent(in)    :: rigids(nRigids) ! Rigid body data types
        integer,                 intent(in)    :: nRigids         ! Total number of rigid bodies
        
        real(prec),              intent(in)    :: rho             ! Fluid density
        real(prec),              intent(in)    :: gVec(3)         ! Gravity vector
        
        integer :: iRB
        
        do iRB=1,nRigids
            fGRB(:,iRB) =  rigids(iRB)%mass*gVec
            
            ! If we have a buoy, the buoyancy is calculated dynamically depending
            ! on how far the buoy is submerged and included in the vector of
            ! hydrodynamic loads
            if ( rigids(iRB)%loadType /= RBOD_BUOY) then
                fBRB(:,iRB) = -rigids(iRB)%vol*rho*gVec
            else
                fBRB(:,iRB) = ZE
            end if
        end do
    END SUBROUTINE

END MODULE
