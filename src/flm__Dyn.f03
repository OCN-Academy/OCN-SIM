!*******************************************************************************
!   OCN-SIM Flex
!   Copyright (C) 2017 by Christoph Otto
!   christoph@ocnacademy.org
!   www.ocnacademy.org
!
!
!   MODULE :: Fluid dynamics
!
!   PURPOSE
!   Package of routines for fluid dynamics
!
!   
!     ___   ____ _   _    ____ ___ __  __    _____ _     _______  __
!    / _ \ / ___| \ | |  / ___|_ _|  \/  |  |  ___| |   | ____\ \/ /
!   | | | | |   |  \| |  \___ \| || |\/| |  | |_  | |   |  _|  \  / 
!   | |_| | |___| |\  |   ___) | || |  | |  |  _| | |___| |___ /  \
!    \___/ \____|_| \_|  |____/___|_|  |_|  |_|   |_____|_____/_/\_\
!
!
!   LICENSE
!   This file is part of OCN-SIM Flex.
!
!   OCN-SIM Flex is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   OCN-SIM Flex is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with OCN-SIM Flex. If not, see <http://www.gnu.org/licenses/>.
!
!*******************************************************************************

MODULE flm__Dyn
    
    USE liboe__Conf
    USE liboe__Types
    USE liboe__Tools
    USE flm__Types
    USE flm__Data
    
    implicit none
    
    !***************************************************************************
    ! Public entities
    !***************************************************************************
    PUBLIC :: flmDyn__calcSurfaceElevation
    PUBLIC :: flmDyn__calcVelocities
    PUBLIC :: flmDyn__calcForceBar
    PUBLIC :: flmDyn__calcForceHydrofoil
    PUBLIC :: flmDyn__calcForceCylindricalBuoy
    
PRIVATE


CONTAINS

    !***************************************************************************
    ! Calculate surface elevation for given positions
    !***************************************************************************
    PURE SUBROUTINE flmDyn__calcSurfaceElevation(z, r, fld, t)
        ! Description___________________________________________________________
        !   Calculates fluid velocities for given discrete positions r and fluid
        !   model.
        ! ______________________________________________________________________
        implicit none
        
        real(prec), intent(out)    :: z(:)   ! Fluid velocities at positions specified in parameter r. Dimension(nPos)
        
        real(prec),    intent(in)  :: r(:,:) ! Vector containing positions at which fluid velocities shall be calculated. Z-coordinate is ignored. Dimension(3,nPos)
        type(flmData), intent(in)  :: fld    ! Fluid model to be used for calculation of the velocities
        real(prec),    intent(in)  :: t      ! Current simulation time
        
        integer :: nPos ! Number of positions on flexible bodies
        integer :: iPos ! Current position
        
        
        nPos = size(r,2)
        
        select case (fld%fieldType)
            case (FLMOD_STATIC) ! Non-moving fluid
                z(:) = ZE
            
            case (FLMOD_CONST)  ! Constant, uniform velocity field
                z(:) = ZE
            
            case (FLMOD_AIRY)
                do iPos = 1,nPos
                    z(iPos) = fld%field__Airy%amp*cos(fld%field__Airy%k*r(1,iPos) - fld%field__Airy%omega*t)
                end do
        end select
        
    END SUBROUTINE flmDyn__calcSurfaceElevation
    
    
    !***************************************************************************
    ! Calculate flow velocities for given positions
    !***************************************************************************
    PURE SUBROUTINE flmDyn__calcVelocities(u, r, fld, t)
        ! Description___________________________________________________________
        !   Calculates fluid velocities for given discrete positions r and fluid
        !   model.
        ! ______________________________________________________________________
        implicit none
        
        real(prec),    intent(out) :: u(:,:) ! Fluid velocities at positions specified in parameter r. Dimension(3,nPos)
        real(prec),    intent(in)  :: r(:,:) ! Vector containing positions at which fluid velocities shall be calculated. Dimension(3,nPos)
        type(flmData), intent(in)  :: fld    ! Fluid model to be used for calculation of the velocities
        real(prec),    intent(in)  :: t      ! Current simulation time
        
        real(prec) :: phi   ! Local wave phase
        
        integer :: nPos ! Number of positions on flexible bodies
        integer :: iPos ! Current position
        
        
        nPos = size(r,2)
        
        select case (fld%fieldType)
            case (FLMOD_STATIC) ! Non-moving fluid
                u(:,:) = ZE
            
            
            case (FLMOD_CONST)  ! Constant, uniform velocity field
                do iPos = 1,nPos
                    u(:,iPos) = fld%field__Const%vConst
                end do
            
            case (FLMOD_AIRY)  ! Constant, uniform velocity field
                do iPos = 1,nPos
                    phi = fld%field__Airy%k*r(1,iPos) + fld%field__Airy%omega*t
    
                    u(1,iPos) = fld%field__Airy%omega*fld%field__Airy%amp*( &
                        cosh(fld%field__Airy%k*(r(3,iPos) + fld%field__Airy%depth))/ &
                        sinh(fld%field__Airy%k*fld%field__Airy%depth) )*cos(phi);
                    
                    u(2,iPos) = ZE
                    
                    u(3,iPos) = fld%field__Airy%omega*fld%field__Airy%amp*( &
                        sinh(fld%field__Airy%k*(r(3,iPos) + fld%field__Airy%depth))/ &
                        sinh(fld%field__Airy%k*fld%field__Airy%depth) )*sin(phi);
                    
                    u(:,iPos) = u(:,iPos) + fld%field__Airy%vConst
                end do
        end select
        
    END SUBROUTINE flmDyn__calcVelocities
    
    
    !***************************************************************************
    ! Calculate hydrodynamic forces on a bar element
    !***************************************************************************
    FUNCTION flmDyn__calcForceBar(bNorm, uRel, rho, AProj, coeffs)
        implicit none
        
        real(prec) :: flmDyn__calcForceBar(3) ! Resulting force vector
        
        real(prec), intent(in) :: bNorm(3)     ! Normalised bar vector
        real(prec), intent(in) :: uRel(3)      ! Vector of relative flow velocities
        real(prec), intent(in) :: rho          ! Density of the fluid
        real(prec), intent(in) :: AProj        ! The bar's projected area (diameter*length)
        real(prec), intent(in) :: coeffs (:,:) ! Rope hydrodynamic coefficients vs angle of attack.
                                               ! Format: [angle of attack, drag, lift, shear]. Dimension(nVals,4)
        
        real(prec) :: uRelM    ! Magnitude of relative flow velocity vector
        real(prec) :: uRelN(3) ! Normalised relative flow velocity vector
        real(prec) :: theta    ! Angle of attack
        real(prec) :: cD       ! Drag coefficient

        
        uRelM  = sqrt(sum(uRel**2))                
        if (uRelM /= 0.0) then
            uRelN                   = uRel/uRelM
            theta                   = angVecN(bNorm,uRelN)
            cD                      = flmDyn__interpCoeffs1D(coeffs, theta)
            flmDyn__calcForceBar    = (0.25_prec*AProj*cD*rho*uRelM**2)*uRelN
        else
            flmDyn__calcForceBar(:) = ZE
        end if
    
    END FUNCTION
    
    
    !***************************************************************************
    ! Calculate hydrodynamic forces on a hydrofoil
    !***************************************************************************
    FUNCTION flmDyn__calcForceHydrofoil(T, uRel, rho, A0, coeffs) RESULT(fHYD)
        implicit none
        
        real(prec)             :: fHyd(6)      ! Vector of resulting hydrodynamic forces and torques
        
        real(prec), intent(in) :: T(3,3)       ! Transformation matrix wrt global coordinate system
        real(prec), intent(in) :: uRel(3)      ! Vector of relative flow velocities
        real(prec), intent(in) :: rho          ! Density of the fluid
        real(prec), intent(in) :: A0           ! Reference area of the hydrofoil
        real(prec), intent(in) :: coeffs (:,:) ! Hydrodynamic coefficients vs angle of attack.
                                               ! Format: [angle of attack, drag, lift, shear]. Dimension(nVals,4)
        
        
        real(prec) :: uLoc(3)  ! Relative rigid body flow velocity in local body-fixed coordinate system
        real(prec) :: uMagXY   ! Magnitude of relative flow velocity in local xy-plane
        real(prec) :: uMagSXY  ! Squared magnitude of relative flow velocity in local xy-plane
        real(prec) :: uLift(3) ! Local lift force direction in body-fixed coordinate system
        real(prec) :: uDrag(3) ! Local drag force direction in body-fixed coordinate system
        real(prec) :: cL       ! Lift coefficient
        real(prec) :: cD       ! Drag coefficient
        real(prec) :: fL       ! Magnitude of lift force
        real(prec) :: fD       ! Magnitude of drag force
        real(prec) :: ga       ! Angle of attack
        integer    :: iLo, iHi ! Index of section, lower and higher boundary theta for lookup in table
        
        
        ! Local flow velocity and magnitude
        uLoc    = matmul(transpose(T), uRel)
        
        uMagSXY = uLoc(1)**2 + uLoc(2)**2
        uMagXY  = sqrt(uMagSXY)
        
        ! Angle of attack and resulting directions of lift and drag
        ga      = atan2(uLoc(2),uLoc(1))
        uLift   = [-sin(ga), cos(ga), ZE]
        uDrag   = [uLoc(1), uLoc(2), ZE]/uMagXY
        
        ! Determine coefficients
        if (.not. bisecSearch(iLo,iHi,coeffs(:,1),ga)) then
            print *, 'Error in lookup table for hydrofoil coefficients. Value out of range: ' // &
                     trim(num2Str(ga))
            call exit
        end if
        
        if (iLo==iHi) then ! Value equals a value given in table. Non-standard case.
            cD = coeffs(iLo,2)
            cL = coeffs(iLo,3)
        
        else ! Value between two given values in table. Standard case.
            cD = coeffs(iLo,2) + &
                (coeffs(iHi,2)-coeffs(iLo,2))*(ga-coeffs(iLo,1))/(coeffs(iHi,1)-coeffs(iLo,1))
                
            cL = coeffs(iLo,3) + &
                (coeffs(iHi,3)-coeffs(iLo,3))*(ga-coeffs(iLo,1))/(coeffs(iHi,1)-coeffs(iLo,1))
        end if
        
        
        ! Lift and drag forces
        fL = 0.5_prec*rho*uMagSXY*A0*cL
        fD = 0.5_prec*rho*uMagSXY*A0*cD
        
        fHyd(1:3) = matmul(T, fL*uLift + fD*uDrag)
        fHyd(4:6) = ZE
        
    END FUNCTION
    
    
    !***************************************************************************
    ! Calculate hydrodynamic forces on a cylindrical buoy
    !***************************************************************************
    PURE FUNCTION flmDyn__calcForceCylindricalBuoy( qB, qB_d, omRB, TB, dB, lB, CDLat, CDLong, &
                                                    nDivB, t, gVec, fld ) RESULT(fHydB)
        implicit none
        
        real(prec) :: fHydB(6) ! Vector of hydrodynamic forces and torques
        
        real(prec),    intent(in) :: qB(7)   ! Rigid body pose
        real(prec),    intent(in) :: qB_d(7) ! Time derivative of rigid body pose
        real(prec),    intent(in) :: omRB(3) ! Rigid body angular velocity vector
        real(prec),    intent(in) :: TB(3,3) ! Rigid body transformation matrix
        real(prec),    intent(in) :: dB      ! Diameter
        real(prec),    intent(in) :: lB      ! Length
        real(prec),    intent(in) :: CDLat   ! Lateral drag coefficient
        real(prec),    intent(in) :: CDLong  ! Longitudinal drag coefficient
        integer,       intent(in) :: nDivB   ! Number of divisions of the buoy for hydrodynamic load calculation
        real(prec),    intent(in) :: t       ! Current time
        real(prec),    intent(in) :: gVec(3) ! Gravity vector
        type(flmData), intent(in) :: fld     ! Fluid model to be used for calculation of the velocities
        
        real(prec) :: lEl                ! Length of one element
        real(prec) :: AEl                ! Area of one element
        real(prec) :: VEl                ! Volume of one element
        
        real(prec) :: zMaLoc(nDivB)      ! Local z-position of current marker, x = y = 0
        real(prec) :: rMaGlob(3,nDivB)   ! Global marker positions
        
        real(prec) :: surfElev(nDivB)    ! Surface elevation at (x,y) coordinates of local markers
        real(prec) :: uFlow(3,nDivB)     ! Flow velocities at marker positions
        real(prec) :: uRel(3)            ! Relative flow velocities at current marker
        real(prec) :: uRelN(3)           ! Normalised vector of relative flow velocity
        real(prec) :: uNorm              ! Normal component of relative flow velocity
        
        real(prec) :: nZB(3)             ! Normalized z-axis vector of buoy
        real(prec) :: nNorm(3)           ! Normalised vector of normal component of relative flow velocity
        
        real(prec) :: fDN                ! Drag force due to normal component of relative flow velocity
        
        real(prec) :: fEl(3)             ! Force component acting on current element
        
        logical    :: isSubmerged(nDivB) ! Stores, whether element is submerged or not
        real(prec) :: subRatio(nDivB)    ! Ratio of submerged and non-submerged part of each element
        
        real(prec) :: kE       ! Factor rX = rEl1 + kE*gE
        
        integer    :: iMa  ! Current marker index
        integer    :: iMaP ! iMa-1
        
        
        fHydB(:) = ZE
        
        
        lEl = lB/nDivB
        AEl = 0.25_prec*PI*dB**2
        VEl = lEl*AEl
        
        do iMa = 1,nDivB
            zMaLoc (iMa)   = 0.5_prec*(lEl-lB) + (iMa-1)*lEl            
            rMaGlob(:,iMa) = qB(1:3) + TB(:,3)*zMaLoc(iMa)
        end do
        
        call flmDyn__calcSurfaceElevation(surfElev,rMaGlob,fld,t)
        call flmDyn__calcVelocities(uFlow,rMaGlob,fld,t)
        
        
        ! --- Calculate intersections with water surface ---
        do iMa = 1,nDivB
            if ( rMaGlob(3,iMa) < surfElev(iMa) ) then
                isSubmerged(iMa) = .true.
                subRatio(iMa) = 1.0_prec
            else
                isSubmerged(iMa) = .false.
                subRatio(iMa) = ZE
            end if
        end do
        
        do iMa = 2,nDivB
            iMaP = iMa-1
            if ( isSubmerged(iMa) .neqv. isSubmerged(iMaP) ) then
                kE = ( rMaGlob(3,iMaP)- surfElev(iMaP) ) / &
                     ( surfElev(iMa)  - surfElev(iMaP) - rMaGlob(3,iMa) + rMaGlob(3,iMaP) )
                     
                if (kE<0.5_prec) then
                    if ( isSubmerged(iMaP) ) then
                        subRatio(iMaP) = subRatio(iMaP) - (0.5_prec - kE)
                    else
                        subRatio(iMaP) = subRatio(iMap) + (0.5_prec - kE)
                    end if
                else
                    if ( isSubmerged(iMa) ) then
                        subRatio(iMa) = subRatio(iMa) - (kE - 0.5_prec)
                    else
                        subRatio(iMa) = subRatio(iMa) + (kE - 0.5_prec)
                    end if
                end if
            end if
        end do
        
        
        ! --- Drag force: normal direction ---
        nZB   = TB(:,3)
        do iMa = 1,nDivB
            uRel  = uFlow(:,iMa) - (qB_d(1:3) + crossProduct(omRB, TB(:,3)*zMaLoc(iMa)))
            uRelN = uRel/sqrt(sum(uRel**2))
            
            nNorm = crossProduct(crossProduct(nZB,uRelN),nZB)
            
            uNorm = dot_product(nNorm,uRel)
            
            fDN   = 0.5_prec*fld%rho*CDLat*uNorm**2*dB*lEl
            
            fEl   = subRatio(iMa)*(nNorm*fDN - VEL*fld%rho*gVec)
            
            fHydB(1:3) = fHydB(1:3) + fEl
                                      
            fHydB(4:6) = fHydB(4:6) + crossProduct( TB(:,3)*zMaLoc(iMa), fEl )
        end do
        
        
        ! --- Drag force: axial direction ---
        uRel = 0.5_prec*( uFlow(:,1    ) - (qB_d(1:3) + crossProduct(omRB, TB(:,3)*zMaLoc(1)    )) + &
                          uFlow(:,nDivB) - (qB_d(1:3) + crossProduct(omRB, TB(:,3)*zMaLoc(nDivB))) )
        
        uNorm = dot_product(nZB,uRel)
            
        fDN   = 0.5_prec*fld%rho*CDLong*uNorm**2*AEl
        
        fHydB(1:3) = fHydB(1:3) - nZB*fDN
        
    END FUNCTION
    
    
    !***************************************************************************
    ! Interpolate hydrodynamic coefficients
    !***************************************************************************
    real(prec) FUNCTION flmDyn__interpCoeffs1D (coeffs, theta)
        implicit none
        
        real(prec), intent(in) :: coeffs(:,:) ! List of hydrodynamic coefficient tables referenced by bar elements
        real(prec), intent(in) :: theta       ! Angle of attack
        
        integer :: iLo, iHi    ! Index of section, lower and higher boundary theta for lookup in table
        logical :: err
        
        
        err = bisecSearch(iLo,iHi,coeffs(:,1),theta)
        
        if (iLo==iHi) then
            ! Value equals a value given in table. Non-standard case.
            flmDyn__interpCoeffs1D = coeffs(iLo,2)
        else
            ! Value between two given values in table. Standard case.
            flmDyn__interpCoeffs1D = coeffs(iLo,2) + &
                (coeffs(iHi,2)-coeffs(iLo,2))*(theta-coeffs(iLo,1))/(coeffs(iHi,1)-coeffs(iLo,1))
        end if
        
    END FUNCTION flmDyn__interpCoeffs1D
    
    
END MODULE flm__Dyn
