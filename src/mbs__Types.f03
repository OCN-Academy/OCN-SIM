!*******************************************************************************
!   OCN-SIM Flex
!   Copyright (C) 2017 by Christoph Otto
!   christoph@ocnacademy.org
!   www.ocnacademy.org
!
!
!   MODULE :: Multibody basic data types
!
!   PURPOSE
!   Basic custom data types.
!
!   
!     ___   ____ _   _    ____ ___ __  __    _____ _     _______  __
!    / _ \ / ___| \ | |  / ___|_ _|  \/  |  |  ___| |   | ____\ \/ /
!   | | | | |   |  \| |  \___ \| || |\/| |  | |_  | |   |  _|  \  / 
!   | |_| | |___| |\  |   ___) | || |  | |  |  _| | |___| |___ /  \
!    \___/ \____|_| \_|  |____/___|_|  |_|  |_|   |_____|_____/_/\_\
!
!
!   LICENSE
!   This file is part of OCN-SIM Flex.
!
!   OCN-SIM Flex is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   OCN-SIM Flex is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with OCN-SIM Flex. If not, see <http://www.gnu.org/licenses/>.
!
!*******************************************************************************

MODULE mbs__Types
    
    USE liboe__Conf
    USE liboe__Types
    USE liboe__Err
    
    implicit none
    
    !***************************************************************************
    ! Module description
    !***************************************************************************
    character(*), parameter, private :: modName = 'mbs__Types'
    
    
    !***************************************************************************
    ! Public entities                                                          !
    !***************************************************************************
    PUBLIC :: structElem__Rigid
    PUBLIC :: sysElem__ConstrExpl
    PUBLIC :: sysElem__Constr, sysElem__ConstrSec, rtData__ConstraintSec
    PUBLIC :: forceElem__Spring, forceElem__PID, forceElem__PID_M
    
    PUBLIC :: mbsTypes__assembleRigids
    PUBLIC :: mbsData__str2refType
    
    PUBLIC :: SOL_EULEX, SOL_VERLE, SOL_RRF_1, SOL_RRF_2, SOL_MAX_T
    PUBLIC :: RBOD_SIMPLE_DAMP, RBOD_HYDROFOIL, RBOD_BUOY
    PUBLIC :: MTYPE__CSYS, MTYPE__FLEX, MTYPE__RBOD
    
    
    !***************************************************************************
    ! Global constants                                                         !
    !***************************************************************************
    ! --- Solver types ---
    ! -ATTENTION- update SOL_MAX_T when adding new solvers, since a check for a
    ! valid solver ID < SOL_MAX_T is performed in mbsData__checkConsistency
    integer, parameter :: SOL_EULEX = 1 ! Explicit Euler-method
    integer, parameter :: SOL_VERLE = 2 ! Velocity-Verlet integration
    integer, parameter :: SOL_RRF_1 = 3 ! RRF I
    integer, parameter :: SOL_RRF_2 = 4 ! RRF II
    
    integer, parameter :: SOL_MAX_T = 4 ! Maximum solver 
    
    ! --- Hydrodynamic load types ---
    integer, parameter :: RBOD_SIMPLE_DAMP = 0 ! Simple, constant coefficient based hydrodynamic damping
    integer, parameter :: RBOD_HYDROFOIL   = 1 ! Airfoil based on coefficients depending on angle of attack
    integer, parameter :: RBOD_BUOY        = 2 ! Partially submerged buoy
    
    
    ! --- Marker types ---
    integer(int8), parameter :: MTYPE__CSYS = 0 ! Coordinate system
    integer(int8), parameter :: MTYPE__FLEX = 1 ! Rope
    integer(int8), parameter :: MTYPE__RBOD = 2 ! Rigid body
    
PRIVATE
    
    !***************************************************************************
    ! Custom data types                                                        !
    !***************************************************************************
    
    ! --- Rigid body data type ---
    TYPE :: structElem__Rigid
        integer                 :: nMarkers         ! Number of markers on rigid body
        real(prec)              :: mass             ! Rigid body mass
        real(prec)              :: ITens(3,3)       ! Inertia tensor
        real(prec)              :: vol              ! Rigid body volume
        real(prec)              :: rCOB(3)          ! Local coordinates of center of buoyancy
        real(prec)              :: rR0(7)           ! Initial pose of body
        real(prec), allocatable :: markerPosns(:,:) ! Local marker positions. Dimensions(3,nMarkers)
        integer                 :: iNRef            ! Node ID of first marker on body. Each body needs to have at least one marker. Node IDs are defined w.r.t. first rigid body and assigned during system assembly
        integer                 :: loadType         ! Type of hydrodynamic loads acting on body. RBOD_SIMPLE_DAMP or RBOD_HYDROFOIL
        integer                 :: IDCT             ! ID of associated hydrodynamic coefficient table for loadType = RBOD_HYDROFOIL
        real(prec)              :: CD               ! Constant drag coefficient for loadType = RBOD_SIMPLE_DAMP
        real(prec)              :: CDLat            ! Constant lateral drag coefficient for loadType = RBOD_BUOY
        real(prec)              :: CDLong           ! Constant longitudinal drag coefficient for loadType = RBOD_BUOY
        real(prec)              :: A0               ! Cross sectional area
        
        real(prec)              :: dBuoy            ! Loadtype cylindrical buoy: Diameter
        real(prec)              :: lBuoy            ! Loadtype cylindrical buoy: Length
        integer                 :: nDivBuoy         ! Loadtype cylindrical buoy: Number of divisions for hydrodynamic load calculation
    END TYPE
    
    
    ! --- Primary constraint in explicit form ---
    TYPE :: sysElem__ConstrExpl
        integer       :: pred   ! Predecessor node
        
        integer(int8) :: predRT ! Predecessor node reference types (MTYPE__FLEX or MTYPE__CSYS0)
        real(prec)    :: length ! Element length
    END TYPE
    
    
    ! --- Primary constraint in implicit form ---
    ! !!! PITFALL-ALERT !!! Node k2 is always supposed to be MTYPE__FLEX
    !                       throughout the whole program!!!
    TYPE :: sysElem__Constr
        integer       :: k1
        integer       :: k2
        
        integer(int8) :: k1RT   ! Node 1 reference types (MTYPE__FLEX or MTYPE__CSYS0)
        integer(int8) :: k2RT   ! Node 2 reference types (Per definition, this is always MTYPE__FLEX!!!)
        real(prec)    :: length ! Element length
    END TYPE

    
    ! --- Loop closing secondary position constraint simulated by PID-controller ---
    TYPE :: sysElem__ConstrSec
        integer       :: k1     ! ID of node number 1 (must always be MTYPE__FLEX)
        integer       :: k2     ! ID of node number 2 (must always be MTYPE__FLEX)
    END TYPE
    
    
    ! --- Runtime data for loop closing secondary position constraint simulated by PID-controller ---
    TYPE :: rtData__ConstraintSec
        real(prec)             :: errI(3)     ! Integral of error in case component-based projection is chosen
        real(prec)             :: errI_M(2)   ! Integral of error in case magnitude-based projection is chosen
    END TYPE
    
    
    ! --- Force element: spring-damper ---
    TYPE :: forceElem__Spring
        real(prec) :: c   ! Stiffness of spring
        real(prec) :: d   ! Damping coefficient
        
        integer       :: k1     ! ID of node number 1
        integer       :: k2     ! ID of node number 2
        
        integer(int8) :: k1RT   ! Node 1 reference types (MTYPE__FLEX, MTYPE__RBOD or MTYPE__CSYS0)
        integer(int8) :: k2RT   ! Node 2 reference types (MTYPE__FLEX, MTYPE__RBOD or MTYPE__CSYS0)
    END TYPE
    
    
    ! --- Force element: PID (component based) ---
    TYPE :: forceElem__PID
        real(prec)    :: kp    ! Proportional part
        real(prec)    :: ki    ! Integral part
        real(prec)    :: kd    ! Differential part
        
        integer       :: k1     ! ID of node number 1
        integer       :: k2     ! ID of node number 2
        
        integer(int8) :: k1RT   ! Node 1 reference types (MTYPE__FLEX, MTYPE__RBOD or MTYPE__CSYS0)
        integer(int8) :: k2RT   ! Node 2 reference types (MTYPE__FLEX, MTYPE__RBOD or MTYPE__CSYS0)
        
        real(prec)    :: rt__errI(3)     ! Runtime information: Integral part of error
    END TYPE
    
    
    ! --- Force element: PID (magnitude based) ---
    TYPE :: forceElem__PID_M
        real(prec)    :: kp    ! Proportional part
        real(prec)    :: ki    ! Integral part
        real(prec)    :: kd    ! Differential part
        
        real(prec)    :: length ! Distance to be kept
        
        integer       :: k1     ! ID of node number 1
        integer       :: k2     ! ID of node number 2
        
        integer(int8) :: k1RT   ! Node 1 reference types (MTYPE__FLEX, MTYPE__RBOD or MTYPE__CSYS0)
        integer(int8) :: k2RT   ! Node 2 reference types (MTYPE__FLEX, MTYPE__RBOD or MTYPE__CSYS0)
        
        real(prec)    :: rt__errI   ! Runtime information: Integral part of error
    END TYPE

CONTAINS
    
    
    !***************************************************************************
    ! Assemble rigid bodies
    !***************************************************************************
    SUBROUTINE mbsTypes__assembleRigids( rigids, nMarkersRB, nRigids )
        implicit none
        
        type(structElem__Rigid), intent(inout) :: rigids(nRigids)
        integer,                 intent(out)   :: nMarkersRB
        integer,                 intent(in)    :: nRigids
        
        integer :: iRB
        
        
        ! Sum up number of markers on rigid bodies
        nMarkersRB = 0
        do iRB = 1,nRigids
            rigids(iRB)%iNRef = nMarkersRB + 1
            nMarkersRB        = nMarkersRB + rigids(iRB)%nMarkers
        end do
        
    END SUBROUTINE
    
    
    !***************************************************************************
    ! Convert entity string to marker refType (e.g. 'ROPE' to MTYPE__FLEX)
    !***************************************************************************
    logical FUNCTION mbsData__str2refType(refType, entity)
        implicit none
        
        integer(int8),                   intent(out) :: refType   ! Resulting marker type (MTYPE__CSYS, MTYPE__FLEX, MTYPE__RBOD)
        character(MAX_STRINGVAL_LENGTH), intent(in)  :: entity    ! Reference entity: "CSYS", "ROPE" or "RBODY"
        
        type(loeErr)              :: err ! Error handler
        
        err = loeErr__init('mbsData__str2refType',modName)
        mbsData__str2refType = .false.
        
        
        if (trim(entity)=='CSYS') then
            refType = MTYPE__CSYS
        
        else if (trim(entity)=='ROPE') then
            refType = MTYPE__FLEX
        
        else if (trim(entity)=='RBODY') then
            refType = MTYPE__RBOD
        
        else
            call loeErr__print( err, 'Illegal entity as marker reference body: ' // trim(entity) // endl //&
                                'Legal entities are: ' //quo// 'ROPE' //quo// ', ' //quo// 'CSYS' //quo// &
                                'and ' //quo// 'RBODY' //quo )
            return
        end if
        
        mbsData__str2refType = .true.
    END FUNCTION
END MODULE mbs__Types
