!*******************************************************************************
!   OCN-SIM Flex
!   Copyright (C) 2017 by Christoph Otto
!   christoph@ocnacademy.org
!   www.ocnacademy.org
!
!
!   MODULE :: Lagrangian dynamics based on implicit constraint equations
!
!   PURPOSE
!   Reconstructed reaction forces (RRF) based solvers
!
!   
!     ___   ____ _   _    ____ ___ __  __    _____ _     _______  __
!    / _ \ / ___| \ | |  / ___|_ _|  \/  |  |  ___| |   | ____\ \/ /
!   | | | | |   |  \| |  \___ \| || |\/| |  | |_  | |   |  _|  \  / 
!   | |_| | |___| |\  |   ___) | || |  | |  |  _| | |___| |___ /  \
!    \___/ \____|_| \_|  |____/___|_|  |_|  |_|   |_____|_____/_/\_\
!
!
!   LICENSE
!   This file is part of OCN-SIM Flex.
!
!   OCN-SIM Flex is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   OCN-SIM Flex is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with OCN-SIM Flex. If not, see <http://www.gnu.org/licenses/>.
!
!*******************************************************************************

MODULE sol__LAGR
    
    USE liboe__Conf
    USE liboe__Types
    USE mbs__Types
    
    implicit none
    
    PUBLIC :: solVars__LAGR
    
    PUBLIC :: solLAGR__initJacobian, solLAGR__calcJacobian
    PUBLIC :: solLAGR__calcInternalReations
    
PRIVATE

    TYPE :: solVars__LAGR
        real(prec), allocatable :: lambda (:)   ! Vector of Lagrange multipliers
        real(prec), allocatable :: work (:)     ! working array for LAPACK'S dgels solver
        real(prec), allocatable :: G(:,:)       ! Jacobian. Dimension(nNodes+3*nConstr, 3*nNodes)
        real(prec), allocatable :: G_T(:,:)     ! Transpose of Jacobian. Dimension Dimension(3*nNodes, nNodes+3*nConstr)
        
        
    END TYPE

CONTAINS
    
    
    !***************************************************************************
    ! Allocate Jacobian matrix
    !***************************************************************************
    PURE SUBROUTINE solLAGR__initJacobian(this, nNodes, nConstr, constrSec, nConstrSec)
        ! Description___________________________________________________________
        !   Allocate Jacobian matrix and setup Secondary constraints
        ! ______________________________________________________________________
        implicit none
        
        type(solVars__LAGR),      intent(inout) :: this
        
        integer,                  intent(in)    :: nNodes
        integer,                  intent(in)    :: nConstr
        
        type(sysElem__ConstrSec), intent(in)    :: constrSec(nConstrSec)
        integer,                  intent(in)    :: nConstrSec
        
        integer :: iCo        ! ID of current constraint
        
        
        allocate(this%G(nConstr+3*nConstrSec,3*nNodes))
        allocate(this%G_T(3*nNodes,nConstr+3*nConstrSec))
        allocate(this%lambda(3*nNodes))
        
        allocate(this%work(5*nNodes))
        
        
        this%G(:,:) = ZE
        
        do iCo = 1,nConstrSec
            associate ( iN1 => constrSec(iCo)%k1, &
                        iN2 => constrSec(iCo)%k2 )
                this%G(nNodes+3*iCo-2 : nNodes+3*iCo, 3*iN1-2:3*iN1) =  0.5*I3
                this%G(nNodes+3*iCo-2 : nNodes+3*iCo, 3*iN2-2:3*iN2) = -0.5*I3
            end associate
        end do
        
    END SUBROUTINE solLAGR__initJacobian
    
    
    !***************************************************************************
    ! Project constraints iteratively
    !***************************************************************************
    PURE SUBROUTINE solLAGR__calcJacobian(G,rN,rCS,constr,nConstr)
        ! Description___________________________________________________________
        !   Updates Jacobian for given position vector. Secondary constraints
        !   are setup in solLAGR__initJacobian
        ! ______________________________________________________________________
        implicit none
        
        real(prec),            intent(inout) :: G(:,:)          ! Jacobian of implicit constraints. Dimension(nConstr+3*nConstrSec, 3*nNodes)
        real(prec),            intent(inout) :: rN(:,:)         ! Vector of node positions. Dimension(3,nNodes)
        real(prec),            intent(in)    :: rCS(:,:)        ! Vector of reference coordinate system marker positions. Dimension(3,nMarkersCSYS0)
        type(sysElem__Constr), intent(in)    :: constr(nConstr)
        integer,               intent(in)    :: nConstr
        
        integer    :: iCo           ! ID of current constraint
        real(prec) :: constrVec(3)  ! Vector pointing from node pred(iN) to node iN
        
        
        do iCo = 1,nConstr
            associate ( k1 => constr(iCO)%k1, k2 => constr(iCO)%k2 )
            
                if ( constr(iCo)%k1RT == MTYPE__FLEX) then
                    constrVec = 2.0_prec*(rN(:,k2) - rN(:,k1))
                    
                    G(iCo, 3*k2-2:3*k2) =  constrVec
                    G(iCo, 3*k1-2:3*k1) = -constrVec
                else
                    constrVec = 2.0_prec*(rN(:,k2) - rCS(:,k1))
                    
                    G(iCo, 3*k2-2:3*k2) = constrVec
                end if

            end associate
        end do

    END SUBROUTINE solLAGR__calcJacobian
    
    
    !***************************************************************************
    ! Calculate internal reaction forces of flexible bodies
    !***************************************************************************
    SUBROUTINE solLAGR__calcInternalReations( this, fBRFB, rMaFB_dd, fEFB, mFB, &
                                              nNodes, constr, nConstr, nConstrSec )
        
        implicit none
        
        type(solVars__LAGR),   intent(inout) :: this
        
        real(prec),            intent(out)   :: fBRFB(3,nNodes)    ! Vector of flexible body internal reaction forces
        
        real(prec),            intent(in)    :: rMaFB_dd(3,nNodes) ! Vector of node accelerations
        real(prec),            intent(in)    :: fEFB(3,nNodes)     ! Vector of flexible body external forces
        real(prec),            intent(in)    :: mFB(nNodes)        ! Node masses
        
        integer,               intent(in)    :: nNodes
        
        type(sysElem__Constr), intent(in)    :: constr(nConstr)
        integer,               intent(in)    :: nConstr
        
        integer,               intent(in)    :: nConstrSec
        
        integer :: iNode
        integer :: iCo
        integer :: iFlag
        
        
        do iNode = 1,nNodes
            this%lambda(3*iNode-2:3*iNode) = mFB(iNode)*rMaFB_dd(:,iNode) - fEFB(:,iNode)
        end do
        
        
        call dgels( 'N', 3*nNodes, nConstr+3*nConstrSec, 1, this%G_T, 3*nNodes, &
                    this%lambda , 3*nNodes, this%work, 5*nNodes, iFlag )
        
        if (iFlag /= 0) then
            print *, "Error calculating internal reaction forces. Aborting. Error code:"
            print *, iFlag
            call exit
        end if
        
        ! Using matrix G instead of G_T here, because G_T is screwed up during dgels
        ! TODO: indexing can impossibly b right here. Just guessed something...
        do iCo = 1,nConstr
            fBRFB(:,iCo) = this%G(iCo, 3*constr(iCo)%k2-2 : 3*constr(iCo)%k2)*&
                           this%lambda(iCO)
        end do
    
    END SUBROUTINE
    
    
END MODULE 
