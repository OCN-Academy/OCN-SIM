!*******************************************************************************
!   OCN-SIM Flex
!   Copyright (C) 2017 by Christoph Otto
!   christoph@ocnacademy.org
!   www.ocnacademy.org
!
!
!   MODULE :: Reader for multibody model data for systems of ropes
!
!   PURPOSE
!   Custom data type for the structural dynamic elements. Contains routines
!   to load data from file, check consistency and assemble the single elements.
!
!   
!     ___   ____ _   _    ____ ___ __  __    _____ _     _______  __
!    / _ \ / ___| \ | |  / ___|_ _|  \/  |  |  ___| |   | ____\ \/ /
!   | | | | |   |  \| |  \___ \| || |\/| |  | |_  | |   |  _|  \  / 
!   | |_| | |___| |\  |   ___) | || |  | |  |  _| | |___| |___ /  \
!    \___/ \____|_| \_|  |____/___|_|  |_|  |_|   |_____|_____/_/\_\
!
!
!   LICENSE
!   This file is part of OCN-SIM Flex.
!
!   OCN-SIM Flex is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   OCN-SIM Flex is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with OCN-SIM Flex. If not, see <http://www.gnu.org/licenses/>.
!
!******************************************************************************* 

MODULE rdr__RopeSys
    
    USE liboe__Conf
    USE liboe__File
    USE liboe__Types
    USE liboe__Err
    USE liboe__Tools
    USE mbs__Data
    USE mbs__Types
    USE mbs__Tools
    
    implicit none
    
    
    !***************************************************************************
    ! Module description                                                       !
    !***************************************************************************
    character(*), parameter, private :: modName = 'rdr__RopeSys'
    
    
    !***************************************************************************
    ! Public entities                                                          !
    !***************************************************************************
    PUBLIC :: rdrRopeSys__loadFromFile
    
    
PRIVATE


    !***************************************************************************
    ! Helper Types for loading data from rope data
    !***************************************************************************
    ! --- Marker ---
    TYPE :: mbdReader_Marker
        integer(int8) :: refType    ! Type of marker (MTYPE__CSYS, MTYPE__FLEX, MTYPE__RBOD)
        integer       :: refBody    ! ID of reference body
        integer       :: refMarker  ! ID of reference marker
    END TYPE
    
    
    ! --- Rope data type ---
    TYPE :: mbdReader__Rope
        real(prec)             :: length    ! Length of rope
        integer                :: nElems    ! Numbers of elements of rope
        real(prec)             :: diam      ! Diameter of rope
        real(prec)             :: rho       ! Density of rope material
        integer                :: IDCT      ! ID of associated hydrodynamic coefficient table
        type(mbdReader_Marker) :: targJoint ! Target joint
        real(prec)             :: alpha0    ! Initial joint angle alpha
        real(prec)             :: beta0     ! Initial joint angle beta
        real(prec)             :: alpha_d0  ! Initial joint velocity alpha_d
        real(prec)             :: beta_d0   ! Initial joint velocity beta_d
        integer                :: iNRef     ! ID of first node of the rope
        integer                :: iNLast    ! ID of last node of the rope
    END TYPE
    
    ! --- Loop closing secondary position constraint simulated by PID-controller ---
    TYPE :: mbdReader__ConstrSec
        type(mbdReader_Marker) :: targ1 ! First node involved in constraint
        type(mbdReader_Marker) :: targ2 ! Second node involved in constraint
    END TYPE
    
    
    ! --- Force element: spring-damper ---
    TYPE :: mbdReader__Spring
        real(prec) :: c                 ! Stiffness of spring
        real(prec) :: d                 ! Damping coefficient
        type(mbdReader_Marker) :: targ1 ! First node involved in constraint
        type(mbdReader_Marker) :: targ2 ! Second node involved in constraint
    END TYPE
    
    
    ! --- Force element: PID (component based) ---
    TYPE :: mbdReader__PID
        real(prec)             :: kp    ! Proportional part
        real(prec)             :: ki    ! Integral part
        real(prec)             :: kd    ! Differential part
        type(mbdReader_Marker) :: targ1 ! First node involved in constraint
        type(mbdReader_Marker) :: targ2 ! Second node involved in constraint
    END TYPE
    
    
CONTAINS
    
    !***************************************************************************
    ! Load configuration from file
    !***************************************************************************
    logical FUNCTION rdrRopeSys__loadFromFile(this, q0, v0, constrExpl, nConstrExpl, fName)
        implicit none
        
        type(mbsData),                          intent(out)   :: this  ! mbsData structure to be read
        
        real(prec),                allocatable, intent(inout) :: q0(:) ! Initial joint positions
        real(prec),                allocatable, intent(inout) :: v0(:) ! Initial joint velocities
        
        type(sysElem__ConstrExpl), allocatable, intent(inout) :: constrExpl(:)
        integer,                                intent(out)   :: nConstrExpl
        
        character(*),                           intent(in)    :: fName ! Name of input file
        
        
        integer                                   :: nRopes   ! Total number of ropes
        type(mbdReader__Rope),        allocatable :: ropes(:) ! Rope elements
        
        integer                                   :: nConstrSec       ! Number of loop closing secondary constraints in system
        type(mbdReader__ConstrSec),   allocatable :: constrSec(:)     ! Loop closing secondary position constraints simulated by PID-controller
        
        integer                                   :: nSprings   ! Number of springs in system
        type(mbdReader__Spring),      allocatable :: springs(:) ! Spring elements
        
        integer                                   :: nPIDs   ! Number of PID elements in system
        type(mbdReader__PID),         allocatable :: pids(:) ! PID elements
        
        type(loeFile)                             :: inFile         ! Input file
        integer                                   :: iRo, iRoCurr   ! Index of current rope
        integer                                   :: iRB, iRBCurr   ! Index of current rigid body
        integer                                   :: iM, iMcurr     ! Index of current marker
        integer                                   :: iCS, iCSCurr   ! Index of current CSYS
        integer                                   :: iSp, iSpCurr   ! Index of current spring element
        integer                                   :: iCo, iCoCurr   ! Index of current constraint
        integer                                   :: iP,  iPCurr    ! Index of current PID element
        character(MAX_STRINGVAL_LENGTH)           :: entity         ! Target entity: "CSYS", "ROPE" or "RBODY"
        real(prec)                                :: r0(3)          ! Initial translational position of rigid body
        real(prec)                                :: phi0(3)        ! Initial xyz-Euler-angles of rigid body
        logical                                   :: allValuesRead  ! Status flag: .false. until all values are successfully read from file
        
        type(loeErr) :: err ! Error handling class
        
        err                        = loeErr__init('rdrRopeSys__loadFromFile',modName)
        rdrRopeSys__loadFromFile = .false.
        
        
        
        if (.not. loeFile__open(inFile,trim(fName))) return
        
        ! Emulated try-finally block using do-loop. Compare programmer's guide.
        allValuesRead = .false.
        readVals: do
            
            ! --- Substitution variables ---------------------------------------
            ! ------------------------------------------------------------------ 
            if (.not. loeFile__readSectionStart(inFile, 'SUBVARS')) exit readVals           
            if (.not. loeFile__readSubVars(inFile))                 exit readVals
            
            
            ! --- Inertial frame CSYS0 -----------------------------------------
            ! ------------------------------------------------------------------
            if (.not. loeFile__readSectionStart(inFile, 'CSYS')) exit readVals
            iCS = 0
            if (.not. loeFile__readEID(inFile, iCSCurr, iCS, 'CSYS'))       exit readVals ! Current element ID 
            if (.not. loeFile__readNEl(inFile, this%nMaCS, 1, 'nMarkers'))  exit readVals ! Number of markers in CSYS0
            
            ! Read individual marker positions
            allocate(this%rMaCS(3,this%nMaCS))
            do iM = 1,this%nMaCS
                if (.not. loeFile__readEID(inFile, iMCurr, iM, 'Marker'))                    exit readVals ! Current element ID
                if (.not. loeFile__readVector(inFile,this%rMaCS(:,iM),3,'rMa')) exit readVals
            end do
            
            
            ! --- Parameters of rope elements ----------------------------------
            ! ------------------------------------------------------------------
            if (.not. loeFile__readSectionStart(inFile, 'ROPES'))         exit readVals
            if (.not. loeFile__readNEl(inFile, nRopes, 1, 'nRopes')) exit readVals ! Number of ropes
            
            ! Data sets
            allocate(ropes(nRopes))
            do iRo = 1,nRopes
                if (.not. loeFile__readEID(inFile, iRoCurr, iRo,      'ROPE'   )) exit readVals ! Current rope ID
                if (.not. loeFile__readVal(inFile, ropes(iRo)%length, 'length' )) exit readVals ! Length of rope
                if (.not. loeFile__readVal(inFile, ropes(iRo)%diam,   'diam'   )) exit readVals ! Diameter of rope
                if (.not. loeFile__readVal(inFile, ropes(iRo)%rho,    'rho'    )) exit readVals ! Density of rope material
                if (.not. loeFile__readVal(inFile, ropes(iRo)%nElems, 'nElems' )) exit readVals ! Number of elements
                
                if (.not. loeFile__readNumberedEntity(inFile,entity, &
                    ropes(iRo)%targJoint%refBody,'joint.refBody'                    )) exit readVals ! ID of rope 1, at which constraint is applied
                
                if (.not. mbsData__str2refType(ropes(iRo)%targJoint%refType, entity )) exit readVals
                
                if (.not. loeFile__readVal(inFile, &
                    ropes(iRo)%targJoint%refMarker,  'joint.refMarker'              )) exit readVals ! Marker on rope 1, at which constraint is applied
                
                if (.not. loeFile__readVal(inFile, ropes(iRo)%alpha0,   'alpha0'   )) exit readVals ! Initial joint angle alpha0
                if (.not. loeFile__readVal(inFile, ropes(iRo)%beta0,    'beta0'    )) exit readVals ! Initial joint angle beta0
                if (.not. loeFile__readVal(inFile, ropes(iRo)%alpha_d0, 'alpha_d0' )) exit readVals ! Initial joint angle alpha0
                if (.not. loeFile__readVal(inFile, ropes(iRo)%beta_d0,  'beta_d0'  )) exit readVals ! Initial joint angle beta0
                
                if (.not. loeFile__readVal(inFile, ropes(iRo)%IDCT,   'IDCTable')) exit readVals ! ID of hydrodynamic coefficient table
            end do
            
            
            ! --- Rigid bodies -------------------------------------------------
            ! ------------------------------------------------------------------
            if (.not. loeFile__readSectionStart(inFile, 'RBODIES'))       exit readVals
            if (.not. loeFile__readNEl(inFile,this%nRigids,0, 'nRigids')) exit readVals    ! Number of rigid bodies in system
            
            ! Data sets
            allocate(this%rigids(this%nRigids))
            do iRB = 1,this%nRigids
                if(.not. loeFile__readEID(inFile, iRBCurr, iRB,          'RBODY' ))             exit readVals ! Current rigid body ID
                if(.not. loeFile__readVal(inFile, this%rigids(iRB)%mass, 'mass'  ))             exit readVals ! Current rigid body mass
                if(.not. loeFile__readVector(inFile, this%rigids(iRB)%ITens(1,:), 3, 'I_1'  ))  exit readVals ! First row of the inertia tensor
                if(.not. loeFile__readVector(inFile, this%rigids(iRB)%ITens(2,:), 3, 'I_2'  ))  exit readVals ! First row of the inertia tensor
                if(.not. loeFile__readVector(inFile, this%rigids(iRB)%ITens(3,:), 3, 'I_3'  ))  exit readVals ! First row of the inertia tensor
                
                if(.not. loeFile__readVal(inFile, this%rigids(iRB)%vol,  'vol'   ))             exit readVals ! Current rigid body volume
                if(.not. loeFile__readVector(inFile, this%rigids(iRB)%rCOB,3, 'rCOB'))          exit readVals
                
                if(.not. loeFile__readVector(inFile, r0,   3, 'r0'   )) exit readVals ! Initial pose
                if(.not. loeFile__readVector(inFile, phi0, 3, 'phi0' )) exit readVals ! Initial pose
                
                this%rigids(iRB)%rR0 =  [r0,mbsTools__xyzEulAng2EulPar(phi0*(PI/180.0))]
                
                if(.not. loeFile__readNEl(inFile,this%rigids(iRB)%nMarkers,1,'nMarkers')) exit readVals ! Number of markers
                
                if (allocated(this%rigids(iRB)%markerPosns)) deallocate(this%rigids(iRB)%markerPosns)
                allocate(this%rigids(iRB)%markerPosns(3,this%rigids(IRB)%nMarkers))
                
                if(.not. loeFile__readVector(inFile, this%rigids(iRB)%markerPosns(1,:),&
                                                     this%rigids(IRB)%nMarkers,'rMa_x')) exit readVals ! X-coordinates of markers
                if(.not. loeFile__readVector(inFile, this%rigids(iRB)%markerPosns(2,:),&
                                                     this%rigids(IRB)%nMarkers,'rMa_y')) exit readVals ! Y-coordinates of markers
                if(.not. loeFile__readVector(inFile, this%rigids(iRB)%markerPosns(3,:),&
                                                     this%rigids(IRB)%nMarkers,'rMa_z')) exit readVals ! Z-coordinates of markers
                
                if(.not. loeFile__readVal(inFile, this%rigids(iRB)%loadType, 'loadType')) exit readVals ! Type of loads acting on body
                
                select case (this%rigids(iRB)%loadType)
                
                case (RBOD_SIMPLE_DAMP)
                    if(.not. loeFile__readVal(inFile, this%rigids(iRB)%CD, 'CD')) exit readVals ! Damping ratio
                    if(.not. loeFile__readVal(inFile, this%rigids(iRB)%A0, 'A0')) exit readVals ! ID of hydrodynamic coefficient table
                
                case (RBOD_HYDROFOIL)
                    if (.not. loeFile__readVal(inFile, this%rigids(iRB)%IDCT, 'IDCTable')) exit readVals ! ID of hydrodynamic coefficient table
                    if (.not. loeFile__readVal(inFile, this%rigids(iRB)%A0,   'A0'))       exit readVals ! ID of hydrodynamic coefficient table
                
                case (RBOD_BUOY)
                    if (.not. loeFile__readVal(inFile, this%rigids(iRB)%lBuoy,    'lBuoy'   )) exit readVals ! ID of hydrodynamic coefficient table
                    if (.not. loeFile__readVal(inFile, this%rigids(iRB)%dBuoy,    'dBuoy'   )) exit readVals ! ID of hydrodynamic coefficient table
                    if (.not. loeFile__readVal(inFile, this%rigids(iRB)%CDLat,    'CDLat'   )) exit readVals ! ID of hydrodynamic coefficient table
                    if (.not. loeFile__readVal(inFile, this%rigids(iRB)%CDLong,   'CDLong'  )) exit readVals ! ID of hydrodynamic coefficient table
                    if (.not. loeFile__readVal(inFile, this%rigids(iRB)%nDivBuoy, 'nDivBuoy')) exit readVals ! ID of hydrodynamic coefficient table
                
                case default
                    call loeErr__print(err,'Illegal load type specified (' &
                        // trim(num2Str(this%rigids(iRB)%loadType)) // ')' // endl // &
                       ' for rigid body #' // trim(num2Str(iRB)) // &
                       '. Load type must be 0 for simple damping  or 1 for hydrofoil.')
                
                end select
                
            end do
            
            ! --- Implicit constraints -----------------------------------------
            ! ------------------------------------------------------------------
            if (.not. loeFile__readSectionStart(inFile, 'CONSTRAINTS'))   exit readVals
            if (.not. loeFile__readNEl(inFile, nConstrSec, 0, 'nConstr')) exit readVals ! Number of loop-closing constraints
            
            ! Data sets
            allocate(constrSec(nConstrSec))
            do iCo = 1,nConstrSec
                if (.not. loeFile__readEID(inFile, iCoCurr,iCo,'CONSTR')) exit readVals ! Current constraint ID
                
                if (.not. loeFile__readNumberedEntity(inFile,entity, &
                    constrSec(iCo)%targ1%refBody,'node1.refBody'                    )) exit readVals ! ID of rope 1, at which constraint is applied
                if (.not. mbsData__str2refType(constrSec(iCo)%targ1%refType, entity )) exit readVals
                if (.not. loeFile__readVal(inFile, &
                    constrSec(iCo)%targ1%refMarker,  'node1.refMarker'              )) exit readVals ! Marker on rope 1, at which constraint is applied
                
                if (.not. loeFile__readNumberedEntity(inFile,entity, &
                    constrSec(iCo)%targ2%refBody,'node2.refBody'                    )) exit readVals ! ID of rope 1, at which constraint is applied
                if (.not. mbsData__str2refType(constrSec(iCo)%targ2%refType, entity )) exit readVals
                if (.not. loeFile__readVal(inFile, &
                    constrSec(iCo)%targ2%refMarker,  'node2.refMarker'              )) exit readVals ! Marker on rope 1, at which constraint is applied
            end do
            
            
            ! --- Parameters of springs ----------------------------------------
            ! ------------------------------------------------------------------
            if (.not. loeFile__readSectionStart(inFile, 'SPRINGS'))      exit readVals
            if (.not. loeFile__readNEl(inFile, nSprings, 0, 'nSprings')) exit readVals ! Number of spring elements
            
            ! Data sets
            allocate(springs(nSprings))
            do iSp = 1,nSprings
                if (.not. loeFile__readEID(inFile, iSpCurr,iSp,'SPRING' )) exit readVals ! Current element ID
                if (.not. loeFile__readVal(inFile, springs(iSP)%c,  'c' )) exit readVals ! Stiffness
                if (.not. loeFile__readVal(inFile, springs(iSP)%d,  'd' )) exit readVals ! Damping
                
                if (.not. loeFile__readNumberedEntity(inFile,entity, &
                    springs(iSP)%targ1%refBody,'node1.refBody'                    )) exit readVals
                if (.not. mbsData__str2refType(springs(iSP)%targ1%refType, entity )) exit readVals
                if (.not. loeFile__readVal(inFile, &
                    springs(iSP)%targ1%refMarker,  'node1.refMarker'              )) exit readVals
                
                if (.not. loeFile__readNumberedEntity(inFile,entity, &
                    springs(iSP)%targ2%refBody,'node2.refBody'                    )) exit readVals
                if (.not. mbsData__str2refType(springs(iSP)%targ2%refType, entity )) exit readVals
                if (.not. loeFile__readVal(inFile, &
                    springs(iSP)%targ2%refMarker,  'node2.refMarker'              )) exit readVals
            end do
            
            
            ! --- Parameters of PID elements -----------------------------------
            ! ------------------------------------------------------------------
            if (.not. loeFile__readSectionStart(inFile,  'PIDS' )) exit readVals
            if (.not. loeFile__readNEl(inFile, nPIDs, 0, 'nPIDs')) exit readVals
            
            ! Data sets
            allocate(pids(nPIDs))
            do iP = 1,nPIDs
                if (.not. loeFile__readEID(inFile, iPCurr, iP, 'PID')) exit readVals ! Current element ID
                if (.not. loeFile__readVal(inFile, pids(iP)%kp, 'kp')) exit readVals
                if (.not. loeFile__readVal(inFile, pids(iP)%ki, 'ki')) exit readVals
                if (.not. loeFile__readVal(inFile, pids(iP)%kd, 'kd')) exit readVals
                
                if (.not. loeFile__readNumberedEntity(inFile,entity, &
                    pids(iP)%targ1%refBody,'node1.refBody'                    )) exit readVals
                if (.not. mbsData__str2refType(pids(iP)%targ1%refType, entity )) exit readVals
                if (.not. loeFile__readVal(inFile, &
                    pids(iP)%targ1%refMarker,  'node1.refMarker'              )) exit readVals
                
                if (.not. loeFile__readNumberedEntity(inFile,entity, &
                    pids(iP)%targ2%refBody,'node2.refBody'                    )) exit readVals
                if (.not. mbsData__str2refType(pids(iP)%targ2%refType, entity )) exit readVals
                if (.not. loeFile__readVal(inFile, &
                    pids(iP)%targ2%refMarker,  'node2.refMarker'              )) exit readVals
            end do
            
            ! --- Set flag for success and finish reading section --------------
            ! ------------------------------------------------------------------
            allValuesRead = .true.
            exit readVals
        end do readVals
        
        call loeFile__close(inFile)
        
        
        ! Handle errors_________________________________________________________
        ! ______________________________________________________________________
        ! Print error and contents of line in input file causing the error
        if (.not. allValuesRead) then
            call loeErr__print(err, 'Error in input file ' // fName // ' in line #' // trim(num2Str(inFile%lineNum)) // ': ' // &
                                     endl // ' ' //quo// trim(inFile%lStr) // quo)
            return
        end if
        
        
        ! Glue it all together__________________________________________________        
        ! ______________________________________________________________________
        
        call mbsTypes__assembleRigids( this%rigids, this%nMarkersRB, this%nRigids )
        
        call rdrRopeSys__assembleRopes( this%nNodes, ropes, nRopes, constrExpl, nConstrExpl )
        
        if (mbsData__checkConsistency ( this%nMaCS, ropes, nRopes, constrSec, nConstrSec, &
                                        springs, nSprings, pids, nPIDs, this%rigids, this%nRigids ) > 0) then
            call loeErr__print(err, 'Terminating due to inconsistent multibody data')
            return
        end if
        
        
        ! --- Copy element data ---
        allocate(this%springs(nSprings))
        this%nSprings = nSprings
        
        do iSp = 1,nSprings
            this%springs(iSp)%c     = springs(iSp)%c
            this%springs(iSp)%d     = springs(iSp)%d
                        
            this%springs(iSp)%k1RT  = springs(iSp)%targ1%refType
            this%springs(iSp)%k1    = mbsData__marker2NodeID( springs(iSp)%targ1, ropes, nRopes, &
                                                              this%rigids,this%nRigids )
            
            this%springs(iSp)%k2RT  = springs(iSp)%targ2%refType
            this%springs(iSp)%k2    = mbsData__marker2NodeID( springs(iSp)%targ2, ropes, nRopes, &
                                                              this%rigids,this%nRigids )
        end do
        
        deallocate(springs)
        
        
        allocate(this%constrSec(nConstrSec))
        this%nConstrSec = nConstrSec
        
        do iCo = 1,nConstrSec
            this%constrSec(iCo)%k1    = mbsData__marker2NodeID(constrSec(iCo)%targ1, ropes, nRopes, &
                                                              this%rigids,this%nRigids )
                                                              
            this%constrSec(iCo)%k2    = mbsData__marker2NodeID(constrSec(iCo)%targ2, ropes, nRopes, &
                                                              this%rigids,this%nRigids )
        end do
        
        deallocate(constrSec)
        
        
        allocate(this%pids(nPIDs))
        this%nPIDs = nPIDs
        
        do iP = 1,nPIDs
            this%pids(iP)%kp    = pids(iP)%kp
            this%pids(iP)%ki    = pids(iP)%ki
            this%pids(iP)%kd    = pids(iP)%kd
            
            this%pids(iP)%k1RT  = pids(iP)%targ1%refType
            this%pids(iP)%k1    = mbsData__marker2NodeID( pids(iP)%targ1, ropes, nRopes, &
                                                          this%rigids,this%nRigids )
            
            this%pids(iP)%k2RT  = pids(iP)%targ2%refType
            this%pids(iP)%k2    = mbsData__marker2NodeID( pids(iP)%targ2, ropes, nRopes, &
                                                          this%rigids,this%nRigids )
        end do
        
        deallocate(pids)
                
        
        ! --- Convert primary constraints to implicit form ---
        ! Remark: Until here, we only needed constraints in explicit form
        allocate(this%constr(nConstrExpl))
        this%nConstr = nConstrExpl
        do iCo = 1,this%nConstr
            this%constr(iCo)%k1     = constrExpl(iCo)%pred
            this%constr(iCo)%k2     = iCo
            
            this%constr(iCo)%k1RT   = constrExpl(iCo)%predRT
            this%constr(iCo)%k2RT   = MTYPE__FLEX
            
            this%constr(iCo)%length = constrExpl(iCo)%length
        end do
        
        
        ! --- Force elements ---
        ! This one needs constraints in implicit form
        call mbsData__initForceElemsAndVolMassFromRopes(this, ropes, nRopes)
        
        
        ! --- Remaining initialisations ---
        allocate (q0(2*this%nNodes), v0(2*this%nNodes))
        q0(:) = ZE
        v0(:) = ZE
        
        do iRo = 1,nRopes
            q0(2*(ropes(iRo)%iNRef-1)+1) = ropes(iRo)%alpha0*PI/180.0
            q0(2*(ropes(iRo)%iNRef-1)+2) = ropes(iRo)%beta0*PI/180.0
            v0(2*(ropes(iRo)%iNRef-1)+1) = ropes(iRo)%alpha_d0*PI/180.0
            v0(2*(ropes(iRo)%iNRef-1)+2) = ropes(iRo)%beta_d0*PI/180.0
        end do
        
        
        ! Finalisations_________________________________________________________
        ! ______________________________________________________________________
        deallocate(ropes)
        rdrRopeSys__loadFromFile = .true.
    END FUNCTION rdrRopeSys__loadFromFile
    
    
    !***************************************************************************
    ! Assemble ropes and generate primary constraints in explicit form
    !***************************************************************************
    SUBROUTINE rdrRopeSys__assembleRopes( nNodes, ropes, nRopes, constrExpl, nConstr )
        implicit none
        
        integer,                                intent(out)   :: nNodes        ! Total number of nodes
        
        type(mbdReader__Rope),                  intent(inout) :: ropes(nRopes) ! Rope elements
        integer,                                intent(in)    :: nRopes        ! Total number of ropes
        
        type(sysElem__ConstrExpl), allocatable, intent(inout) :: constrExpl(:) ! Primary constraints in explicit form
        integer,                                intent(out)   :: nConstr       ! Total number of primary constraints
        
        integer :: iNode ! ID of current node
        integer :: iR    ! ID of current rope
        
        
        ! General initialisations_______________________________________________
        ! ______________________________________________________________________
        ! Sum up number of nodes and calculate offsets of local element IDs.
        nNodes = 0
        do iR = 1,nRopes
            ropes(iR)%iNRef  = nNodes + 1
            ropes(iR)%iNLast = nNodes + ropes(iR)%nElems
            nNodes           = nNodes + ropes(iR)%nElems
        end do
        
        
        ! Setup flexible bodies_________________________________________________
        !   Insert branches. Determine predecessor depending on whether rope is
        !   attached to another rope or marker in CSYS0. Determine node masses.
        ! ______________________________________________________________________
        ! Basic setup of predecessor nodes for primary constraints in explicit form
        nConstr = nNodes
        
        allocate (constrExpl(nConstr))
        do iNode = 1,nConstr
            constrExpl(iNode)%pred   = iNode-1
            constrExpl(iNode)%predRT = MTYPE__FLEX
        end do
        
        ! Insert branches in predecessor nodes
        do iR = 1,nRopes; associate (rope => ropes(iR))
            constrExpl(rope%iNRef)%predRT = rope%targJoint%refType
            
            select case(rope%targJoint%refType)
                case (MTYPE__FLEX)
                    constrExpl(rope%iNRef)%pred = &
                        ropes(rope%targJoint%refBody)%iNRef-1 + &
                        rope%targJoint%refMarker
                        
                case (MTYPE__CSYS)
                    constrExpl(rope%iNRef)%pred = rope%targJoint%refMarker
            end select
        end associate; end do
        
        do iR = 1,nRopes; associate (rope => ropes(iR))
            constrExpl(rope%iNRef:rope%iNLast)%length = rope%length/rope%nElems
        end associate; end do

        
    END SUBROUTINE rdrRopeSys__assembleRopes
    
    
    !***************************************************************************
    ! Converts marker (refBody+refMarker to node ID
    !***************************************************************************
    FUNCTION mbsData__marker2NodeID( marker, ropes, nRopes, rigids, nRigids ) RESULT(nodeID)
        implicit none
        
        integer                             :: nodeID
        
        type(mbdReader_Marker),  intent(in) :: marker
        
        type(mbdReader__Rope),   intent(in) :: ropes(nRopes) ! Rope elements
        integer,                 intent(in) :: nRopes        ! Total number of ropes
        
        type(structElem__Rigid), intent(in) :: rigids(nRigids) ! Rigid bodies
        integer,                 intent(in) :: nRigids         ! Total number of rigid bodies
        
            
        select case(marker%refType)
            case(MTYPE__CSYS)
                nodeID = marker%refMarker
                
            case(MTYPE__FLEX)
                nodeID = ropes(marker%refBody)%iNRef-1  + marker%refMarker
            
            case(MTYPE__RBOD)
                nodeID = rigids(marker%refBody)%iNRef-1 + marker%refMarker
        end select
    
    END FUNCTION
    
    
    !***************************************************************************
    ! Initialise force elements based upon ropes data
    !***************************************************************************
    SUBROUTINE mbsData__initForceElemsAndVolMassFromRopes(this, ropes, nRopes)
        implicit none
        
        type(mbsData),         intent(inout) :: this
        type(mbdReader__Rope), intent(in)    :: ropes(nRopes) ! Rope elements
        integer,               intent(in)    :: nRopes        ! Total number of ropes
        
        integer :: iCo    ! Current constraint ID
        integer :: paRope ! Parent rope of node
        
        
        ! Mass and volume based on bar type force elements ---
        allocate (this%mFB(this%nNodes))
        allocate (this%vFB(this%nNodes))
        this%vFB(:) = ZE
        this%mFB(:) = ZE
        
        allocate(this%fElems__bar(this%nConstr))
        this%nBars = this%nConstr
        
        ! Copy force element lists from constraints
        do iCo = 1,this%nConstr
            associate (bar=>this%fElems__bar(iCo))
                bar%l     = this%constr(iCo)%length
                bar%k1    = this%constr(iCo)%k1
                bar%k1RT  = this%constr(iCo)%k1RT
                bar%k2    = this%constr(iCo)%k2
                bar%k2RT  = this%constr(iCo)%k2RT
                
                paRope    = findRope(this%constr(iCo)%k2)
                bar%d     = ropes(paRope)%diam
                bar%AProj = bar%l*bar%d
                bar%IDCT  = ropes(paRope)%IDCT
                
                if (bar%k1RT==MTYPE__FLEX) then
                    this%vFB(bar%k1) = this%vFB(bar%k1) + 0.125*PI*bar%d**2*bar%l
                    this%mFB(bar%k1) = this%mFB(bar%k1) + ropes(paRope)%rho*0.125*PI*bar%d**2*bar%l

                    this%vFB(bar%k2) = this%vFB(bar%k2) + 0.125*PI*bar%d**2*bar%l
                    this%mFB(bar%k2) = this%mFB(bar%k2) + ropes(paRope)%rho*0.125*PI*bar%d**2*bar%l
                else
                    this%vFB(bar%k2) = this%vFB(bar%k2) + 0.25*PI*bar%d**2*bar%l
                    this%mFB(bar%k2) = this%mFB(bar%k2) + ropes(paRope)%rho*0.25*PI*bar%d**2*bar%l
                end if
                
            end associate
        end do
        
    CONTAINS
        
        PURE INTEGER FUNCTION findRope(nodeID) RESULT(iRope)
            implicit none
            
            integer, intent(in) :: nodeID
            
            integer :: iR
            
            do iR = 1,nRopes
                if (nodeID >= ropes(iR)%iNRef .and. nodeID <= ropes(iR)%iNLast) then
                    iRope = iR
                    return
                end if
            end do
            
            iRope = -1
            return
        END FUNCTION
        
    END SUBROUTINE
    
    
    !***************************************************************************
    ! Check multibody data consistency
    !***************************************************************************
    integer FUNCTION mbsData__checkConsistency( nMaCS, ropes, nRopes, &
        constrSec, nConstrSec, springs, nSprings, pids, nPIDs, rigids, nRigids )
        ! TODO: - update continuously, as new data is introduced...
        !       - check valid coefficient table ID
        implicit none
        
        integer,                    intent(in) :: nMaCS    ! Number of markers in CSYS0
        
        type(mbdReader__Rope),      intent(in) :: ropes(:) ! Rope elements
        integer,                    intent(in) :: nRopes   ! Total number of ropes
        
        type(mbdReader__ConstrSec), intent(in) :: constrSec(nConstrSec) ! Loop closing secondary position constraints simulated by PID-controller
        integer,                    intent(in) :: nConstrSec            ! Number of loop closing secondary constraints in system
        
        type(mbdReader__Spring),    intent(in) :: springs(nSprings) ! Spring elements
        integer,                    intent(in) :: nSprings          ! Number of springs in system
        
        type(mbdReader__PID),       intent(in) :: pids(nPIDs) ! PID elements
        integer,                    intent(in) :: nPIDs       ! Number of PID elements in system
        
        type(structElem__Rigid),    intent(in) :: rigids(nRigids) ! Rigid bodies
        integer,                    intent(in) :: nRigids         ! Total number of rigid bodies
        
        type(datCHK)                      :: errCh ! Data error checking class
        integer                           :: iR    ! Index of rope
        integer                           :: iSp   ! Index of spring
        integer                           :: iP    ! Index of pid element
        integer                           :: iCo   ! Index of constraint
        character(len=MAX_NUM2STR_LENGTH) :: IDStr ! String of arbitrary element ID for error string creation
        type(loeErr)                      :: err   ! Error handling class
        
        err   = loeErr__init('mbsData__checkConsistency',modName)
        errCh = datCHK__init('mbsData__checkConsistency',modName)
        
        
        ! Check, whether all values are consistent______________________________
        ! ______________________________________________________________________
        ! --- Markers in CSYS0 ---
        call datCHK__checkVal(errCh, 'CSYS0_nMarkers',nMaCS,'>=', 1) ! Number of markers in CSYS0
        
        ! --- Ropes ---
        call datCHK__checkVal(errCh, 'nRopes', nRopes, '>=', 1) ! Number of ropes
        do iR = 1,nRopes
            IDStr = num2Str(iR) ! IDStr of current element for error message creation
            call datCHK__checkVal(errCh, 'rope__length('//trim(IDStr)//')',ropes(iR)%length,'>',  ZE) ! Rope length
            call datCHK__checkVal(errCh, 'rope__diam('  //trim(IDStr)//')',ropes(iR)%diam,  '>',  ZE) ! Rope diameter
            call datCHK__checkVal(errCh, 'rope__nElems('//trim(IDStr)//')',ropes(iR)%nElems,'>=', 1 ) ! Number of elements
            
            if(ropes(iR)%targJoint%refType==MTYPE__RBOD) &
                call datCHK__genErr(errCh, 'Joint of rope #' // trim(IDStr) // ' references a marker on a rigid body.' // endl // &
                    ' The joint of a rope can only reference another rope or markers in CSYS__0.' // endl // &
                    ' If you want connect a rigid body to a rope, please use a force element instead.')
            
            ! Check for valid target marker of joint of rope
            if (.not. mbsData__checkMarker (ropes(iR)%targJoint, nMaCS, ropes, nRopes, rigids, nRigids)) &
                call datCHK__genErr(errCh, 'Invalid target marker referenced as joint by rope #'//trim(IDStr))
        end do
        
        
        ! --- Implicit constraints ---
        do iCo = 1,nConstrSec
            IDStr = num2Str(iCo) ! IDStr of current element for error message creation
            
            if (constrSec(iCo)%targ1%refType==MTYPE__CSYS) &
                call datCHK__genErr(errCh, 'First marker of constraint #' // trim(IDStr) // 'references marker in a CSYS. &
                                    &If you want to connect to a node in a CSYS, please use a joint instead.')
            if (constrSec(iCo)%targ2%refType==MTYPE__CSYS) &
                call datCHK__genErr(errCh, 'Second marker of constraint #' // trim(IDStr) // 'references marker in a CSYS. &
                                    &If you want to connect to a node in a CSYS, please use a joint instead.')
            
            if (constrSec(iCo)%targ1%refType==MTYPE__RBOD) &
                call datCHK__genErr(errCh, 'First marker of constraint #' // trim(IDStr) // 'references marker on rigid body. &
                                    &If you want to connect to a node in CSYS0, please use a force element instead.')
            if (constrSec(iCo)%targ2%refType==MTYPE__RBOD) &
                call datCHK__genErr(errCh, 'Second marker of constraint #' // trim(IDStr) // 'references marker on rigid body. &
                                    &If you want to connect to a node on a rigid body, please use a force element instead.')
            
            ! Check for valid target markers of constraint
            if (.not. mbsData__checkMarker (constrSec(iCo)%targ1, nMaCS, ropes, nRopes, rigids, nRigids)) &
                call datCHK__genErr(errCh, 'Invalid first marker referenced by constraint #' //IDStr)
            if (.not. mbsData__checkMarker (constrSec(iCo)%targ2, nMaCS, ropes, nRopes, rigids, nRigids)) &
                call datCHK__genErr(errCh, 'Invalid second marker referenced by constraint #'//IDStr)
        end do
        
        
        ! --- Properties of springs ---
        do iSp = 1,nSprings
            IDStr = num2Str(iSp)  ! IDStr of current element for error message creation
            call datCHK__checkVal(errCh, 'c('//trim(IDStr)//')',springs(iSp)%c,'>=',ZE) ! Stiffness
            call datCHK__checkVal(errCh, 'd('//trim(IDStr)//')',springs(iSp)%d,'>=',ZE) ! Damping
            
            ! Check for valid target markers of spring
            if (.not. mbsData__checkMarker (springs(iSp)%targ1, nMaCS, ropes, nRopes, rigids, nRigids)) &
                call datCHK__genErr(errCh, 'Invalid first marker referenced by spring #' //trim(IDStr))
            if (.not. mbsData__checkMarker (springs(iSp)%targ2, nMaCS, ropes, nRopes, rigids, nRigids)) &
                call datCHK__genErr(errCh, 'Invalid second marker referenced by spring #'//trim(IDStr))
        end do
        
        ! --- Properties of pids ---
        do iP = 1,nPIDs
            IDStr = num2Str(iP)  ! IDStr of current element for error message creation
            call datCHK__checkVal(errCh, 'kp('//trim(IDStr)//')',pids(iP)%kp,'>=',ZE) ! Stiffness
            call datCHK__checkVal(errCh, 'ki('//trim(IDStr)//')',pids(iP)%ki,'>=',ZE) ! Damping
            call datCHK__checkVal(errCh, 'kd('//trim(IDStr)//')',pids(iP)%kd,'>=',ZE) ! Damping
            
            ! Check for valid target markers of spring
            if (.not. mbsData__checkMarker (pids(iP)%targ1, nMaCS, ropes, nRopes, rigids, nRigids)) &
                call datCHK__genErr(errCh, 'Invalid first marker referenced by spring #' //trim(IDStr))
            if (.not. mbsData__checkMarker (pids(iP)%targ2, nMaCS, ropes, nRopes, rigids, nRigids)) &
                call datCHK__genErr(errCh, 'Invalid second marker referenced by spring #'//trim(IDStr))
        end do
        
        
        ! Finalisations_________________________________________________________
        ! ______________________________________________________________________
        if (errCh%nErr>0) &
            call loeErr__print(err, 'MBS data check finished with a total number of ' // &
                                    trim(num2Str(errCh%nErr)) // ' errors.')
        mbsData__checkConsistency = errCh%nErr
    END FUNCTION mbsData__checkConsistency
    
    
    !***************************************************************************
    ! Check for whether marker exists in system
    !***************************************************************************
    logical FUNCTION mbsData__checkMarker(marker, nMaCS, ropes, nRopes, rigids, nRigids)
        implicit none
        
        type(mbdReader_Marker),  intent(in) :: marker
        
        integer,                 intent(in) :: nMaCS
        
        type(mbdReader__Rope),   intent(in) :: ropes(:) ! Rope elements
        integer,                 intent(in) :: nRopes   ! Total number of ropes
        
        type(structElem__Rigid), intent(in) :: rigids(nRigids) ! Rigid bodies
        integer,                 intent(in) :: nRigids         ! Total number of rigid bodies
        
        integer :: iBody   ! Shorthand for body ID
        integer :: iMarker ! Shorthand for marker ID
        
        type(loeErr) :: err ! Error handler
        err = loeErr__init('mbsData__checkMarker',modName)
        
        
        mbsData__checkMarker = .false.
        
        iBody   = marker%refBody
        iMarker = marker%refMarker
        
        if (iMarker < 1) then
            call loeErr__print(err,' Marker id (' // num2Str(iMarker) // ') must not be < 1')
            return
        end if
        
        select case (marker%refType)
            
            case (MTYPE__CSYS)
                if (iBody/=0) then
                    call loeErr__print(err, 'Illegal Coordinate system ID: ' // trim(num2Str(iBody)) // &
                                            ' Currently the inertial frame CSYS__0 is the only supported CSYS.')
                    return
                end if
                
                if(iMarker > nMaCS) then
                    call loeErr__print( err, ' Nonexistent marker (' // trim(num2Str(iMarker)) &
                                             // ') in CSYS #' // trim(num2Str(iBody)) )
                    return
                end if
            
            
            case (MTYPE__FLEX)
                if (iBody > nRopes) then
                    call loeErr__print( err, 'Body id (' // trim(num2Str(iBody)) // &
                                             ') is > number of ropes in the system (' &
                                             // trim(num2Str(nRigids)) // ')' )
                    return
                end if
                
                if (iMarker > ropes(iBody)%nElems) then
                    call loeErr__print( err, ' Nonexistent marker (' // trim(num2Str(iMarker)) &
                                             //  ') on rope #' // num2Str(iBody))
                    return
                end if
            
            
            case (MTYPE__RBOD)
                if (iBody > nRigids) then
                    call loeErr__print( err, 'Body id (' // trim(num2Str(iBody)) // &
                                        ') is > number of rigid bodies in the system (' &
                                        // trim(num2Str(nRigids)) // ')' )
                    return
                end if
                
                if(iMarker > rigids(iBody)%nMarkers) then
                    call loeErr__print(err, ' Nonexistent marker (' // trim(num2Str(iMarker)) &
                                            //  ') on rigid body #' // num2Str(iBody))
                    return
                end if
            
        end select
        
        
        mbsData__checkMarker = .true.
    END FUNCTION
    
END MODULE
