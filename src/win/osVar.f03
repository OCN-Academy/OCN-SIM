MODULE osVar
    
    character(*),parameter :: os__pathSep    = '\'
    
    ! Os-specific commands. Include trailing blank character.
    character(*),parameter :: os__del    = 'del '
    character(*),parameter :: os__delDir = 'rmdir '
    character(*),parameter :: os__mkDir  = 'mkdir '
    
    
END MODULE osVar
