!*******************************************************************************
!   OCN-SIM Flex
!   Copyright (C) 2017 by Christoph Otto
!   christoph@ocnacademy.org
!   www.ocnacademy.org
!
!
!   MODULE :: Main program
!
!   PURPOSE
!   Command option parsing, initiation and execution of time integration,
!   conversion of results and cleanup.
!
!   
!     ___   ____ _   _    ____ ___ __  __    _____ _     _______  __
!    / _ \ / ___| \ | |  / ___|_ _|  \/  |  |  ___| |   | ____\ \/ /
!   | | | | |   |  \| |  \___ \| || |\/| |  | |_  | |   |  _|  \  / 
!   | |_| | |___| |\  |   ___) | || |  | |  |  _| | |___| |___ /  \
!    \___/ \____|_| \_|  |____/___|_|  |_|  |_|   |_____|_____/_/\_\
!
!
!   LICENSE
!   This file is part of OCN-SIM Flex.
!
!   OCN-SIM Flex is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   OCN-SIM Flex is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with OCN-SIM Flex. If not, see <http://www.gnu.org/licenses/>.
!
!*******************************************************************************

PROGRAM multiBodySimulation
    
    USE sys__Dyn
    USE liboe__Conf
    USE liboe__Types
    USE liboe__Tools
    
    
    implicit none
    
    integer                        :: nArg           ! Number of command line arguments
    character(MAXSTRL_FILE__FNAME) :: arg_fName      ! Name of loadcase main file
    character(MAXSTRL_FILE__FNAME) :: arg_opt        ! Optional argument
    logical                        :: convertResults ! Automatically convert results to vtk files using python
    
    
    ! --- Process command line arguments ---
    ! Check number of arguments passed to the program
    nArg = command_argument_count()
    if (nArg < 1) then

    end if
    
    select case (nArg)
        case(0)
            print *, '--- Error in main program ---' // endl // &
                     'Illegal number of arguments: ' // trim(num2Str(nArg)) // endl // endl // &
                     'Expected parameters: [-novtk] inputfile' // endl // endl // &
                     '-novtk flag is optional and supresses automatic conversion of results to &
                     &vtk files via external python scripts.' // endl // endl // &
                     '--- Terminating program ---'   // endl
            call exit
            
        case(1)
            call get_command_argument(1,arg_fName)
            convertResults = .true.
        
        case(2)
            call get_command_argument(1,arg_opt)
            call get_command_argument(2,arg_fName)
            
            if(trim(arg_opt)=='-novtk') then
                convertResults = .false.
            else
                print *, '--- Error in main program ---' // endl // &
                         'Illegal optional argument: '   // trim(arg_opt) // endl // endl // &
                         '--- Terminating program ---'   // endl
                call exit
            end if
            
        case(3:)
            print *, '--- Error in main program ---' // endl // &
                     'Illegal number of arguments: ' // trim(num2Str(nArg)) // endl // endl // &
                     'Expected parameters: [-novtk] inputfile' // endl // endl // &
                     '-novtk flag is optional and supresses automatic conversion of results to &
                     &vtk files via external python scripts.' // endl // endl // &
                     '--- Terminating program ---'   // endl
                call exit
    end select
    
    print *
    
    ! --- Main program ---
    if (.not. sysDyn__init(trim(arg_fName))) then
        print *, '--- Terminating program ---' // endl
        call exit
    end if
    
    ! Write general load case data to files
    call sysDyn__writeCaseData()
    
    ! Perform time integration
    if ( sysDyn__timeInt()) then
    
        ! Write result files
        if (convertResults) call sysDyn__convertResults()
    
    end if
    
    ! Free memory
    call sysDyn__cleanup()
    
END PROGRAM multiBodySimulation
