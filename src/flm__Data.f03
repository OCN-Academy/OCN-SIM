!*******************************************************************************
!   OCN-SIM Flex
!   Copyright (C) 2017 by Christoph Otto
!   christoph@ocnacademy.org
!   www.ocnacademy.org
!
!
!   MODULE :: Fluid model data
!
!   PURPOSE
!   Custom data type describing the properties of the fluid. Contains routines
!   to load data from file, check consistency and create force elements from
!   associated rope elements from rope elements contained in multibody data.
!
!   
!     ___   ____ _   _    ____ ___ __  __    _____ _     _______  __
!    / _ \ / ___| \ | |  / ___|_ _|  \/  |  |  ___| |   | ____\ \/ /
!   | | | | |   |  \| |  \___ \| || |\/| |  | |_  | |   |  _|  \  / 
!   | |_| | |___| |\  |   ___) | || |  | |  |  _| | |___| |___ /  \
!    \___/ \____|_| \_|  |____/___|_|  |_|  |_|   |_____|_____/_/\_\
!
!
!   LICENSE
!   This file is part of OCN-SIM Flex.
!
!   OCN-SIM Flex is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   OCN-SIM Flex is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with OCN-SIM Flex. If not, see <http://www.gnu.org/licenses/>.
!
!*******************************************************************************

MODULE flm__Data
    
    USE liboe__Conf
    USE liboe__Types
    USE liboe__Err
    USE liboe__Tools
    USE liboe__File
    USE flm__Types
    USE mbs__Data
    
    implicit none
    
    
    !***************************************************************************
    ! Module description                                                       !
    !***************************************************************************
    character(*), parameter, private :: modName = 'flm__Data'
    
    
    !***************************************************************************
    ! Public entities                                                          !
    !***************************************************************************
    PUBLIC :: flmData__loadFromFile, flmData__checkConsistency, flmData__cleanup
    PUBLIC :: flmData
    
    PUBLIC :: FLMOD_STATIC, FLMOD_CONST, FLMOD_AIRY, FLMOD_MAX_T
    
    
    !***************************************************************************
    ! Global constants                                                         !
    !***************************************************************************
    ! Fluid models to depict fluid velocity field
    integer, parameter :: FLMOD_STATIC    = 1 ! Non-moving fluid
    integer, parameter :: FLMOD_CONST     = 2 ! Constant, uniform velocity field
    integer, parameter :: FLMOD_AIRY      = 3 ! Linear Airy wave theory
    
    integer, parameter :: FLMOD_MAX_T     = 3 ! Maximum fluid model ID
    
    
PRIVATE


    !***************************************************************************
    ! Fluid data type                                                          !
    !***************************************************************************
    TYPE :: flmData
        
        ! Common parameters_____________________________________________________
        ! ______________________________________________________________________
        real(kind=prec) :: rho      ! Fluid density
        logical         :: simpDamp ! Apply simple, mass-proportional damping
        real(kind=prec) :: dampFac  ! Damping factor, if simple damping is applied
        
        
        ! Fluid-model-specific parameters_______________________________________
        ! ______________________________________________________________________
        integer                   :: fieldType        ! Type of model to depict fluid velocity field
        type(velField__Const)     :: field__Const     ! Data for constant, uniform velocity field
        type(velField__Airy)      :: field__Airy      ! Data for linear airy waves
        
        
        ! Force elements________________________________________________________
        ! ______________________________________________________________________
        integer                                :: nCTables        ! Number of coefficient tables
        type(fluid__CoeffTable1D), allocatable :: rope__coeffs(:) ! Drag, lift and shear coefficients vs relative angle of attack. Dimensions(nRopes)
        
    END TYPE flmData


CONTAINS
    
    
    !***************************************************************************
    ! Load fluid data from file
    !***************************************************************************
    logical FUNCTION flmData__loadFromFile(this, fName)
        implicit none
        
        type(flmData), intent(out)      :: this           ! Model to be read from file
        character(*), intent(in)        :: fName          ! Name of input file
        
        type(loeErr)                    :: err            ! Error handling class
        type(loeFile)                   :: inFile         ! Main input file
        logical                         :: allValuesRead  ! Status flag: .false. until all values are successfully read from file
        
        integer                         :: simpDamp       ! Integer provisionally holding the flag simplified damping. This is an integer, because we don't have function for reading boolean values from the file so far.
        
        integer                         :: iCT, iCTCurr   ! Indices of current coefficient table
        character(MAX_STRINGVAL_LENGTH) :: fNameCoeffData ! Name of hydrodynamic coefficient file for individual rope
        
        err = loeErr__init('flmData__loadFromFile',modName)
        flmData__loadFromFile = .false.
        
        
        if (.not. loeFile__open(inFile,trim(fName))) return
        allValuesRead = .false.
        readVals: do
            ! --- Common properties ---
            if (.not. loeFile__readVal(inFile, this%rho, 'rho')) exit readVals ! Fluid density
            
            
            ! --- Velocity field specific data ---
            if (.not. loeFile__readVal(inFile, this%fieldType,'fieldType')) exit readVals ! Type of velocity field
            
            select case (this%fieldType)
                ! Field type not supported
                case (:0)
                    call loeErr__print(err,'Illegal field type specified (' // trim(num2Str(this%fieldType)) // ')' // endl // &
                                           ' FieldType must be > 0')
                    exit readVals
                
                ! Non-moving field
                case (FLMOD_STATIC)
                    ! Nothing to be done here
                
                ! Constant, uniform field
                case (FLMOD_CONST)
                    ! Read the three velocity components in x-,y- and z-direction
                    if (.not. loeFile__readVal(inFile, this%field__Const%vConst(1),'u')) exit readVals
                    if (.not. loeFile__readVal(inFile, this%field__Const%vConst(2),'v')) exit readVals
                    if (.not. loeFile__readVal(inFile, this%field__Const%vConst(3),'w')) exit readVals
                
                case (FLMOD_AIRY)
                    if (.not. loeFile__readVal(inFile, this%field__Airy%omega,  'omega' )) exit readVals
                    if (.not. loeFile__readVal(inFile, this%field__Airy%lambda, 'lambda')) exit readVals
                    if (.not. loeFile__readVal(inFile, this%field__Airy%depth,  'depth' )) exit readVals
                    if (.not. loeFile__readVal(inFile, this%field__Airy%amp,    'amp'   )) exit readVals
                    
                    if (.not. loeFile__readVal(inFile, this%field__Airy%vConst(1),'u')) exit readVals
                    if (.not. loeFile__readVal(inFile, this%field__Airy%vConst(2),'v')) exit readVals
                    if (.not. loeFile__readVal(inFile, this%field__Airy%vConst(3),'w')) exit readVals
                    
                    this%field__Airy%k = 2*PI/this%field__Airy%lambda
                    
                ! Field type not supported
                case (FLMOD_MAX_T+1:)
                    call loeErr__print(err,'Illegal field type specified ('  // trim(num2Str(this%fieldType)) // ')' // &
                                            endl // ' FieldType must be <= ' // trim(num2Str(FLMOD_MAX_T)) )
                    exit readVals
            end select
            
            
            ! --- Hydrodynamic forces ---
            if (.not. loeFile__readVal(inFile, simpDamp, 'simpDamp')) exit readVals ! Simple damping flag
            this%simpDamp = simpDamp == 1 ! Convert to logical value. 1 equals true, everything else .false.
            
            if (this%simpDamp) then
                if (.not. loeFile__readVal(inFile, this%dampFac, 'dampFac')) exit readVals  ! Damping factor if simple damping is applied
            end if
            
            
            ! --- Hydrodynamic coefficient tables ---
            if (.not. this%simpDamp) then
                if (.not. loeFile__readNEl(inFile, this%nCTables, 1, 'nCTables')) exit readVals ! Number of coefficient tables
                
                ! Read individual tables
                ! TODO check for proper properties of coefficient table (i.e. ascending angles, 4 columns, etc.)
                allocate(this%rope__coeffs(this%nCTables))
                do iCT = 1,this%nCTables
                    if (.not. loeFile__readEID(inFile, iCTCurr,iCT,    'CTable'))                           exit readVals ! Current element ID
                    if (.not. loeFile__readVal(inFile, fNameCoeffData, 'fName'  ))                           exit readVals ! File name of coefficient table
                    if (.not. loeFile__loadMatrixFromFile(this%rope__coeffs(iCT)%vals,trim(fNameCoeffData))) exit readVals ! Actual coefficient table
                    this%rope__coeffs(iCT)%nVals = size(this%rope__coeffs(iCT)%vals,1)                                     ! Set number of values accordingly
                    
                    this%rope__coeffs(iCT)%vals(:,1) = this%rope__coeffs(iCT)%vals(:,1)*PI/180.0   ! Convert degree to radians
                end do
            end if
            
            
            allValuesRead = .true.
            exit readVals
        end do readVals
        
        
        if (.not. allValuesRead) then
            call loeErr__print(err,'Error in input file ' // fName // ' in line #' // trim(num2Str(inFile%lineNum)) // &
                                     ':' // endl // ' ' //quo// trim(inFile%lStr) //quo )
            return
        end if
        
        
        call loeFile__close(inFile)
        flmData__loadFromFile = .true.
    END FUNCTION flmData__loadFromFile
    
    
    !***************************************************************************
    ! Check data consistency
    !***************************************************************************
    integer FUNCTION flmData__checkConsistency(this)
        implicit none
        
        type(flmData), intent(in) :: this
        
        type(loeErr) :: err   ! Error handling class
        type(datCHK) :: errCh ! Data error checking class

        err   = loeErr__init('flmData__checkConsistency',modName)
        errCh = datCHK__init('flmData__checkConsistency',modName)
        
        
        call datCHK__checkVal(errCh, 'rho', this%rho, '>=', ZE)
        if (this%simpDamp) call datCHK__checkVal(errCh, 'dampFac', this%dampFac, '>=', ZE)
        
        call datCHK__checkVal(errCh, 'fieldType', this%fieldType, '>',  0)
        call datCHK__checkVal(errCh, 'fieldType', this%fieldType, '<=', FLMOD_MAX_T)
        
        if (errCh%nErr>0) &
            call loeErr__print(err, 'Fluid model data check finished with a total number of ' // &
                                    trim(num2Str(errCh%nErr)) // ' errors.')
        flmData__checkConsistency = errCh%nErr
    END FUNCTION flmData__checkConsistency
    
    
    !***************************************************************************
    ! Pseudo-destructor for flmData
    !***************************************************************************
    SUBROUTINE flmData__cleanup(this)
        implicit none
        
        type(flmData), intent(inout) :: this
        
        integer                      :: iCT ! Index of current coefficient table
        
        
        if(.not. this%simpDamp) then
            do iCT = 1,this%nCTables
                deallocate(this%rope__coeffs(iCT)%vals)
            end do
            
            deallocate(this%rope__coeffs)
        end if
        
    END SUBROUTINE
    
END MODULE flm__Data
