!*******************************************************************************
!   OCN-SIM Flex
!   Copyright (C) 2017 by Christoph Otto
!   christoph@ocnacademy.org
!   www.ocnacademy.org
!
!
!   MODULE :: Fluid model basic data types
!
!   PURPOSE
!   Basic custom data types.
!
!   
!     ___   ____ _   _    ____ ___ __  __    _____ _     _______  __
!    / _ \ / ___| \ | |  / ___|_ _|  \/  |  |  ___| |   | ____\ \/ /
!   | | | | |   |  \| |  \___ \| || |\/| |  | |_  | |   |  _|  \  / 
!   | |_| | |___| |\  |   ___) | || |  | |  |  _| | |___| |___ /  \
!    \___/ \____|_| \_|  |____/___|_|  |_|  |_|   |_____|_____/_/\_\
!
!
!   LICENSE
!   This file is part of OCN-SIM Flex.
!
!   OCN-SIM Flex is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   OCN-SIM Flex is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with OCN-SIM Flex. If not, see <http://www.gnu.org/licenses/>.
!
!*******************************************************************************

MODULE flm__Types
    
    USE liboe__Conf
    
    implicit none
    
    
    !***************************************************************************
    ! Custom data types
    !***************************************************************************
    ! --- Fluid model data of constant, uniform velocity field ---
    TYPE :: velField__Const
        real(prec), dimension(3) :: vConst ! Constant velocity of fluid
    END TYPE velField__Const
    
    
    ! --- Fluid model data for linear Airy waves---
    TYPE :: velField__Airy
        real(prec) :: omega     ! Angular frequency
        real(prec) :: lambda    ! Wave length
        real(prec) :: amp       ! Wave amplitude
        real(prec) :: depth     ! Water depth
        real(prec) :: k         ! Wave number. Calculated based on omega
        
        real(prec) :: vConst(3) ! Constant velocity of fluid superposed to waves
    END TYPE
    
    
    ! --- Coefficient table for drag, lift and shear on 1-dimensional bodies ---
    TYPE :: fluid__CoeffTable1D
        integer                                      :: nVals ! Number of values
        real(kind=prec), dimension(:,:), allocatable :: vals  ! Values: [Angle of attack, drag, lift, shear]. Dimension(nVals,4)
    END TYPE
    
    
    ! --- 1-dimensional force element: bar ---
    TYPE :: forceElem__Bar
        real(prec)      :: l         ! Length of bar element
        real(prec)      :: d         ! Diameter of bar element
        real(prec)      :: AProj     ! The bar's projected area (d*l)
        integer         :: k1,k2     ! Nodes one and two connected by bar element
        integer(int8)   :: k1RT,k2RT ! Reference type of node one and two.  Can be one of: MTYPE__CSYS, MTYPE__FLEX
        integer         :: IDCT      ! ID of hydrodynamic coefficient table
    END TYPE forceElem__Bar
    
    
END MODULE flm__Types
