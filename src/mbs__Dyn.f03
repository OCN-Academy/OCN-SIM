!*******************************************************************************
!   OCN-SIM Flex
!   Copyright (C) 2017 by Christoph Otto
!   christoph@ocnacademy.org
!   www.ocnacademy.org
!
!
!   MODULE :: Multibody dynamics
!
!   PURPOSE
!   Package of routines for multibody system dynamics
!
!   
!     ___   ____ _   _    ____ ___ __  __    _____ _     _______  __
!    / _ \ / ___| \ | |  / ___|_ _|  \/  |  |  ___| |   | ____\ \/ /
!   | | | | |   |  \| |  \___ \| || |\/| |  | |_  | |   |  _|  \  / 
!   | |_| | |___| |\  |   ___) | || |  | |  |  _| | |___| |___ /  \
!    \___/ \____|_| \_|  |____/___|_|  |_|  |_|   |_____|_____/_/\_\
!
!
!   LICENSE
!   This file is part of OCN-SIM Flex.
!
!   OCN-SIM Flex is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   OCN-SIM Flex is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with OCN-SIM Flex. If not, see <http://www.gnu.org/licenses/>.
!
!*******************************************************************************

MODULE mbs__Dyn
    
    USE liboe__Conf
    USE liboe__Err
    USE liboe__Tools
    USE liboe__Types
    USE mbs__Types
    USE mbs__Tools
    
    
    implicit none
    
    !***************************************************************************
    ! Module description
    !***************************************************************************
    character(*), parameter, private :: modName = 'mbs__Dyn'
    
    
    !***************************************************************************
    ! Public entities
    !***************************************************************************
    PUBLIC :: mbsDyn__initPIDS
    
    PUBLIC :: mbsDyn__markerPosFrom_ID_RT
    PUBLIC :: mbsDyn__markerVelFrom_ID_RT
    PUBLIC :: mbsDyn__markerStateFrom_ID_RT
    
    PUBLIC :: mbsDyn__applyForceBy_ID_RT
    
    PUBLIC :: mbsDyn__updatePIDs_M
    PUBLIC :: mbsDyn__calcConstraintForces
    
    PUBLIC :: mbsDyn__calcTransMatsFB, mbsDyn__calcPositionsFB
    PUBLIC :: mbsDyn__calcTransMatsFB_timeDeriv
    
    
    PUBLIC :: mbsDyn__slvAccsRB, mbsDyn__slvAccRB, mbsDyn__sumMarkerForcesRB
    PUBLIC :: mbsDyn__updateSolVarsRB
    

PRIVATE

CONTAINS
    
    !***************************************************************************
    ! Initialise PID force elements
    !***************************************************************************
    SUBROUTINE mbsDyn__initPIDS (pids, nPIDs)
        ! TODO: init correctly, if there is is an initial distance
        implicit none
        
        type(forceElem__PID), intent(inout) :: pids(nPIDs) ! List of PID elements
        integer,              intent(in)    :: nPIDs       ! Number of PID elements
        
        integer :: iP   ! Counting index
        
        ! I-part of PID force elements. TODO: init correctly
        do iP=1,nPIDs
            pids(iP)%rt__errI(:)   = ZE
        end do
    END SUBROUTINE
    
    
    !***************************************************************************
    ! Marker position depending on reference type
    !***************************************************************************
    PURE FUNCTION mbsDyn__markerPosFrom_ID_RT(nodeID, nodeRT, rMaCS, rMaFB, rMaRB) RESULT (r)
        implicit none
        
        real(prec)                :: r(3)       ! Resulting position vector
        
        integer,       intent(in) :: nodeID     ! ID of node in corresponding list (CSYS,ROPE,RBOD)
        integer(int8), intent(in) :: nodeRT     ! Reference type of marker (MTYPE__CSYS, MTYPE__FLEX, MTYPE__RBOD)
        real(prec),    intent(in) :: rMaCS(:,:) ! Marker positions in CSYS__0
        real(prec),    intent(in) :: rMaFB(:,:) ! Marker positions on ropes
        real(prec),    intent(in) :: rMaRB(:,:) ! Marker positions on rigid bodies
        
        
        select case (nodeRT)
            case (MTYPE__CSYS)
                r = rMaCS(:,nodeID)

            case (MTYPE__FLEX)
                r = rMaFB(:,nodeID)

            case (MTYPE__RBOD)
                r = rMaRB(:,nodeID)
        end select
    
    END FUNCTION
    
    
    !***************************************************************************
    ! Marker velocity depending on reference type
    !***************************************************************************
    PURE FUNCTION mbsDyn__markerVelFrom_ID_RT(nodeID, nodeRT, rMaFB_d, rMaRB_d) RESULT (r_d)
        implicit none
        
        real(prec)                :: r_d(3)       ! Resulting velocity vector
        
        integer,       intent(in) :: nodeID       ! ID of node in corresponding list (CSYS,ROPE,RBOD)
        integer(int8), intent(in) :: nodeRT       ! Reference type of marker (MTYPE__CSYS, MTYPE__FLEX, MTYPE__RBOD)
        real(prec),    intent(in) :: rMaFB_d(:,:) ! Marker velocities on ropes
        real(prec),    intent(in) :: rMaRB_d(:,:) ! Marker velocities on rigid bodies
        
        
        select case (nodeRT)
            case (MTYPE__CSYS)
                r_d = ZE

            case (MTYPE__FLEX)
                r_d = rMaFB_d(:,nodeID)

            case (MTYPE__RBOD)
                r_d = rMaRB_d(:,nodeID)
        end select
    
    END FUNCTION
    
    
    !***************************************************************************
    ! Marker state vector depending on reference type
    !***************************************************************************
    PURE SUBROUTINE mbsDyn__markerStateFrom_ID_RT( r, r_d, nodeID, nodeRT, rMaCS, &
                                                   rMaFB, rMaFB_d, rMaRB, rMaRB_d )
        ! Description
        !   Implemented for performance and convenience. Note, that we already
        !   have mbsDyn__markerPosFrom_ID_RT and mbsDyn__markerVelFrom_ID_RT.
        !   However, this way we can save one select block during execution.
        implicit none
        
        real(prec),    intent(out) :: r(3)         ! Resulting position vector
        real(prec),    intent(out) :: r_d(3)       ! Resulting velocity vector
        
        integer,       intent(in)  :: nodeID       ! ID of node in corresponding list (CSYS,ROPE,RBOD)
        integer(int8), intent(in)  :: nodeRT       ! Reference type of marker (MTYPE__CSYS, MTYPE__FLEX, MTYPE__RBOD)
        real(prec),    intent(in)  :: rMaCS(:,:)   ! Marker positions in CSYS__0
        real(prec),    intent(in)  :: rMaFB(:,:)   ! Marker positions on ropes
        real(prec),    intent(in)  :: rMaFB_d(:,:) ! Marker velocities on ropes
        real(prec),    intent(in)  :: rMaRB(:,:)   ! Marker positions on rigid bodies
        real(prec),    intent(in)  :: rMaRB_d(:,:) ! Marker velocities on rigid bodies
        
        
        select case (nodeRT)
            case (MTYPE__CSYS)
                r   = rMaCS(:,nodeID)
                r_d = ZE

            case (MTYPE__FLEX)
                r   = rMaFB  (:,nodeID)
                r_d = rMaFB_d(:,nodeID)

            case (MTYPE__RBOD)
                r   = rMaRB  (:,nodeID)
                r_d = rMaRB_d(:,nodeID)
        end select
    
    END SUBROUTINE
    
    
    !***************************************************************************
    ! Applies force to marker depending on reference type
    !***************************************************************************
    PURE SUBROUTINE mbsDyn__applyForceBy_ID_RT( fEFB, fERBMa, fExt, nodeID, nodeRT)
        implicit none
        
        real(prec),    intent(inout) :: fEFB(:,:)   ! Flexible body marker forces
        real(prec),    intent(inout) :: fERBMa(:,:) ! Rigid body marker forces
        
        real(prec),    intent(in)    :: fExt(3)     ! Vector of external force to be applied
        integer,       intent(in)    :: nodeID      ! ID of node in corresponding list (CSYS,ROPE,RBOD)
        integer(int8), intent(in)    :: nodeRT      ! Reference type of marker (MTYPE__CSYS, MTYPE__FLEX, MTYPE__RBOD)
        
        
        select case (nodeRT)
            case (MTYPE__CSYS)
                ! Nothing to be done here
                
            case (MTYPE__FLEX)
                fEFB  (:,nodeID) = fEFB  (:,nodeID) + fExt
                
            case (MTYPE__RBOD)
                fERBMa(:,nodeID) = fERBMa(:,nodeID) + fExt
                
        end select
        
    END SUBROUTINE
    
    
    !***************************************************************************
    ! Update magnitude-based PID force elements
    !***************************************************************************
    PURE SUBROUTINE mbsDyn__updatePIDs_M ( pids, nPIDs, fPID_FB, fPID_RB, rMaCS, nMaCS, &
                                           rMaFB, rMaFB_d, nNodes, &
                                           rMaRB, rMaRB_d, nMaRB, tStep )
        implicit none
        
        type(forceElem__PID_M), intent(inout) :: pids(nPIDS)
        integer,                intent(in)    :: nPIDs
        
        real(prec),             intent(inout) :: fPID_FB(3,nNodes)
        
        real(prec),             intent(inout) :: fPID_RB(3,nMaRB)
        
        real(prec),             intent(in)    :: rMaCS(3,nMaCS)
        integer,                intent(in)    :: nMaCS
        
        real(prec),             intent(in)    :: rMaFB(3,nNodes)
        real(prec),             intent(in)    :: rMaFB_d(3,nNodes)
        integer,                intent(in)    :: nNodes
        
        real(prec),             intent(in)    :: rMaRB(3,nMaRB)
        real(prec),             intent(in)    :: rMaRB_d(3,nMaRB)
        integer,                intent(in)    :: nMaRB
        
        real(prec),             intent(in)    :: tStep
        
        
        integer    :: iP
        real(prec) :: fP(3)
        
        real(prec) :: r1(3)
        real(prec) :: r2(3)
        
        real(prec) :: v1(3)
        real(prec) :: v2(3)
        
        do iP = 1,nPIDs
            call mbsDyn__markerStateFrom_ID_RT( r1, v1, pids(iP)%k1, pids(iP)%k1RT, &
                                                rMaCS, rMaFB, rMaFB_d, rMaRB, rMaRB_d )
            
            call mbsDyn__markerStateFrom_ID_RT( r2, v2, pids(iP)%k2, pids(iP)%k2RT, &
                                                rMaCS, rMaFB, rMaFB_d, rMaRB, rMaRB_d )
            
            call mbsTools__calcForcePID_M( pids(iP)%rt__errI, fP, r1, v1, r2, v2, pids(iP)%length, &
                                           pids(iP)%kp, pids(iP)%ki, pids(iP)%kd, tStep )
            
            call mbsDyn__applyForceBy_ID_RT( fPID_FB, fPID_RB,  fP, pids(iP)%k1, pids(iP)%k1RT )
            call mbsDyn__applyForceBy_ID_RT( fPID_FB, fPID_RB, -fP, pids(iP)%k2, pids(iP)%k2RT )
        end do
        
    
    END SUBROUTINE
    
    
    !***************************************************************************
    ! Calculate secondary constraint forces using PID-controllers
    !***************************************************************************
    PURE SUBROUTINE mbsDyn__calcConstraintForces( rtc, fCo, constrSec, nConstrSec, &
                                                  constrExpl, rMaFB, rMaFB_d, kp, ki, kd, tStep )
        implicit none
        
        type(rtData__ConstraintSec), intent(inout) :: rtc(:)   ! Constraint runtime data type
        real(prec),                  intent(out)   :: fCo(:,:) ! Resulting constraint forces. We assume all values not involved in constraints to be initialised to zero. Dimension(3,nNodes)
        
        type(sysElem__ConstrSec),    intent(in)    :: constrSec(nConstrSec) ! Custom data type describing the multibody data of the system 
        integer,                     intent(in)    :: nConstrSec            ! Number of secondary constraints
        
        type(sysElem__ConstrExpl),   intent(in)    :: constrExpl(:) ! Custom data type describing the multibody data of the system 
        
        real(prec),                  intent(in)    :: rMaFB(:,:) ! Vector of node positions. Dimension(3,nNodes)
        real(prec),                  intent(in)    :: rMaFB_d(:,:) ! Vector of node velocities. Dimension(3,nNodes)
        real(prec),                  intent(in)    :: kp      ! Linear coefficient
        real(prec),                  intent(in)    :: ki      ! Integral coefficient
        real(prec),                  intent(in)    :: kd      ! Derivative coefficient
        real(prec),                  intent(in)    :: tStep   ! Discrete timestep size
        
        integer    :: iCo        ! ID of current loop-closing constraint
        integer    :: iN1, iN2
        integer    :: iNP1, iNP2
        real(prec) :: fConstr(3) ! Constraint force
        real(prec) :: fCO_M1(3)   ! Magnitude of constraint force
        real(prec) :: fCO_M2(3)   ! Magnitude of constraint force
        
        
        if (calcConstraintsCross) then
            ! TODO: Check, whether nodes happen to coincide and we're in danger
            !       to get a div by zero :)
            do iCo = 1,nConstrSec
                iN1  = constrSec(iCo)%k1
                iNP1 = constrExpl(iN1)%pred
                
                iN2  = constrSec(iCo)%k2
                iNP2 = constrExpl(iN2)%pred
                
                call mbsTools__calcForcePID_M( rtc(iCo)%errI_M(1), fCO_M1, &
                    rMaFB(:,iNP1), rMaFB_d(:,iNP1), rMaFB(:,iN2), rMaFB_d(:,iN2), &
                    constrExpl(iN1)%length, kp, ki, kd, tStep )
                
                call mbsTools__calcForcePID_M( rtc(iCo)%errI_M(2), fCO_M2, &
                    rMaFB(:,iNP2), rMaFB_d(:,iNP2), rMaFB(:,iN1), rMaFB_d(:,iN1), &
                    constrExpl(iN2)%length, kp, ki, kd, tStep )
                
                fCo(:,iNP1) =   fCO_M1
                fCo(:,iN2)  =  -fCo(:,iNP1)
                
                fCo(:,iNP2) =   fCO_M2
                fCo(:,iN1)  =  -fCo(:,iNP2)
            end do
        else
            do iCo = 1,nConstrSec
                call mbsTools__calcForcePID( rtc(iCo)%errI, fConstr, &
                    rMaFB(:,constrSec(iCo)%k1), rMaFB_d(:,constrSec(iCo)%k1), &
                    rMaFB(:,constrSec(iCo)%k2), rMaFB_d(:,constrSec(iCo)%k2), &
                    kp, ki, kd, tStep)
                
                fCo(:,constrSec(iCo)%k1) = fConstr
                fCo(:,constrSec(iCo)%k2) = -fConstr
            end do
        end if
    END SUBROUTINE
    
    
    !***************************************************************************
    ! Transformation matrices and derivatives of system of flexible bodies
    !***************************************************************************
    PURE SUBROUTINE mbsDyn__calcTransMatsFB(T, T_AL, T_BE, T_ZE, q, constrExpl, nNodes)
        ! Calculates transformation matrices (relative and absolute) and 
        ! derivatives based on Kardan joint angles alpha and beta.
        implicit none
        
        real(prec),                intent(out) :: T(3,3,nNodes)      ! Transformation matrices with respect to predecessor
        real(prec),                intent(out) :: T_AL(3,3,nNodes)   ! Derivative w.r.t. alpha 
        real(prec),                intent(out) :: T_BE(3,3,nNodes)   ! Derivative w.r.t. beta
        real(prec),                intent(out) :: T_ZE(3,3,nNodes)   ! Transformation matrices with respect to reference coordinate system CSYS0
        real(prec),                intent(in)  :: q(2*nNodes)        ! Relative joint angles
        type(sysElem__ConstrExpl), intent(in)  :: constrExpl(nNodes) ! Constraints in explicit form
        integer,                   intent(in)  :: nNodes             ! Number of nodes
        
        integer :: iNode ! Current node
        
        do iNode = 1,nNodes
            T(:,:,iNode)   = mbsTools__calcTransMatKar(q(2*iNode-1:2*iNode))
            
            T_AL(1,:,iNode) =  ZE
            T_AL(2,:,iNode) = -T(3,:,iNode)
            T_AL(3,:,iNode) =  T(2,:,iNode)
            
            T_BE(:,1,iNode) = -T(:,3,iNode)
            T_BE(:,2,iNode) =  ZE
            T_BE(:,3,iNode) =  T(:,1,iNode)
            
            if (constrExpl(iNode)%predRT==MTYPE__FLEX) then
                T_ZE(:,:,iNode) = matmul(T_ZE(:,:,constrExpl(iNode)%pred), T(:,:,iNode))
            else
                T_ZE(:,:,iNode) = T(:,:,iNode)
            end if
        end do
    END SUBROUTINE
    
    
    !***************************************************************************
    ! Transformation matrices and derivatives of system of flexible bodies
    !***************************************************************************
    PURE SUBROUTINE mbsDyn__calcTransMatsFB_timeDeriv( T_AL_d, T_BE_d, T_ZE_d, &
                                                       T, T_AL, T_BE, T_ZE, q_d, &
                                                       constrExpl, nNodes )
        ! Calculates time-derivatives of transformation matrices
        implicit none
        
        real(prec),                intent(out) :: T_AL_d(3,3,nNodes) ! Time-derivative of derivative of transformation matrices w.r.t. alpha 
        real(prec),                intent(out) :: T_BE_d(3,3,nNodes) ! Time-derivative of derivative of transformation matrices w.r.t. beta
        real(prec),                intent(out) :: T_ZE_d(3,3,nNodes) ! Time-derivative of transformation matrices with respect to reference coordinate system CSYS0
        real(prec),                intent(in)  :: T(3,3,nNodes)      ! Transformation matrices with respect to predecessor
        real(prec),                intent(in)  :: T_AL(3,3,nNodes)   ! Derivative w.r.t. alpha 
        real(prec),                intent(in)  :: T_BE(3,3,nNodes)   ! Derivative w.r.t. beta
        real(prec),                intent(in)  :: T_ZE(3,3,nNodes)   ! Transformation matrices with respect to reference coordinate system CSYS0
        real(prec),                intent(in)  :: q_d(2*nNodes)      ! Relative joint velocities
        type(sysElem__ConstrExpl), intent(in)  :: constrExpl(nNodes) ! Constraints in explicit form
        integer,                   intent(in)  :: nNodes             ! Number of nodes
        
        real(prec) :: T_d(3,3)
        real(prec) :: T_AA(3,3)
        real(prec) :: T_BB(3,3)
        real(prec) :: T_AB(3,3)
        real(prec) :: T_BA(3,3)
        
        integer    :: iNode ! Current node
        
        
        do iNode = 1,nNodes
            ! Look closely, we're able to create the derivatives from the
            ! original matrix
            T_AA(1,  :) = ZE
            T_AA(2:3,:) = -T(2:3,:,iNode)
            
            T_BB(1,:) = -T(1,:,iNode)
            T_BB(2,:) = ZE
            T_BB(3,:) = -T(3,:,iNode)
            
            T_AB(:,1) = [ZE,  T(3,3,iNode), -T(2,3,iNode)]
            T_AB(:,2) = ZE
            T_AB(:,3) = [ZE,  -T(3,1,iNode),  T(2,1,iNode)]
            
            T_BA = T_AB
            
            T_AL_d(:,:,iNode) = T_AA*q_d(2*iNode-1) + T_AB*q_d(2*iNode)
            T_BE_d(:,:,iNode) = T_BA*q_d(2*iNode-1) + T_BB*q_d(2*iNode)
            
            T_d = T_AL(:,:,iNode)*q_d(2*iNode-1) + T_BE(:,:,iNode)*q_d(2*iNode)
            
            if (constrExpl(iNode)%predRT==MTYPE__FLEX) then
                T_ZE_d(:,:,iNode) = matmul(T_ZE_d(:,:,constrExpl(iNode)%pred),T(:,:,iNode)) + &
                                    matmul(T_ZE(:,:,constrExpl(iNode)%pred),  T_d)
            else
                T_ZE_d(:,:,iNode) = T_d
            end if
        end do
    END SUBROUTINE


    !***************************************************************************
    ! Calculate marker positions of system of flexible bodies
    !***************************************************************************
    PURE SUBROUTINE mbsDyn__calcPositionsFB(rMaFB, rMaCS, T_ZE, constrExpl, nMarkersCSYS0, nNodes)
        ! Calculation is recursive and based on transformation matrices. REMARK:
        ! We exclusively use the third col for transformation, since local vectors on
        ! rope elements are in z-direction by definition. Accordingly, the local
        ! displacement vector is rLoc = [0,0,elem_length(iNode)] and we can
        ! save the zero values in matrix multiplication.
        implicit none
        
        real(prec),                intent(out) :: rMaFB(3, nNodes)        ! Marker positions -- flexible bodies
        real(prec),                intent(in)  :: rMaCS(3, nMarkersCSYS0) ! Positions of markers in reference coordinate system CSYS0.
        real(prec),                intent(in)  :: T_ZE(3,3,nNodes)        ! Transformation matrices -- flexible bodies
        type(sysElem__ConstrExpl), intent(in)  :: constrExpl(nNodes)      ! Constraints in explicit form
        integer,                   intent(in)  :: nMarkersCSYS0           ! Number of markers in CSYS0
        integer,                   intent(in)  :: nNodes                  ! Number of nodes on flexible bodies
        
        integer :: iNode                   ! Current node
        
        
        do iNode= 1,nNodes
            if (constrExpl(iNode)%predRT==MTYPE__FLEX) then
                rMaFB(:,iNode) = rMaFB(:,constrExpl(iNode)%pred) + T_ZE(:,3,iNode)*constrExpl(iNode)%length
            else
                rMaFB(:,iNode) = rMaCS(:,constrExpl(iNode)%pred) + T_ZE(:,3,iNode)*constrExpl(iNode)%length
            end if
        end do
    END SUBROUTINE
    
    
    !***************************************************************************
    ! Update rigid body solver variables
    !***************************************************************************
    PURE SUBROUTINE mbsDyn__updateSolVarsRB( rMaRB, rMaRB_d, omRB, TRB, PRB, PRB_d, &
                                             qRB, qRB_d, rBodies, nRigids, nMarkersRB )
        implicit none
        
        real(prec), intent(out) :: rMaRB(3,nMarkersRB)   ! Rigid body marker positions
        real(prec), intent(out) :: rMaRB_d(3,nMarkersRB) ! Rigid body marker velocities
        real(prec), intent(out) :: TRB(3,3,nRigids)      ! Rigid body transformation matrices
        real(prec), intent(out) :: PRB(4,3,nRigids)      ! Projection matrix for angular velocities 
        real(prec), intent(out) :: PRB_d(4,3,nRigids)    ! Derivative of projection matrix for angular velocities 
        real(prec), intent(out) :: omRB(3,nRigids)       ! Angular velocity in CSYS0
        
        real(prec),              intent(in) :: qRB(7,nRigids)   ! Rigid body poses
        real(prec),              intent(in) :: qRB_d(7,nRigids) ! Rigid body poses, time derivative
        type(structElem__Rigid), intent(in) :: rBodies(nRigids) ! List of rigid body elements
        integer,                 intent(in) :: nRigids          ! Number of rigid bodies
        integer,                 intent(in) :: nMarkersRB       ! Total number of markers on rigid bodies
        
        integer :: iRB ! ID of current rigid body
        
        
        do iRB = 1,nRigids
            PRB(:,:,iRB)   = mbsTools__calcProjMatEul(qRB(4:7,iRB))
            PRB_d(:,:,iRB) = mbsTools__calcProjMatEul(qRB_d(4:7,iRB))
            omRB(:,iRB)    = 2.0*matmul(transpose(PRB(:,:,iRB)),qRB_d(4:7,iRB))
        end do

        TRB     = mbsDyn__calcTransMatsRB(qRB,nRigids)
        rMaRB   = mbsDyn__calcMarkerPositionsRB(qRB,TRB,rBodies,nRigids,nMarkersRB)
        rMaRB_d = mbsDyn__calcMarkerVelocitiesRB(qRB_d, omRB, TRB, rBodies, nRigids, nMarkersRB)
    END SUBROUTINE
    
    
    !***************************************************************************
    ! Calculate transformation matrices of system of rigid bodies
    !***************************************************************************
    PURE FUNCTION mbsDyn__calcTransMatsRB(qRB, nRigids)
        implicit none
        
        real(prec) :: mbsDyn__calcTransMatsRB(3,3,nRigids)
        
        real(prec), intent(in)  :: qRB(7,nRigids)
        integer,    intent(in)  :: nRigids
        
        integer                 :: iRB
        
        
        do iRB = 1,nRigids
            mbsDyn__calcTransMatsRB(:,:,iRB) = mbsTools__calcTransMatEul(qRB(4:7,iRB))
        end do
    END FUNCTION
    
    
    !***************************************************************************
    ! Calculate marker positions of system of rigid bodies
    !***************************************************************************
    PURE FUNCTION mbsDyn__calcMarkerPositionsRB(qRB, TRB, rBodies, nRigids, nMarkers)
        ! Description
        !   Transform local marker coordinates using body reference position plus
        !   transformed local marker coordinates.
        implicit none
        
        real(prec) :: mbsDyn__calcMarkerPositionsRB(3,nMarkers)         ! Rigid body marker positions
        
        real(prec),              intent(in) :: qRB(7,nRigids)   ! Rigid body poses
        real(prec),              intent(in) :: TRB(3,3,nRigids) ! Rigid body transformation matrices
        type(structElem__Rigid), intent(in) :: rBodies(nRigids) ! List of rigid body elements
        integer,                 intent(in) :: nRigids          ! Number of rigid bodies
        integer,                 intent(in) :: nMarkers         ! Total number of rigid body markers 
        
        integer :: iRB ! ID of current rigid body
        integer :: iMa ! ID of current marker
        
        
        do iRB = 1,nRigids
            do iMa = 1,rBodies(iRB)%nMarkers
                mbsDyn__calcMarkerPositionsRB(:, rBodies(iRB)%iNRef-1+iMa) = &
                    qRB(1:3,iRB) + matmul(TRB(:,:,iRB),rBodies(iRB)%markerPosns(:,iMa))
            end do
        end do
        
    END FUNCTION
    
    
    !***************************************************************************
    ! Calculate marker positions of system of rigid bodies
    !***************************************************************************
    PURE FUNCTION mbsDyn__calcMarkerVelocitiesRB( qRB_d, omRB, TRB, rBodies, nRigids, &
                                                  nMarkers ) RESULT (rMaRB_d)
        ! Description
        !   Transform local marker coordinates using body reference position plus
        !   transformed local marker coordinates.
        implicit none
        
        real(prec)                          :: rMaRB_d(3,nMarkers) ! Rigid body marker velocities
        
        real(prec),              intent(in) :: qRB_d(7,nRigids)    ! Rigid body poses, time derivative
        real(prec),              intent(in) :: omRB(3,nRigids)     ! Rigid body angular velocities
        real(prec),              intent(in) :: TRB(3,3,nRigids)    ! Rigid body transformation matrices
        type(structElem__Rigid), intent(in) :: rBodies(nRigids)    ! List of rigid body elements
        integer,                 intent(in) :: nRigids             ! Number of rigid bodies
        integer,                 intent(in) :: nMarkers            ! Total number of rigid body markers 
        
        integer :: iRB ! ID of current rigid body
        integer :: iMa ! ID of current marker
        
        ! TODO coordinate system, transform marker position!
        do iRB = 1,nRigids
            do iMa = 1,rBodies(iRB)%nMarkers
                rMaRB_d(:, rBodies(iRB)%iNRef-1+iMa) = qRB_d(1:3,iRB) + &
                    crossProduct(omRB(:,iRB), matmul(TRB(:,:,iRB), rBodies(iRB)%markerPosns(:,iMa)))
            end do
        end do
        
    END FUNCTION
    
    
    !***************************************************************************
    ! Solves for accelerations of system of rigid bodies
    !***************************************************************************
    PURE FUNCTION mbsDyn__slvAccsRB( qRB_d, omRB, TRB, PRB, PRB_d, fERB, &
                                     rigids, nRigids ) RESULT(qRB_dd)
        ! Description
        !   Calculates rigid body accelerations based on forces and torques on
        !   rigid body centers of gravity.
        implicit none
        
        real(prec) :: qRB_dd(7,nRigids)                           ! Rigid body accelerations
        
        real(prec),              intent(in) :: qRB_d(7,nRigids)   ! Rigid body velocities
        real(prec),              intent(in) :: omRB(3,nRigids)    ! Angular velocity in CSYS0
        real(prec),              intent(in) :: TRB(3,3,nRigids)   ! Rigid body transformation matrices
        real(prec),              intent(in) :: PRB(4,3,nRigids)   ! Projection matrix for angular velocities 
        real(prec),              intent(in) :: PRB_d(4,3,nRigids) ! Derivative of projection matrix for angular velocities 
        real(prec),              intent(in) :: fERB(6,nRigids)    ! External forces and torques acting on rigid body centers of gravity
        type(structElem__Rigid), intent(in) :: rigids(:)          ! Array of rigid body data
        integer,                 intent(in) :: nRigids            ! Number of rigid bodies
        
        integer :: iRB ! ID of current rigid body
        
        do iRB = 1,nRigids
            qRB_dd(:,iRB) = mbsDyn__slvAccRB( qRB_d(:,iRB), omRB(:,iRB), &
                                              TRB(:,:,iRB), PRB(:,:,iRB), PRB_d(:,:,iRB), &
                                              fERB(:,iRB), rigids(iRB)%mass, rigids(iRB)%ITens )
        end do
    END FUNCTION
    
    
    !***************************************************************************
    ! Calculate acceleration of one rigid body for given state and loads
    !***************************************************************************
    PURE FUNCTION mbsDyn__slvAccRB(qRB_d, omRB, TRB, P, P_d, fERB, m, ITens) RESULT(qRB_dd)
        ! Description
        !   Calculates rigid body accelerations based on forces and torques on
        !   rigid body centers of gravity. All values are defined w.r.t. CSYS0.
        !   Coordinates: 3 translational coordinates + 4 Euler-parameters
        implicit none
        
        real(prec)             :: qRB_dd(7)  ! Resulting rigid body accelerations
        
        real(prec), intent(in) :: qRB_d(7)   ! Rigid body velocities
        real(prec), intent(in) :: omRB(3)     ! Angular velocity in CSYS0
        real(prec), intent(in) :: TRB(3,3)   ! Rigid body transformation matrix w.r.t. CSYS0
        real(prec), intent(in) :: P(4,3)     ! Projection matrix for angular velocities 
        real(prec), intent(in) :: P_d(4,3)   ! Derivative of projection matrix for angular velocities 
        real(prec), intent(in) :: fERB(6)    ! External forces and torques acting on rigid body centers of gravity defined in CSYS0
        real(prec), intent(in) :: m          ! Mass
        real(prec), intent(in) :: ITens(3,3) ! Moment of inertia in local coordinate system
        
        real(prec) :: al0(3)         ! Angular acceleration in CSYS0
        real(prec) :: ITens0(3,3)    ! Inertia tensor transformed to CSYS0
        real(prec) :: ITens0Inv(3,3) ! Inverse of inertia tensor in CSYS0
        
        ITens0    = matmul(matmul(TRB,ITens),transpose(TRB))
        ITens0Inv = invertMatr3(ITens0)
        al0       = matmul(ITens0Inv,(matmul(matmul(vec2ScewMatr(omRB),ITens0),omRB) + fERB(4:6)))
        
        qRB_dd = [(1.0_prec/m)*fERB(1:3), 0.5_prec*(matmul(P_d,omRB) + matmul(P,al0)) - qRB_d(4:7)*1.0_prec]
        
    END FUNCTION mbsDyn__slvAccRB
    
    
    !***************************************************************************
    ! Sum up marker forces to resulting torque and force wrt center of gravity
    !***************************************************************************
    PURE FUNCTION mbsDyn__sumMarkerForcesRB(fEMa,rMa,T) RESULT(fCOG)
        implicit none
        
        real(prec)              :: fCOG(6)   ! Resulting forces and torques wrt center of gravity in global coordinate system
        
        real(prec), intent(in)  :: fEMa(:,:) ! Marker forces in global coordinate system. Dimension (3,nMarkers)
        real(prec), intent(in)  :: rMa(:,:)  ! Marker coordinates wrt center of gravity in local coordinate system. Dimension (3,nMarkers)
        real(prec), intent(in)  :: T(3,3)    ! Transformation matrix from local to global coordinate system
        
        integer :: nMarkers ! Number of markers
        integer :: iMa      ! Index of current marker
        
        
        nMarkers = size(rMa,2)
        fCOG(:) = ZE
        
        do iMa = 1,nMarkers
            fCOG(1:3) = fCOG(1:3) + fEMa(:,iMa)
            fCOG(4:6) = fCOG(4:6) + crossProduct( matmul(T,rMa(:,iMa)), fEMa(:,iMa) )
        end do
        
    END FUNCTION mbsDyn__sumMarkerForcesRB
    
END MODULE 
