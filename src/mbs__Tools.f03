!*******************************************************************************
!   OCN-SIM Flex
!   Copyright (C) 2017 by Christoph Otto
!   christoph@ocnacademy.org
!   www.ocnacademy.org
!
!
!   MODULE :: Multibody dynamics tools
!
!   PURPOSE
!   Package of basic routines for multibody system dynamics
!
!   
!     ___   ____ _   _    ____ ___ __  __    _____ _     _______  __
!    / _ \ / ___| \ | |  / ___|_ _|  \/  |  |  ___| |   | ____\ \/ /
!   | | | | |   |  \| |  \___ \| || |\/| |  | |_  | |   |  _|  \  / 
!   | |_| | |___| |\  |   ___) | || |  | |  |  _| | |___| |___ /  \
!    \___/ \____|_| \_|  |____/___|_|  |_|  |_|   |_____|_____/_/\_\
!
!
!   LICENSE
!   This file is part of OCN-SIM Flex.
!
!   OCN-SIM Flex is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   OCN-SIM Flex is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with OCN-SIM Flex. If not, see <http://www.gnu.org/licenses/>.
!
!*******************************************************************************

MODULE mbs__Tools
    
    USE liboe__Conf
    USE liboe__Types
    
    implicit none
    
    PUBLIC :: mbsTools__calcForcePID
    PUBLIC :: mbsTools__calcForcePID_M
    PUBLIC :: mbsTools__calcTransMatKar, mbsTools__calcTransMatKarSinCos
    PUBLIC :: mbsTools__calcTransMatEul, mbsTools__calcProjMatEul
    PUBLIC :: mbsTools__xyzEulAng2EulPar
    
PRIVATE

CONTAINS

    !***************************************************************************
    ! Calculate force induced by discrete PID controller -- component-wise
    !***************************************************************************
    PURE SUBROUTINE mbsTools__calcForcePID( errI, fPID, r1, v1, r2, v2, &
                                            kp, ki, kd, tStep)
        implicit none
        
        real(prec), intent(inout) :: errI(3)   ! Integral error
        real(prec), intent(out)   :: fPID(3)   ! Resulting force
        real(prec), intent(in)    :: r1(3)     ! Position of first node
        real(prec), intent(in)    :: v1(3)     ! Velocity of first node
        real(prec), intent(in)    :: r2(3)     ! Position of second node
        real(prec), intent(in)    :: v2(3)     ! Velocity of second node
        real(prec), intent(in)    :: kp        ! Proportional constant
        real(prec), intent(in)    :: ki        ! Integral constant
        real(prec), intent(in)    :: kd        ! Differential constant
        real(prec), intent(in)    :: tStep     ! Discrete timestep size
        
        real(prec)                :: rDiff(3)  ! Current difference in positions
        real(prec)                :: errD(3)   ! Differential error
        
        
        rDiff = r2 -r1
        errI  = errI + rDiff*tStep
        errD  = v2 - v1
        
        fPID   = kp*rDiff + ki*errI + kd*errD
    END SUBROUTINE
    
    
    !***************************************************************************
    ! Calculate force induced by discrete PID controller -- magnitude based
    !***************************************************************************
    PURE SUBROUTINE mbsTools__calcForcePID_M( errI, fPID, r1, v1, r2, v2,&
                                              l0, kp, ki, kd, tStep )
        implicit none
        
        real(prec), intent(inout) :: errI   ! Integral error
        real(prec), intent(out)   :: fPID(3) ! Resulting force
        real(prec), intent(in)    :: r1(3)  ! Position of first node
        real(prec), intent(in)    :: v1(3)  ! Velocity of first node
        real(prec), intent(in)    :: r2(3)  ! Position of second node
        real(prec), intent(in)    :: v2(3)  ! Velocity of second node
        real(prec), intent(in)    :: l0     ! Distance to be kept
        real(prec), intent(in)    :: kp     ! Proportional constant
        real(prec), intent(in)    :: ki     ! Integral constant
        real(prec), intent(in)    :: kd     ! Differential constant
        real(prec), intent(in)    :: tStep  ! Discrete timestep size
        
        real(prec) :: rDiff  ! Current difference in positions
        real(prec) :: errD   ! Differential error
        
        real(prec) :: d_r1r2
        real(prec) :: uN(3) ! PID normal vector
        
        d_r1r2 = sqrt(sum((r2-r1)**2))
        
        rDiff = d_r1r2 - l0
        errI  = errI + rDiff*tStep
        
        uN   = (r2-r1)/d_r1r2
        errD = dot_product(uN,(v2-v1))
        
        fPID   = uN*(kp*rDiff + ki*errI + kd*errD)
    END SUBROUTINE
    
    
    !***************************************************************************
    ! Convert xyz-Euler-angles to Euler-parameters
    !***************************************************************************
    PURE FUNCTION mbsTools__xyzEulAng2EulPar(eulAngs)
        implicit none
        
        real(prec)             :: mbsTools__xyzEulAng2EulPar(4)
        real(prec), intent(in) :: eulAngs(3)
        
        real(prec) :: sa, ca, sb, cb, sg, cg ! Sines and cosines of 0.5*alpha, 0.5*beta and 0.5*gamma
        
        sa = sin(0.5*eulAngs(1)); ca = cos(0.5*eulAngs(1))
        sb = sin(0.5*eulAngs(2)); cb = cos(0.5*eulAngs(2))
        sg = sin(0.5*eulAngs(3)); cg = cos(0.5*eulAngs(3))
        
        mbsTools__xyzEulAng2EulPar = [ -sa*sb*sg + ca*cb*cg, & 
                              sa*cb*cg + ca*sb*sg, &
                             -sa*cb*sg + ca*sb*cg, &
                              sa*sb*cg + ca*cb*sg ]
    END FUNCTION
    
    
    !***************************************************************************
    ! Transformation matrices based on Kardan angles alpha an beta 
    !***************************************************************************
    PURE FUNCTION mbsTools__calcTransMatKar(q)
        ! Calculates transformation matrices based on Kardan joint angles alpha and beta..
        implicit none
        
        real(prec)             :: mbsTools__calcTransMatKar(3,3) ! Transformation matrices with respect to predecessor
        real(prec), intent(in) :: q(2)                         ! Relative joint angles
        
        real(prec)             :: sa,ca,sb,cb                  ! sines and cosines of alpha and beta
        
        sa= sin(q(1)); ca= cos(q(1))
        sb= sin(q(2)); cb= cos(q(2))

        mbsTools__calcTransMatKar(:,1) = [ cb,  sa*sb, -ca*sb ]
        mbsTools__calcTransMatKar(:,2) = [ ZE,  ca,     sa    ]
        mbsTools__calcTransMatKar(:,3) = [ sb, -sa*cb,  ca*cb ]

    END FUNCTION
    
    !***************************************************************************
    ! Transformation matrices based on sines and cosines of Kardan angles alpha an beta 
    !***************************************************************************
    PURE FUNCTION mbsTools__calcTransMatKarSinCos(sa, ca, sb, cb)
        ! Calculates transformation matrices based on Kardan joint angles alpha and beta..
        implicit none
        
        real(prec)             :: mbsTools__calcTransMatKarSinCos(3,3) ! Transformation matrices with respect to predecessor
        real(prec), intent(in) :: sa,ca,sb,cb                          ! sines and cosines of alpha and beta

        mbsTools__calcTransMatKarSinCos(:,1) = [ cb,  sa*sb, -ca*sb ]
        mbsTools__calcTransMatKarSinCos(:,2) = [ ZE,  ca,     sa    ]
        mbsTools__calcTransMatKarSinCos(:,3) = [ sb, -sa*cb,  ca*cb ]

    END FUNCTION
    
    
    !***************************************************************************
    ! Transformation matrix based on EULER-parameters
    !***************************************************************************
    PURE FUNCTION mbsTools__calcTransMatEul(p)
        implicit none
        
        real(prec)             :: mbsTools__calcTransMatEul(3,3)
        real(prec), intent(in) :: p(4)
        
        
        associate( ps=>p(1), px=>p(2), py=>p(3), pz=>p(4) )
            
            mbsTools__calcTransMatEul(:,1) = 2*[ ps**2+px**2-0.5, px*py+ps*pz,     px*pz-ps*py     ]
            mbsTools__calcTransMatEul(:,2) = 2*[ px*py-ps*pz,     ps**2+py**2-0.5, py*pz+ps*px     ]
            mbsTools__calcTransMatEul(:,3) = 2*[ px*pz+ps*py,     py*pz-ps*px,     ps**2+pz**2-0.5 ]
        
        end associate
        
    END FUNCTION
    
    
    !***************************************************************************
    ! Kinematic projection matrix based on EULER-parameters
    !***************************************************************************
    PURE FUNCTION mbsTools__calcProjMatEul(p)
        implicit none
        
        real(prec)             :: mbsTools__calcProjMatEul(4,3)
        real(prec), intent(in) :: p(4)
        
        
        associate( ps=>p(1), px=>p(2), py=>p(3), pz=>p(4) )
            
            mbsTools__calcProjMatEul(:,1) = [ -px,  ps, -pz,  py ]
            mbsTools__calcProjMatEul(:,2) = [ -py,  pz,  ps, -px ]
            mbsTools__calcProjMatEul(:,3) = [ -pz, -py,  px,  ps ]
        
        end associate
        
    END FUNCTION
END MODULE
