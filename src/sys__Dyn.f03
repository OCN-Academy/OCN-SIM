!*******************************************************************************
!   OCN-SIM Flex
!   Copyright (C) 2017 by Christoph Otto
!   christoph@ocnacademy.org
!   www.ocnacademy.org
!
!
!   MODULE :: System dynamics
!
!   PURPOSE
!   This module contains the main routines for time integration of the
!   combined system of flexible and rigid bodies under the influence of
!   hydrodynamic loads. For this purpose, it also contains the glue code
!   calculating external loads, hydrodynamic loads as well as the
!   interaction of flexible and rigid bodies.
!
!
!     ___   ____ _   _    ____ ___ __  __    _____ _     _______  __
!    / _ \ / ___| \ | |  / ___|_ _|  \/  |  |  ___| |   | ____\ \/ /
!   | | | | |   |  \| |  \___ \| || |\/| |  | |_  | |   |  _|  \  /
!   | |_| | |___| |\  |   ___) | || |  | |  |  _| | |___| |___ /  \
!    \___/ \____|_| \_|  |____/___|_|  |_|  |_|   |_____|_____/_/\_\
!
!
!   LICENSE
!   This file is part of OCN-SIM Flex.
!
!   OCN-SIM Flex is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   OCN-SIM Flex is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with OCN-SIM Flex. If not, see <http://www.gnu.org/licenses/>.
!
!*******************************************************************************

MODULE sys__Dyn

    USE osVar
    USE liboe__Conf
    USE liboe__Types
    USE liboe__Tools
    USE liboe__Err
    USE liboe__File
    USE mbs__Types
    USE mbs__Tools
    USE mbs__Dyn
    USE mbs__Data
    USE sol__JAC
    USE sol__RRF
    USE sol__LAGR
    USE flm__Types
    USE flm__Data
    USE flm__Dyn
    USE sys__Data
    USE sys__Tools

    implicit none


    !***************************************************************************
    ! Module description                                                       !
    !***************************************************************************
    character(*), parameter, private :: modName = 'sys__Dyn'


    !***************************************************************************
    ! Public functions
    !***************************************************************************
    PUBLIC :: sysDyn__init, sysDyn__timeInt, sysDyn__cleanup
    PUBLIC :: sysDyn__convertResults, sysDyn__writeCaseData
    
    
    !***************************************************************************
    ! Hiding away stuff from my private experimental repository
    !***************************************************************************
#define USE_EXPERIMENTAL_STUFF 0

#if  USE_EXPERIMENTAL_STUFF == 1
#define timeInt__experimental "snippet__timeInt__experimental.f03"
#define init__experimental    "snippet__init__experimental.f03"
#define cleanup__experimental "snippet__cleanup__experimental.f03"
#define calcAcc__experimental "snippet__calcAcc__experimental.f03"
#define vars__experimental    "snippet__vars__experimental.f03"
#else
#define timeInt__experimental "snippet__empty.f03"
#define init__experimental    "snippet__empty.f03"
#define cleanup__experimental "snippet__empty.f03"
#define calcAcc__experimental "snippet__empty.f03"
#define vars__experimental    "snippet__empty.f03"
#endif

PRIVATE

    !***************************************************************************
    ! Variables
    !***************************************************************************

    ! Common data_______________________________________________________________
    ! __________________________________________________________________________
    ! --- Global configuration data ---
    character(MAX_STRINGVAL_LENGTH) :: rootDir         ! String containing command to run python


    ! --- General system data ---
    type(sysData) :: sys ! Custom data type describing the system in terms of general data specified in the main input file
    type(mbsData) :: mbd ! Custom data type describing the multibody data of the system
    type(flmData) :: fld ! Custom data type describing the fluid properties and the corresponding velocity field


    ! --- External Loads ---
    real(prec), allocatable :: fEFB(:,:)   ! Flexible body external forces. Dimension(3,mbd%nNodes)
    real(prec), allocatable :: fGFB(:,:)   ! Flexible body gravitational loads. Dimension(3,mbd%nNodes)
    real(prec), allocatable :: fBFB(:,:)   ! Flexible body buoyancy. Dimension(3,mbd%nNodes)
    
    real(prec), allocatable :: fERB(:,:)   ! Rigid body hydrodynamic forces and torques vector. Dimension(6,mbd%nRigids)
    real(prec), allocatable :: fERBMa(:,:) ! Rigid body marker external forces. Dimension(6,mbd%nMarkersRB)
    real(prec), allocatable :: fGRB(:,:)   ! Rigid body gravitational loads forces and torques vector. Dimension(3,mbd%nRigids)
    real(prec), allocatable :: fBRB(:,:)   ! Rigid body buoyancy forces and torques vector. Dimension(3,mbd%nRigids)


    ! --- Hydrodynamic loads ---
    real(prec), allocatable :: uFB(:,:)    ! Flexible body flow velocity at positions of nodes. Dimension(3,mbd%nNodes)
    real(prec), allocatable :: uRelFB(:,:) ! Flexible body relative flow velocity with respect to nodes. Dimension(3,mbd%nNodes)
    real(prec), allocatable :: fHyd(:,:)   ! Flexible body hydrodynamic forces vector. Dimension(3,mbd%nNodes)

    real(prec), allocatable :: uRB(:,:)    ! Flow velocity at positions of rigid bodies. Dimension(3,mbd%nRigids)
    real(prec), allocatable :: uRelRB(:,:) ! Rigid body relative flow velocities. Dimension(3,mbd%nRigids)
    real(prec), allocatable :: fHydRB(:,:) ! Rigid body hydrodynamic forces and torques vector. Dimension(6,mbd%nRigids)


    ! --- Cartesian coordinates of all markers and time derivatives ---
    real(prec), allocatable :: rMaFB(:,:)    ! Marker positions: Flexible body nodes. Dimension(3,nNodes)
    real(prec), allocatable :: rMaFB_d(:,:)  ! Marker velocities: Flexible body nodes. Dimension(3,nNodes)

    real(prec), allocatable :: rMaRB(:,:)    ! Marker positions: Rigid body markers. Dimension(3,nMarkersRB)
    real(prec), allocatable :: rMaRB_d(:,:)  ! Marker velocities: Rigid body markers. Dimension(3,nMarkersRB)


    ! --- Rigid body kinematic quantities ---
    real(prec), allocatable :: qRB(:,:)     ! Rigid body poses. Each pose is described by 3 translational coordinates plus 4 Euler parameters describing the body's rotation w.r.t. CSYS0. Dimension(7, mbd%nRigids).
    real(prec), allocatable :: qRB_d(:,:)   ! Rigid body velocities. Dimension(7, mbd%nRigids).
    real(prec), allocatable :: qRB_dd(:,:)  ! Rigid body accelerations. Dimension(7, mbd%nRigids).
    real(prec), allocatable :: omRB(:,:)    ! Rigid body angular velocities with respect to CSYS_0. Dimension(3, mbd%nRigids).
    real(prec), allocatable :: TRB(:,:,:)   ! Rigid body transformation matrices. Dimension (3,3,mbd%nRigids).
    real(prec), allocatable :: PRB(:,:,:)   ! Rigid body velocity projection matrices. Dimension (4,3,mbd%nRigids).
    real(prec), allocatable :: PRB_d(:,:,:) ! Rigid body velocity projection matrices, time derivative. Dimension (4,3,mbd%nRigids).


    ! Data specific to Lagrangian dynamics______________________________________
    ! __________________________________________________________________________
    type(solVars__JAC)  :: svj ! Solver specific variables for non-recursive formulations
    
    
    ! RRF-specific data_________________________________________________________
    ! __________________________________________________________________________
    real(prec), allocatable :: mFBInv(:) ! Vector of reciprocal element masses. Dimension(nNodes)
    real(prec), allocatable :: rMaFB_dd(:,:)    ! Marker accelerations: Flexible body nodes. Dimension(3,nNodes)
    real(prec), allocatable :: rMaFB0(:,:)      ! Marker positions, backup: Flexible body nodes. Dimension (3,mbd%nNodes)
    real(prec), allocatable :: rMaFB_d0(:,:)    ! Marker velocities, backup: Flexible body nodes. Dimension (3,mbd%nNodes)
    real(prec), allocatable :: fRFB(:,:)        ! Vector of flexible body reaction forces. Dimension (3,mbd%nNodes).
    real(prec), allocatable :: fEFB0(:,:)       ! Dimension (3,mbd%nNodes)
    
    
    ! Internal forces___________________________________________________________
    ! __________________________________________________________________________
    real(prec), allocatable :: fBRFB(:,:) ! Vector of flexible body internal reaction forces. Dimension (3,mbd%nConstr).
    type(solVars__LAGR)     :: svl        ! Solver specific variables for Lagrangian dynamics
    
    
    ! Water surface_____________________________________________________________
    ! __________________________________________________________________________
    real(prec), allocatable :: rSurf(:,:)   ! Coordinates at which to calculate water surface elevation
    real(prec), allocatable :: zSurf(:)     ! Water surface elevation

#include vars__experimental
CONTAINS


    !***************************************************************************
    ! Initialisations
    !***************************************************************************
    logical FUNCTION sysDyn__init(fName)
        implicit none

        character(*), intent(in) :: fName
        
        type(loeFile)                          :: confFile      ! Global configuration file
        logical                                :: outDirExists  ! Check, whether output directory exists
        
        ! --- Data needed for solvers based on the explicit constraint equations ---
        real(prec),                allocatable :: q0(:)         ! Initial joint positions
        real(prec),                allocatable :: v0(:)         ! Initial joint velocities
        
        type(sysElem__ConstrExpl), allocatable :: constrExpl(:) ! Explicit constraints
        integer                                :: nConstrExpl   ! Number of explicit constraints
        
        ! --- Common data ---
        integer,                   allocatable :: map__full_to_Merged(:) ! Maps original node IDs to merged node IDs. Only needed, when rrf solvers are used and the merge nodes feature is enabled
        
        integer                                :: iEl       ! Counting index for elements
        
        integer                                :: iPos      ! Counting index for water surface output positions
        real(prec)                             :: wSurfLDiv ! Distance between two water surface output positions 
        
        type(loeErr)                           :: err       ! Error handler

        err          = loeErr__init('sysDyn__init',modName)
        sysDyn__init = .false.
        
        
        ! --- Load global program configuration ---
        if (.not. loeFile__open(confFile,'OCN-SIM-FLEX.conf')) then
            call loeErr__print(err, 'Could not open program configuration file OCN-SIM-FLEX.conf. &
                                    &File must be located in current working directory.')
            return
        end if

        if (.not. loeFile__readVal(confFile,rootDir,'rootDir')) then
            call loeErr__print(err, 'Could not read root directory variable ''rootDir'' from program &
                                    &configuration file OCN-SIM-FLEX.conf in current working directory.')
            return
        end if

        call loeFile__close(confFile)
        
        
        ! --- Load the loadcase (pun intended!) ---
        if ( .not. sysData__loadFromFile( sys, mbd, rMaFB, rMaFB_d, &
             qRB, q0, v0, constrExpl, nConstrExpl, fld, fName) ) then
            call loeErr__print(err, 'Could not load file ' // fName)
            return
        end if
        
        
        ! --- Merge constraints ---
        if (sys%solType >= SOL_RRF_1 .and. RRF__mergeSecConstraints .and. mbd%nConstrSec > 0) then
            call mbsData__mergeSecConstraints(mbd, rMaFB, rMaFB_d, map__full_to_Merged)
            
            do iEl = 1, size(mbd%fElems__bar)
                mbd%fElems__bar(iEl)%k1 = map__full_to_Merged(mbd%fElems__bar(iEl)%k1)
                mbd%fElems__bar(iEl)%k2 = map__full_to_Merged(mbd%fElems__bar(iEl)%k2)
            end do
            
            do iEl = 1, mbd%nSprings
                mbd%springs(iEl)%k1 = map__full_to_Merged(mbd%springs(iEl)%k1)
                mbd%springs(iEl)%k2 = map__full_to_Merged(mbd%springs(iEl)%k2)
            end do
            
            do iEl = 1, mbd%nPIDs
                mbd%pids(iEl)%k1 = map__full_to_Merged(mbd%pids(iEl)%k1)
                mbd%pids(iEl)%k2 = map__full_to_Merged(mbd%pids(iEl)%k2)
            end do
            
            deallocate(map__full_to_Merged)
        end if
        
        
        ! --- Allocations and initialisations ---
        call sysTools__allocIntVars( rMaRB, rMaRB_d, qRB_d, qRB_dd, &
                                     omRB, TRB, PRB, PRB_d, &
                                     uRelFB, uFB, uRelRB, uRB, &
                                     fEFB, fGFB, fBFB, fERB, fERBMa, &
                                     fGRB, fBRB, fHyd, fHydRB, &
                                     mbd%nNodes, mbd%nRigids, mbd%nMarkersRB )
        
        call sysTools__initGravityAndBuoyancyFB( fGFB, fBFB, mbd%mFB, mbd%vFB, & 
                                               mbd%nNodes, fld%rho, sys%gVec )
        
        call sysTools__initGravityAndBuoyancyRB( fGRB, fBRB, mbd%rigids, &
                                               mbd%nRigids, fld%rho, sys%gVec )
        
        call mbsDyn__initPIDS(mbd%pids, mbd%nPIDs)
        
        call mbsDyn__updateSolVarsRB( rMaRB, rMaRB_d, omRB, TRB, PRB, PRB_d, &
                                      qRB, qRB_d, mbd%rigids, mbd%nRigids, mbd%nMarkersRB )
        
        
        ! --- Solver dependent allocations ---
        if (sys%solType <= SOL_VERLE) then
            call slvJAC__initSolVars( svj, mbd%mFB, mbd%nNodes, constrExpl, nConstrExpl,&
                                      q0, v0, mbd%nConstrSec )
            
            call slvJAC__updateSolVars( svj, rMaFB, rMaFB_d, mbd%nNodes, &
                                        mbd%rMaCS, mbd%nMaCS, &
                                        mbd%constrSec, mbd%nConstrSec, &
                                        sys%constr_kp, sys%constr_ki, &
                                        sys%constr_kd, sys%tStep)
        
        else if (sys%solType >= SOL_RRF_1) then
            
            allocate (mFBInv(mbd%nNodes))
            mFBInv = 1.0_prec/mbd%mFB
            
            allocate (rMaFB0  (3, mbd%nNodes))
            allocate (rMaFB_d0(3, mbd%nNodes))
            allocate (rMaFB_dd(3, mbd%nNodes))
            
            rMaFB_dd(:,:) = ZE
            
            allocate (fRFB  (3, mbd%nNodes))
            fRFB(:,:) = ZE
            
            allocate (fEFB0 (3, mbd%nNodes))
            
#include init__experimental
            
        end if
        
        deallocate (q0, v0)
        
        
        ! --- Calculation of flexible body internal forces ---
        if (calcInternalForces)then
            call solLAGR__initJacobian( svl,mbd%nNodes, mbd%nConstr, &
                                        mbd%constrSec, mbd%nConstrSec )
            if (sys%solType < SOL_RRF_1) allocate (rMaFB_dd(3, mbd%nNodes))
            allocate(fBRFB(3, mbd%nConstr))
            fBRFB(:,:) = ZE
        end if
        
        
        
        ! --- Additional output of water surface elevation ---
        if(sys%writeWaterSurface) then
            allocate(rSurf(3,sys%waterSurfacenDiv+1))
            allocate(zSurf(sys%waterSurfacenDiv+1))
            
            wSurfLDiv = (sys%waterSurfaceXMax-sys%waterSurfaceXMin)/sys%waterSurfaceNDiv
            
            rSurf(:,:) = ZE
            
            do iPos = 0,sys%waterSurfaceNDiv
                rSurf(1,iPos+1) = sys%waterSurfaceXMin + iPos*wSurfLDiv
            end do
        end if
        
        
        ! --- Clear previous output files ---
        inquire (file=trim(sys%outDir)//'.', exist=outDirExists)
        if(.not. outDirExists) then
            call system(os__mkDir  // trim(sys%outDir) )
        else
            call system(os__del    // trim(sys%outDir) // '*csv*')
            call system(os__delDir // trim(sys%outDir) // 'vtk')
            call system(os__del    // trim(sys%outDir) // 'common')
            call system(os__del    // trim(sys%outDir) // 'constr')
        end if


        sysDyn__init = .true.
    END FUNCTION
    

    !***************************************************************************
    ! Clean-up
    !***************************************************************************
    SUBROUTINE sysDyn__cleanup()
        ! Description___________________________________________________________
        !  Here we basically clean up what was allocated in sysDyn__init and
        !  nullify some pointers.
        ! ______________________________________________________________________
        implicit none


        ! Common data___________________________________________________________
        ! ______________________________________________________________________
        ! General data
        deallocate(uRelFB,uFB)
        deallocate(uRelRB,uRB)
        
        deallocate(fEFB)
        deallocate(fGFB)
        deallocate(fBFB)
        deallocate(fHyd)
        
        deallocate(fGRB)
        deallocate(fBRB)
        deallocate(fERB)
        deallocate(fERBMa)
        deallocate(fHydRB)
        
        deallocate(qRB, qRB_d, qRB_dd)
        deallocate(omRB)
        deallocate(TRB)
        deallocate(PRB)
        deallocate(PRB_d)
        
        if (calcInternalForces) then
            deallocate(fBRFB)
            deallocate (svl%G, svl%G_T)
            if (sys%solType < SOL_RRF_1) deallocate (rMaFB_dd)
        end if
        
        if(sys%writeWaterSurface) then
            deallocate(rSurf)
            deallocate(zSurf)
        end if

        ! Data associated to mbsData struct 'mbd'
        call mbsData__cleanup(mbd)

        ! Data associated to flmData struct 'fld'
        call flmData__cleanup(fld)


        ! Solver-specific data__________________________________________________
        ! ______________________________________________________________________
        if (sys%solType <= SOL_VERLE) then
            call slvJAC__cleanupSolVars(svj)
        
        else if (sys%solType>=SOL_RRF_1) then
            deallocate (mFBInv)
            deallocate (rMaFB0)
            deallocate (rMaFB_d0)
            deallocate (rMaFB_dd)

            deallocate (fRFB)
            deallocate (fEFB0)

#include cleanup__experimental

        end if

        deallocate(rMaFB)
        deallocate(rMaFB_d)

        deallocate(rMaRB)
        deallocate(rMaRB_d)
    END SUBROUTINE


    !***************************************************************************
    ! Time integration
    !***************************************************************************
    LOGICAL FUNCTION sysDyn__timeInt()
        implicit none

        integer         :: iStep                 ! Current timestep
        real(prec)      :: tStart, tStop, tInter ! Timing values
        integer         :: iNode                 ! ID of current node
        integer         :: iRB                   ! ID of current rigid body
        real(prec)      :: tInt                  ! Current integration time
        
        real(prec), parameter :: loadRatio = 1.0_prec ! Ramped loads ratio -- TODO: not yet implemented
        
        sysDyn__timeInt = .false.
        
        
        iStep = 0
        tInt  = ZE
        call writeTimeStep()

        ! Start timer
        call cpu_time(tStart)
        
        ! Time integration main loop
        do iStep = 1,sys%nSteps            
            ! --- Flexible bodies ---
            select case (sys%solType)
            
                        
            ! Lagrangian dynamics -- explicit Euler method
            case (SOL_EULEX)
                call sysDyn__calcExternalForces(fEFB, fERB, tInt, loadRatio)
                
                call slvJAC__slvAccFB(svj, fEFB+svj%fCo, mbd%nNodes)

                svj%q = svj%q + svj%v*sys%tStep
                svj%v = svj%v + svj%a*sys%tStep

                call slvJAC__updateSolVars( svj, rMaFB, rMaFB_d, mbd%nNodes, &
                                            mbd%rMaCS, mbd%nMaCS, mbd%constrSec, mbd%nConstrSec, &
                                            sys%constr_kp, sys%constr_ki, sys%constr_kd, sys%tStep)
            
            
            ! Lagrangian dynamics -- velocity-Verlet integration step
            case (SOL_VERLE)
                call sysDyn__calcExternalForces(fEFB, fERB, tInt, loadRatio)
                
                call slvJAC__slvAccFB(svj, fEFB+svj%fCo, mbd%nNodes)

                svj%q = (0.5_prec*sys%tStep**2)*svj%a + sys%tStep*svj%v + svj%q
                svj%v = svj%a*sys%tStep + svj%v

                call slvJAC__updateSolVars( svj, rMaFB, rMaFB_d, mbd%nNodes, &
                                            mbd%rMaCS, mbd%nMaCS, &
                                            mbd%constrSec, mbd%nConstrSec, &
                                            sys%constr_kp, sys%constr_ki, &
                                            sys%constr_kd, sys%tStep)
            
            
            ! Reconstructed reaction forces formulation Type I
            case (SOL_RRF_1)
                rMaFB0   = rMaFB
                rMaFB_d0 = rMaFB_d
                
                call sysDyn__calcExternalForces(fEFB0, fERB, tInt, loadRatio)

                call solRRF__calcProjectorStep ( rMaFB, rMaFB_d, rMaFB_dd, rMaFB0, & 
                                                 mbd%nNodes, mbd%rMaCS, mbd%nMaCS, &
                                                 fEFB0, mFBInv, mbd%mFB, &
                                                 mbd%constr, mbd%nConstr, &
                                                 mbd%constrSec, mbd%nConstrSec, &
                                                 sys%rrf_wtFac, sys%rrf_nIter, sys%tStep )
                
                call sysDyn__calcExternalForces(fEFB, fERB, tInt+sys%tStep, loadRatio)
                
                call solRRF__calcCorrectorStep ( rMaFB, rMaFB_d, rMaFB_dd, &
                                                 rMaFB0, rMaFB_d0, mbd%nNodes,  &
                                                 mbd%rMaCS, mbd%nMaCS, &
                                                 fEFB-fEFB0, mFBInv, mbd%mFB, &
                                                 mbd%constr, mbd%nConstr, &
                                                 mbd%constrSec, mbd%nConstrSec, &
                                                 sys%rrf_wtFac, sys%rrf_nIter, sys%tStep )
            
            
            ! Reconstructed reaction forces formulation Type II
            case (SOL_RRF_2)
                rMaFB0   = rMaFB
                rMaFB_d0 = rMaFB_d
                
                call sysDyn__calcExternalForces(fEFB0, fERB, tInt, loadRatio)
                
                call solRRF__calcProjectorStep ( rMaFB, rMaFB_d, rMaFB_dd, rMaFB0, & 
                                                 mbd%nNodes, mbd%rMaCS, mbd%nMaCS, &
                                                 fEFB0+0.5_prec*fRFB, mFBInv, mbd%mFB, &
                                                 mbd%constr, mbd%nConstr, &
                                                 mbd%constrSec, mbd%nConstrSec, &
                                                 sys%rrf_wtFac, sys%rrf_nIter, sys%tStep )
                
                call sysDyn__calcExternalForces(fEFB, fERB, tInt+sys%tStep, loadRatio)
                
                call solRRF__calcCorrectorStep ( rMaFB, rMaFB_d, rMaFB_dd, &
                                                 rMaFB0, rMaFB_d0, mbd%nNodes,  &
                                                 mbd%rMaCS, mbd%nMaCS, &
                                                 fEFB-fEFB0, mFBInv, mbd%mFB, &
                                                 mbd%constr, mbd%nConstr, &
                                                 mbd%constrSec, mbd%nConstrSec, &
                                                 sys%rrf_wtFac, sys%rrf_nIter, sys%tStep )
                
                fRFB = matmulDiagRight( rMaFB_dd, mbd%mFB, 3, mbd%nNodes ) - 0.5_prec*(fEFB0+fEFB)

#include timeInt__experimental
                
            end select
            
            if ( isnan(rMaFB(1,1)) ) then
                print *, "Bazinga! Your simulation just crashed."
                return
            end if
            
            
            ! --- Rigid bodies ---
            qRB_dd = mbsDyn__slvAccsRB( qRB_d, omRB, TRB, PRB, PRB_d, &
                                        fERB, mbd%rigids, mbd%nRigids )
            
            if (sys%solType<=SOL_EULEX) then
                qRB   = qRB   + qRB_d*sys%tStep
                qRB_d = qRB_d + sys%tStep*qRB_dd
            else
                qRB   = (0.5*sys%tStep**2)*qRB_dd + sys%tStep*qRB_d + qRB
                qRB_d = qRB_d + sys%tStep*qRB_dd
            end if
            
            ! Normalise Euler parameters of rigid bodies
            do iRB = 1,mbd%nRigids
                qRB(4:7,iRB) = qRB(4:7,iRB)/sqrt(sum(qRB(4:7,iRB)**2))
            end do
            
            call mbsDyn__updateSolVarsRB( rMaRB, rMaRB_d, omRB, TRB, PRB, PRB_d, &
                                          qRB, qRB_d, mbd%rigids, mbd%nRigids, mbd%nMarkersRB )
            
            
            ! --- Increase current time and save state vector ---
            tInt = iStep*sys%tStep
            if (mod(iStep,sys%nOut)==0) call writeTimeStep()
        end do
        
        
        ! Stop timer and display time elapsed
        call cpu_time(tStop)
        write (*,*) 'Done in ', tStop-tStart, 's'
        
        sysDyn__timeInt = .true.
        
    CONTAINS

        SUBROUTINE writeTimeStep
            implicit none
            
            character(1024) :: fName            ! Name of current timestep's output file
            integer         :: ioUnit, ioError  ! IO unit and error flag
            integer         :: iPos             ! Counting index for water surface output positions
            integer         :: iCo
            
            ! --- Average calculation time per timestep ---
            tInter = tStop
            call cpu_time(tStop)
            
            write (*,'( f8.3, A, f8.3, A, e10.3, A)') tInt, ' of ', sys%tEnd, &
                                                      's. Time per Step: ', &
                                                      (tStop-tInter)/sys%nOut, 's'
            
            ! --- Cartesian coordinates ---
            fName = trim(sys%outDir) // 'massp.csv.' // trim(num2Str(iStep/sys%nOut))
            
            open(unit=loeFile__newUnit(ioUnit) , file=fName, status='replace', action='write', iostat=ioError)
            
            do iNode = mbd%nMaCS,1,-1
                write (ioUnit,*) mbd%rMaCS(:,iNode)
            end do
            
            do iNode = 1,mbd%nNodes
                write (ioUnit,*) rMaFB(:,iNode)
            end do
            
            close(ioUnit)


            ! --- Flexible body internal forces ---
            if (calcInternalForces) then
                if (sys%solType<=SOL_VERLE) then
                    call sysDyn__calcExternalForces(fEFB, fERB, tInt, loadRatio)
                
                    call slvJAC__slvAccFB(svj, fEFB+svj%fCo, mbd%nNodes)
                    
                    call slvJAC__calcCartAccFB( svj, rMaFB_dd, rMaFB, mbd%nNodes, &
                                                mbd%rMaCS, mbd%nMaCS )
                end if
                
#include calcAcc__experimental
                
                call solLAGR__calcJacobian( svl%G, rMaFB, mbd%rMaCS, mbd%constr, &
                                            mbd%nConstr )
                
                svl%G_T = transpose(svl%G)
                
                call solLAGR__calcInternalReations( svl, fBRFB, rMaFB_dd, fEFB, &
                                                    mbd%mFB, mbd%nNodes, mbd%constr, &
                                                    mbd%nConstr, mbd%nConstrSec )
                
                fName = trim(sys%outDir) // 'react.csv.' // trim(num2Str(iStep/sys%nOut))
                
                open(unit=loeFile__newUnit(ioUnit) , file=fName, status='replace', action='write', iostat=ioError)
                
                do iCO = 1,mbd%nConstr
                    write (ioUnit,*) sqrt(sum(fBRFB(:,iCo)**2))
                end do
                
                close(ioUnit)
            else
                fName = trim(sys%outDir) // 'react.csv.' // trim(num2Str(iStep/sys%nOut))
                
                open(unit=loeFile__newUnit(ioUnit) , file=fName, status='replace', action='write', iostat=ioError)
                
                do iCO = 1,mbd%nConstr
                    write (ioUnit,*) ZE
                end do
                
                close(ioUnit)
            end if


            ! --- Rigid body poses ---
            if (mbd%nRigids>0) then
                fName = trim(sys%outDir) // 'rigpo.csv.' // trim(num2Str(iStep/sys%nOut))
                
                open(unit=loeFile__newUnit(ioUnit) , file=fName, status='replace', action='write', iostat=ioError)
                
                do iRB = 1,mbd%nRigids
                    write (ioUnit,*) qRB(:,iRB)
                end do
                
                close(ioUnit)
            end if


            ! --- Lagrangian dynamics only: joint angles and velocities ---
            if (sys%solType < SOL_RRF_1) then
                fName = trim(sys%outDir) // 'jntpo.csv.' // trim(num2Str(iStep/sys%nOut))
                
                open(unit=loeFile__newUnit(ioUnit) , file=fName, status='replace', action='write', iostat=ioError)
                    write (ioUnit,*) svj%q
                close(ioUnit)

                fName = trim(sys%outDir) // 'jntvl.csv.' // trim(num2Str(iStep/sys%nOut))
                open(unit=loeFile__newUnit(ioUnit) , file=fName, status='replace', action='write', iostat=ioError)
                    write (ioUnit,*) svj%v
                close(ioUnit)
            end if
            
            
            
            ! --- Water surface elevation ---
            if (sys%writeWaterSurface) then
                fName = trim(sys%outDir) // 'wsurf.csv.' // trim(num2Str(iStep/sys%nOut))
                
                open(unit=loeFile__newUnit(ioUnit) , file=fName, status='replace', action='write', iostat=ioError)
                
                call flmDyn__calcSurfaceElevation(zSurf,rSurf,fld,tInt)
                
                do iPos = 1,sys%waterSurfacenDiv+1
                    write (ioUnit,*) rSurf(1,iPos), rSurf(2,iPos), zSurf(iPos)
                end do
                
                close(ioUnit)
            end if
        END SUBROUTINE

    END FUNCTION sysDyn__timeInt


    !***************************************************************************
    ! Calculate external forces
    !***************************************************************************
    SUBROUTINE sysDyn__calcExternalForces(fEFB,fERB,tInt,scaleFac)
        ! Description___________________________________________________________
        !   Calculates external forces on flexible bodies and forces+torques
        !   on rigid bodies. This one contains a lot glues code between reference
        !   coordinate systems, flexible bodies and rigid bodies.
        !   TODO: - incorporate rigid body marker velocities!!!
        !         - include rotational damping
        ! ______________________________________________________________________
        implicit none

        real(prec), intent(inout) :: fEFB(3,mbd%nNodes)
        real(prec), intent(inout) :: fERB(6,mbd%nRigids)
        
        real(prec), intent(in)    :: tInt     ! Current time
        real(prec), intent(in)    :: scaleFac ! Load scaling factor

        integer    :: iRB     ! Index of current rigid body
        integer    :: iBar    ! Index of current bar element
        integer    :: iSp     ! Index of spring currently being calculated
        integer    :: iP      ! Index of PID element currently being calculated
        real(prec) :: r1(3)   ! Position of node 1 (spring and bar type force elements)
        real(prec) :: r2(3)   ! Position of node 1 (spring  and bar type force elements)
        real(prec) :: r1_d(3) ! Velocity of node 1 (spring/damper)
        real(prec) :: r2_d(3) ! Velocity of node 1 (spring/damper)
        real(prec) :: fSpr(3) ! Force resulting from spring-damper element
        real(prec) :: fPID(3) ! Force resulting from PID element
        real(prec) :: bN(3)   ! Bar element normal vector
        

        ! Hydrodynamic forces, gravity, buoyancy________________________________
        ! ______________________________________________________________________
        
        ! --- Flexible bodies ---
        call flmDyn__calcVelocities(uFB,rMaFB,fld,tInt)
        uRelFB = uFB - rMaFB_d

        if (fld%simpDamp) then
            fHyd = fld%dampFac*matmulDiagRight(uRelFB,mbd%mFB,3,mbd%nNodes)
        else
            fHyd(:,:) = ZE
            
            do iBar = 1,mbd%nBars
                associate ( bar => mbd%fElems__bar(iBar) )
                    
                    ! (No calculation if node k1/k2 is fixed or uRel=0.0)
                    r1 = mbsDyn__markerPosFrom_ID_RT(bar%k1,bar%k1RT,mbd%rMaCS, rMaFB, rMaRB)
                    r2 = mbsDyn__markerPosFrom_ID_RT(bar%k1,bar%k1RT,mbd%rMaCS, rMaFB, rMaRB)
                    
                    bN = (r2-r1)/bar%l

                    if (bar%k1RT==MTYPE__FLEX)   fHyd(:,bar%k1) = fHyd(:,bar%k1) + &
                        flmDyn__calcForceBar(bN, uRelFB(:,bar%k1), fld%rho, bar%AProj, fld%rope__coeffs(bar%IDCT)%vals)
                    
                    if (bar%k2RT==MTYPE__FLEX)   fHyd(:,bar%k2) = fHyd(:,bar%k2) + &
                        flmDyn__calcForceBar(bN, uRelFB(:,bar%k2), fld%rho, bar%AProj, fld%rope__coeffs(bar%IDCT)%vals)
                    
                end associate
            end do
        end if

        fEFB  = fGFB + fBFB + fHyd
        
        ! --- Rigid bodies ---
        if (mbd%nRigids>0) then
            call flmDyn__calcVelocities(uRB,qRB(1:3,:),fld,tInt)
            uRelRB = uRB - qRB_d(1:3,:)
            
            do iRB = 1,mbd%nRigids
            
                select case (mbd%rigids(iRB)%loadType)
                
                case (RBOD_SIMPLE_DAMP)
                    fHydRB(1:3,iRB) = 0.5_prec*fld%rho* &
                                      uRelRB(:,iRB)*sqrt(sum(uRelRB(:,iRB)**2))* &
                                      mbd%rigids(iRB)%A0*mbd%rigids(iRB)%CD
                                      
                    fHydRB(4:6,iRB) = ZE
                
                case (RBOD_HYDROFOIL)
                    fHydRB(:,iRB) = flmDyn__calcForceHydrofoil( &
                        TRB(:,:,iRB), uRelRB(:,iRB), fld%rho, mbd%rigids(iRB)%A0, &
                        fld%rope__coeffs(mbd%rigids(iRB)%IDCT)%vals )
                
                case (RBOD_BUOY)
                    fHydRB(:,iRB) = flmDyn__calcForceCylindricalBuoy( &
                        qRB(:,iRB), qRB_d(:,iRB), omRB(:,iRB), TRB(:,:,iRB), &
                        mbd%rigids(iRB)%dBuoy, mbd%rigids(iRB)%lBuoy, &
                        mbd%rigids(iRB)%CDLat, mbd%rigids(iRB)%CDLong, &
                        mbd%rigids(iRB)%nDivBuoy, tInt, sys%gVec, fld )
                    
                end select
            end do
            
            fERB        = fHydRB
            fERB(1:3,:) = fERB(1:3,:) + fGRB + fBRB

            ! Torques induced by buoyancy
            do iRB = 1,mbd%nRigids
                fERB(4:6,iRB) = fERB(4:6,iRB) + crossProduct( matmul(TRB(:,:,iRB),mbd%rigids(iRB)%rCOB), fBRB(:,iRB) )
            end do
        end if


        ! Springs_______________________________________________________________
        ! ______________________________________________________________________
        if (mbd%nRigids>0) then
            fERBMa(:,:) = ZE
        end if

        do iSp = 1,mbd%nSprings
            associate ( spring => mbd%springs(iSP))
            
            
            call mbsDyn__markerStateFrom_ID_RT( r1, r1_d, spring%k1, spring%k1RT,&
                                                mbd%rMaCS, rMaFB, rMaFB_d, rMaRB, rMaRB_d )
            
            call mbsDyn__markerStateFrom_ID_RT( r2, r2_d, spring%k2, spring%k2RT,&
                                                mbd%rMaCS, rMaFB, rMaFB_d, rMaRB, rMaRB_d )
            
            fSpr = (r2-r1)*spring%c + (r2_d-r1_d)*spring%d
            
            call mbsDyn__applyForceBy_ID_RT( fEFB, fERBMa,  fSpr, spring%k1, spring%k1RT)
            
            call mbsDyn__applyForceBy_ID_RT( fEFB, fERBMa, -fSpr, spring%k2, spring%k2RT)

            end associate
        end do
        
        
        do iP = 1,mbd%nPIDs
            associate ( elPID => mbd%pids(iP))
            
            call mbsDyn__markerStateFrom_ID_RT( r1, r1_d, elPID%k1, elPID%k1RT,&
                                                mbd%rMaCS, rMaFB, rMaFB_d, rMaRB, rMaRB_d )
            
            call mbsDyn__markerStateFrom_ID_RT( r2, r2_d, elPID%k2, elPID%k2RT,&
                                                mbd%rMaCS, rMaFB, rMaFB_d, rMaRB, rMaRB_d )
            
            call mbsTools__calcForcePID( elPID%rt__errI, fPID, r1, r1_d, r2, r2_d, &
                                         elPID%kp, elPID%ki, elPID%kd,sys%tStep)
            
            call mbsDyn__applyForceBy_ID_RT( fEFB, fERBMa,  fPID, elPID%k1, elPID%k1RT )
            
            call mbsDyn__applyForceBy_ID_RT( fEFB, fERBMa, -fPID, elPID%k2, elPID%k2RT)

            end associate
        end do
        
        ! Update rigid body load vector_________________________________________
        ! ______________________________________________________________________
        ! Transform marker forces to force+torque w.r.t. center of gravity
        do iRB = 1,mbd%nRigids
            associate ( rBody=>mbd%rigids(iRB) )
                fERB(:,iRB) = fERB(:,iRB) + mbsDyn__sumMarkerForcesRB( &
                    fERBMa(:, rBody%iNRef:rBody%iNRef+rBody%nMarkers-1), &
                    rBody%markerPosns, TRB(:,:,iRB) )
            end associate
        end do
        
        
        ! Load Scaling__________________________________________________________
        ! ______________________________________________________________________
        fEFB = fEFB*scaleFac
        fERB = fERB*scaleFac
        
    END SUBROUTINE


    !***************************************************************************
    ! Write common case data to output directory
    !***************************************************************************
    SUBROUTINE sysDyn__writeCaseData()
        implicit none

        integer :: ioUnit_CD ! IO-unit IDs of common data output file
        integer :: ioUnit_CO ! IO-unit IDs of constraint data output file
        integer :: ioUnit_RB ! IO-unit IDs of rigid body data output file
        integer :: iCo       ! ID of current constraint
        integer :: iRb       ! ID of current rigid body
        integer :: iMaRb     ! ID of marker on rigid body
        integer :: ioError   ! IO error flag


        ! Common data
        open( unit=loeFile__newUnit(ioUnit_CD), file=trim(sys%outDir)//'common', &
              status='replace', action='write', iostat=ioError )
        write (ioUnit_CD,'(A,i0)')    'nSteps          ', sys%nSteps/sys%nOut
        write (ioUnit_CD,'(A,i0)')    'nMarkers        ', mbd%nMaCS + mbd%nNodes
        write (ioUnit_CD,'(A,i0)')    'nMaCS ', mbd%nMaCS
        
        if (sys%writeWaterSurface) then
            write (ioUnit_CD,'(A,i0)')    'waterSurfaceNPos ', sys%waterSurfaceNDiv+1
        else
            write (ioUnit_CD,'(A,i0)')    'waterSurfaceNPos ', 0
        end if
        
        write (ioUnit_CD,'(A,e12.5)') 'total mass     ', sum(mbd%mFB)
        
        close(ioUnit_CD)


        ! Constraints
        open( unit=loeFile__newUnit(ioUnit_CO), file=trim(sys%outDir)//'constr', &
              status='replace', action='write', iostat=ioError )
        
        do iCo = 1,mbd%nConstr
            if (mbd%constr(iCo)%k1RT==MTYPE__FLEX) then
                write (ioUnit_CO,'(i0,A, i0)') mbd%constr(iCo)%k1, ' ', mbd%constr(iCo)%k2
            else
                write (ioUnit_CO,'(i0,A, i0)') -mbd%constr(iCo)%k1+1, ' ', mbd%constr(iCo)%k2
            end if
        end do
        

        close(ioUnit_CO)


        ! Rigid body data
        open( unit=loeFile__newUnit(ioUnit_RB), file=trim(sys%outDir)//'rigids', &
              status='replace', action='write', iostat=ioError )
        
        write(ioUnit_RB,'(A,i0)') 'nRigids      ', mbd%nRigids
        
        do iRb = 1,mbd%nRigids
            
            if (mbd%rigids(iRb)%loadType<RBOD_BUOY) then
                write(ioUnit_RB,'(A,e12.5)') 'lx          ' , mbd%rigids(iRb)%vol**(1.0_prec/3.0_prec)
                write(ioUnit_RB,'(A,e12.5)') 'ly          ' , mbd%rigids(iRb)%vol**(1.0_prec/3.0_prec)
                write(ioUnit_RB,'(A,e12.5)') 'lz          ' , mbd%rigids(iRb)%vol**(1.0_prec/3.0_prec)
            else
                write(ioUnit_RB,'(A,e12.5)') 'lx          ' , mbd%rigids(iRb)%dBuoy
                write(ioUnit_RB,'(A,e12.5)') 'ly          ' , mbd%rigids(iRb)%dBuoy
                write(ioUnit_RB,'(A,e12.5)') 'lz          ' , mbd%rigids(iRb)%lBuoy
            end if
            
            write(ioUnit_RB,'(A,i0)')    'nMarkers     ', mbd%rigids(iRb)%nMarkers

            do iMaRb = 1,mbd%rigids(iRb)%nMarkers
                write(ioUnit_RB,*) mbd%rigids(iRb)%markerPosns(1,iMaRb), ' ', &
                                   mbd%rigids(iRb)%markerPosns(2,iMaRb), ' ', &
                                   mbd%rigids(iRb)%markerPosns(3,iMaRb)
            end do
            
        end do

    END SUBROUTINE sysDyn__writeCaseData


    !***************************************************************************
    ! Calculate Cartesian positions of resulting timeseries and write output
    !***************************************************************************
    SUBROUTINE sysDyn__convertResults()
        ! Calls python script to convert data to vtk
        implicit none

        character(MAXSTRL_FILE__FNAME) :: cmdSTR ! command string for python script


        cmdSTR = 'python ' // trim(rootDir) // os__pathSep // 'prepost' // os__pathSep // 'timeSeriesToVTK.py ' // &
                     trim(sys%outDir) // 'common'

        print *, 'Converting results to vtk file timeseries. Executing command:' // endl // ' ' // trim(cmdSTR)
        call system(trim(cmdSTR))

        print *, 'Done'
    END SUBROUTINE

END MODULE sys__Dyn
