!*******************************************************************************
!   OCN-SIM Flex
!   Copyright (C) 2017 by Christoph Otto
!   christoph@ocnacademy.org
!   www.ocnacademy.org
!
!
!   MODULE :: File handling
!
!   PURPOSE
!   File handling
!
!   
!     ___   ____ _   _    ____ ___ __  __    _____ _     _______  __
!    / _ \ / ___| \ | |  / ___|_ _|  \/  |  |  ___| |   | ____\ \/ /
!   | | | | |   |  \| |  \___ \| || |\/| |  | |_  | |   |  _|  \  / 
!   | |_| | |___| |\  |   ___) | || |  | |  |  _| | |___| |___ /  \
!    \___/ \____|_| \_|  |____/___|_|  |_|  |_|   |_____|_____/_/\_\
!
!
!   LICENSE
!   This file is part of OCN-SIM Flex.
!
!   OCN-SIM Flex is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   OCN-SIM Flex is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with OCN-SIM Flex. If not, see <http://www.gnu.org/licenses/>.
!
!*******************************************************************************

MODULE liboe__File
    
    USE liboe__Conf
    USE liboe__Types
    USE liboe__Tools
    USE liboe__Err
    
    implicit none
    
    
    !***************************************************************************
    ! Module description                                                       !
    !***************************************************************************
    character(*), parameter, private :: modName = 'liboe__File'
    
    
    !***************************************************************************
    ! Public entities                                                          !
    !***************************************************************************
    PUBLIC :: loeFile
    PUBLIC :: loeFile__newUnit, loeFile__open, loeFile__close
    PUBLIC :: loeFile__readSubVars
    PUBLIC :: loeFile__readVal, loeFile__readNEl, loeFile__readEID
    PUBLIC :: loeFile__readSectionStart, loeFile__readNumberedEntity
    PUBLIC :: loeFile__readVector, loeFile__readMatrix, loeFile__loadMatrixFromFile
    
    
PRIVATE
    
    ! Substitution variable data type
    TYPE :: loeSubVar
        character(MAX_STRINGVAL_LENGTH) :: name
        character(MAX_STRINGVAL_LENGTH) :: sval
    END TYPE
    
    TYPE :: loeFile
        ! TODO: add ioErr to this one. check if isOpen in every routine
        character(MAXSTRL_FILE__FNAME)  :: fName      ! Filename
        
        logical                         :: isOpen     ! Indicating, whether the file is currently opened
        integer,private                 :: iUnit      ! Input unit for main input file
        
        character(MAX_FILE_LINE_LENGTH) :: lStr       ! Current line string
        integer                         :: lineNum    ! Current line number. Initialised to 0 and increased by every call of readLine()
        
        integer                         :: nSubVars   ! Number of substitution variables
        type(loeSubVar), allocatable    :: subVars(:) ! Substitution variables
    END TYPE
    
    !***************************************************************************
    ! Overloaded interfaces
    !***************************************************************************
    INTERFACE loeFile__readVal
        MODULE PROCEDURE loeFile__readReal, loeFile__readInt, loeFile__readStr
    END INTERFACE
    
    INTERFACE loeFile__readMatrix
        MODULE PROCEDURE loeFile__readMatrix_Real, loeFile__readMatrix_Int
    END INTERFACE
    
    INTERFACE loeFile__loadMatrixFromFile
        MODULE PROCEDURE loeFile__loadMatrixFromFile_Real, loeFile__loadMatrixFromFile_Int
    END INTERFACE
    
CONTAINS
    
    !***************************************************************************
    ! Generate new IO-Unit
    !***************************************************************************
    integer FUNCTION loeFile__newUnit(ioUnit)
        ! Finds and return new IO-unit as return value and as a parameter Thus,
        ! it can be used directly in an open(...) statement, plus we get the ID
        ! as a variable, which we can use for subsequent IO-calls as well as
        ! closing the file. Based on http://fortranwiki.org/fortran/show/newunit
        implicit none
        
        integer, intent(out), optional :: ioUnit    ! New IO-unit ID

        integer, parameter             :: IOU_MIN=10, IOU_MAX=1000
        logical                        :: isOpen
        integer                        :: ioUCurr
        
        
        loeFile__newUnit=-1
        do ioUCurr = IOU_MIN,IOU_MAX
            inquire(unit=ioUCurr, opened=isOpen)
            
            if (.not. isOpen) then
                loeFile__newUnit = ioUCurr
                exit
            end if
        end do
        
        if (present(ioUnit)) ioUnit = loeFile__newUnit
    END FUNCTION loeFile__newUnit
    
    
    !***************************************************************************
    ! open input file
    !***************************************************************************
    logical FUNCTION loeFile__open(this, fName)
        implicit none
        
        type(loeFile), intent(inout) :: this  ! loeFile structure
        character(*), intent(in)     :: fName ! Input filename
        type(loeErr)                 :: err   ! Error handling class
        integer                      :: ioErr ! I/O status
        
        err           = loeErr__init('loeFile__open',modName)
        this%fName    = ''
        this%iUnit    = 0
        this%isOpen   = .false.
        loeFile__open = .false.
        this%nSubVars   = 0
        
        
        open(unit=loeFile__newUnit(this%iUnit), file=trim(fName), status='old', action='read', iostat=ioErr)
        if (ioErr == 0) then
            this%fName    = fName
            this%lineNum  = 0
        else
            call loeErr__print(err,'Cannot open file ' //quo// trim(fName) //quo)
            return
        end if
        
        this%isOpen   = .true.
        loeFile__open = .true.
        
    END FUNCTION loeFile__open
    

    !***************************************************************************
    ! Close input file
    !***************************************************************************    
    SUBROUTINE loeFile__close(this)
        implicit none
        type(loeFile), intent(inout) :: this
        
        if (allocated(this%subVars)) deallocate(this%subVars)
        
        close(this%iUnit)
        this%iUnit  = 0
        this%isOpen = .false.
    END SUBROUTINE loeFile__close
    
    
    !***************************************************************************
    ! Read substitution variables
    !***************************************************************************    
    logical FUNCTION loeFile__readSubVars(this)
        implicit none
        
        type(loeFile), intent(inout) :: this
        
        integer :: iSVar         ! Counting index
        integer :: iSVCurr       ! Current svar id read from file
        
        loeFile__readSubVars = .false.
        
        
        readVals: do
            if (.not. loeFile__readNEl(this, this%nSubVars, 0, 'nSubVars')) exit readVals ! Number of markers in CSYS0
            
            if (this%nSubVars>0) allocate(this%subVars(this%nSubVars))
            
            do iSVar=1,this%nSubVars
                if (.not. loeFile__readEID(this, iSVCurr, iSVar, 'SUBVAR'))        exit readvals
                if (.not. loeFile__readVal(this, this%subVars(iSVar)%name, 'name')) exit readVals
                if (.not. loeFile__readVal(this, this%subVars(iSVar)%sval, 'sval')) exit readVals
            end do
            
            loeFile__readSubVars = .true.
            exit readVals
        end do readvals
        
    END FUNCTION
    
    
    !***************************************************************************
    ! Read line string
    !***************************************************************************   
    logical FUNCTION loeFile__readLn(this)
        ! Automatically skips empty lines and comment lines starting with # or ;
        implicit none
        
        type(loeFile), intent(inout) :: this
        
        integer                      :: ioErr ! I/O status
        type(loeErr)                 :: err   ! Error handling class
        
        err             = loeErr__init('loeFile__readLn',modName)
        loeFile__readLn = .false.
        this%lStr       = ''
        
        
        do while (len(trim(this%lStr))==0 .or. this%lStr(1:1)=='#' .or. this%lStr(1:1)==';')
            read(this%iUnit, fmt='(A)', iostat=ioErr) this%lStr
            
            select case (ioErr)
            case (:-1)
                call loeErr__print(err, 'Trying to read past the end of the of the file.' // endl // &
                                        ' Please check for missing values.')
                return
            case (1:)
                call loeErr__print(err, ' Error code: ' // trim(num2Str(ioErr)))
                return
            end select
            
            this%lineNum = this%lineNum+1
        end do
        
        loeFile__readLn = .true.
    END FUNCTION loeFile__readLn 
    
    
    !***************************************************************************
    ! Read assignment and return value as a string
    !***************************************************************************   
    logical FUNCTION loeFile__readStr(this, stringVal, ident)
        ! Reads assignment in the form ident = stringVal and checks for validity
        implicit none
        
        type(loeFile),                   intent(inout) :: this      ! loeFile structure
        character(MAX_STRINGVAL_LENGTH), intent(out)   :: stringVal ! String value to be read
        character(*),                    intent(in)    :: ident     ! Expected identifier of value
        
        integer                                        :: ioErr     ! I/O status
        character(64)                                  :: identRead ! Actual identifier read from file
        character(8)                                   :: assOp     ! Actual assignment operator read from file
        integer                                        :: iSV       ! Counting index for substitution variables
        logical                                        :: foundSV   ! Did we find the subvar?
        type(loeErr)                                   :: err       ! Error handling class
        
        err              = loeErr__init('loeFile__readStr',modName)
        loeFile__readStr = .false.
        identRead        = ''
        assOp            = ''
        
        ! --- Read value and do basic checking ---
        if (.not. loeFile__readLn(this)) then
            call loeErr__print( err, 'Expected: ' //quo// ident //quo// &
                                ' = ...' // endl // ' ' // errStr_Identifier ) 
            return
        end if
        
        read (this%lStr,*, iostat = ioErr) identRead, assOp, stringVal
        
        if (ioErr==0) then 
            if (trim(assOp) /= '=') then
                call loeErr__print(err, 'Illegal assignment operator: ' //quo// trim(assOp) //quo// endl // &
                                        ' Please use a ' //quo// '=' //quo// ' sign separated by whitespaces')
                return
            end if
            
            if (trim(identRead) /= ident) then
                call loeErr__print(err, 'Illegal identifier: ' //quo// trim(identRead) //quo // endl // &
                                        ' Expected: ' //quo// ident //quo// endl // ' ' // errStr_Identifier)
                return
            end if
        else
            call loeErr__print(err, 'Cannot read value in assignment ' //quo// ident // ' = ...' //quo// endl // &
                                    ' Error code: ' // trim(num2Str(ioErr)))
            return
        end if
        
        
        ! --- Check for substitution variable ---
        if (  stringVal(1:1) == '$' ) then
            foundSV = .false.
            
            findSubVar: do iSV = 1,this%nSubVars
                if (trim(stringVal(2:)) == trim(this%subVars(iSV)%name)) then
                    stringVal = trim(this%subVars(iSV)%sval)
                    foundSV   = .true.
                    exit findSubVar
                end if
            end do findSubVar
            
            if (.not. foundSV) then
                call loeErr__print(err, 'Undefined substitution variable ' //quo// trim(stringVal) //quo)
                return
            end if
        end if
        
        
        loeFile__readStr = .true.
    
    END FUNCTION loeFile__readStr
    
    
    !***************************************************************************
    ! Read real value
    !***************************************************************************   
    logical FUNCTION loeFile__readReal(this, rVal, ident)
        implicit none
        
        type(loeFile),   intent(inout)  :: this  ! loeFile structure
        real(kind=prec), intent(out)    :: rVal  ! Real value to be read
        character(*),    intent(in)     :: ident ! identifier of value
        
        character(MAX_STRINGVAL_LENGTH) :: strVal ! String value read from assignment
        
        loeFile__readReal = .false.
        
        
        if (.not. loeFile__readStr(this, strVal, ident)) return
        if (.not. str2Num(strVal,rVal))                  return
        
        loeFile__readReal = .true.
    END FUNCTION loeFile__readReal
    
    
    !***************************************************************************
    ! Read integer value
    !***************************************************************************   
    logical FUNCTION loeFile__readInt(this, iVal, ident)
        implicit none
        
        type(loeFile), intent(inout)    :: this   ! loeFile structure
        integer,       intent(out)      :: iVal   ! Integer value to be read
        character(*),  intent(in)       :: ident  ! identifier of value
        character(MAX_STRINGVAL_LENGTH) :: strVal ! String value read from assignment
        
        loeFile__readInt = .false.
        
        
        if (.not. loeFile__readStr(this, strVal, ident)) return
        if (.not. str2Num(strVal,iVal))                  return
        
        loeFile__readInt = .true.
    END FUNCTION loeFile__readInt
    
    
    !***************************************************************************
    ! Read current ID of element in list and check validity
    !***************************************************************************
    logical FUNCTION loeFile__readSectionStart(this, ident)
        implicit none
        
        type(loeFile), intent(inout)     :: this     ! loeFile structure
        character(*),  intent(in)        :: ident    ! Name of ID of elements in list (e.g. 'iSpring')
        character(MAX_STRINGVAL_LENGTH)  :: strVal   ! String read from file
        integer                          :: strLen   ! Length of string excluding trailing blanks
        type(loeErr)                     :: err      ! Error handling class
        
        character(1024) :: strExpected
        
        err                       = loeErr__init('loeFile__readSectionStart',modName)
        loeFile__readSectionStart = .false.
        
        strExpected= ' Expected' // quo// '[' // ident // ']' //quo// endl // &
                     ' Section headers need to be given in the form: ' // endl // &
                     ' ' //quo// '[section]' //quo// '.'
        
        ! Read value and do basic checking
        if (.not. loeFile__readLn(this)) then
            call loeErr__print( err, 'Cannot read section header.' // endl // trim(strExpected))
            return
        end if
        
        strVal = this%lStr
        strLen = len_trim(strVal)
        
        ! Check format and value
        if (strVal(1:1) /= '[' .or. strVal(strLen:strLen) /= ']') then
            call loeErr__print( err, 'Cannot read section header.' // endl // trim(strExpected))
            return
        end if
        
        if (trim(strVal(2:strLen-1)) /= trim(ident)) then
            call loeErr__print( err, 'Illegal section identifier.' // endl // trim(strExpected))
            return
        end if
        
        loeFile__readSectionStart = .true.
        
    END FUNCTION loeFile__readSectionStart
    
    
    !***************************************************************************
    ! Read number of elements in element list and check validity
    !***************************************************************************
    logical FUNCTION loeFile__readNEl(this, nElems, nElemsMin, ident)
        implicit none
        
        type(loeFile), intent(inout) :: this      ! loeFile structure
        integer,       intent(out)   :: nElems    ! Number of elements in list to be read
        integer,       intent(in)    :: nElemsMin ! Minimum number of elements in list
        character(*),  intent(in)    :: ident     ! Name of number of elements in list (e.g. 'nSprings')
        type(loeErr)                 :: err       ! Error handler
        
        err              = loeErr__init('loeFile__readNEl',modName)
        loeFile__readNEl = .false.
        
        
        if (.not. loeFile__readVal(this, nElems, trim(ident))) return
        
        if (nElems<nElemsMin) then
            call loeErr__print(err, 'Illegal number of elements in list ' // ident // ' = ' // trim(num2Str(nElems)) // endl // &
                                    ' ' // ident // ' must be >= ' // trim(num2Str(nElemsMin)))
            return
        end if
        
        loeFile__readNEl = .true.
    END FUNCTION loeFile__readNEl
    
    
    !***************************************************************************
    ! Read current ID of element in list and check validity
    !***************************************************************************
    logical FUNCTION loeFile__readEID(this, IDElCurr, IDElExpd, ident)
        implicit none
        
        type(loeFile), intent(inout)     :: this     ! loeFile structure
        integer,       intent(out)       :: IDElCurr ! ID of current element in list (specified in input file)
        integer,       intent(in)        :: IDElExpd ! Expected ID of current element in list
        character(*),  intent(in)        :: ident    ! Name of ID of elements in list (e.g. 'iSpring')
        character(MAX_STRINGVAL_LENGTH)  :: strVal   ! String read from file
        integer                          :: strLen   ! Length of string excluding trailing blanks
        character(MAX_STRINGVAL_LENGTH)  :: entity   ! Entity read from file
        integer                          :: posSep   ! Position of '__' separator in string
        type(loeErr)                     :: err      ! Error handling class
        
        character(1024) :: strExpected
        
        err              = loeErr__init('loeFile__readEID',modName)
        loeFile__readEID = .false.
        
        strExpected= ' Expected' // quo// '[' // ident // '__' // trim(num2Str(IDElExpd)) // ']' //quo// endl // &
                     ' List item headers need to be given in the form: ' // endl // &
                     ' ' //quo// '[' // 'entity__xx' // ']' //quo// ' where ' &
                     //quo// 'entity' //quo// ' is the name of the ' // &
                     'list and the integer ' //quo// 'xx' //quo// ' is the item ID in the list.'
        
        ! Read value and do basic checking
        if (.not. loeFile__readLn(this)) return
        strVal = this%lStr
        strLen = len_trim(strVal)
        
        
        ! Check format and extract id
        if (strVal(1:1) /= '[' .or. strVal(strLen:strLen) /= ']') then
            call loeErr__print( err, 'Cannot read list item header.' // endl // trim(strExpected))
            return
        end if
        
        posSep = index(strVal,'__')
        if (posSep==0) then
            call loeErr__print( err, 'Cannot read list item header.' // endl // trim(strExpected))
            return
        end if
        
        if (.not. str2Num(strVal(posSep+2:strLen-1),IDElCurr)) then
            call loeErr__print( err, 'Cannot read ID in list item header.' // endl // trim(strExpected))
            return
        end if
        
        entity = strVal(2:posSep-1)
        
        if (trim(entity) /= trim(ident)) then
            call loeErr__print( err, 'Illegal list specifier.' // endl // trim(strExpected))
            return
        end if
        
        if (IDElCurr/= IDElExpd) then
            call loeErr__print( err, 'Illegal list element.' // endl // trim(strExpected))
            return
        end if
        
        loeFile__readEID = .true.
        
    END FUNCTION loeFile__readEID
    
    
    !***************************************************************************
    ! Read numbered entity in the for ident = entity__xx
    !***************************************************************************
    logical FUNCTION loeFile__readNumberedEntity(this, entity, entityID, ident)
        implicit none
        
        type(loeFile),                   intent(inout) :: this     ! loeFile structure
        character(MAX_STRINGVAL_LENGTH), intent(out)   :: entity   ! Name of entity read from file
        integer,                         intent(out)   :: entityID ! Integer value to be read
        character(*),                    intent(in)    :: ident    ! Expected identifier of value
        character(MAX_STRINGVAL_LENGTH)                :: strVal   ! String read from file
        integer                                        :: posSep   ! Position of '__' separator in string
        type(loeErr)                                   :: err      ! Error handler
        
        err = loeErr__init('loeFile__readNumberedEntity',modName)
        loeFile__readNumberedEntity = .false.
        
        
        if (.not. loeFile__readStr(this, strVal, ident)) return
        
        posSep = index(strVal,'__')
        if (posSep==0) then
            call loeErr__print( err, 'Cannot read numbered entity in assignment ' // &
                                     quo// ident // ' = ...' //quo// endl // &
                                     ' Numbered entities need to be given in the form: ' // endl // &
                                     ' ' //quo// 'entity__x' //quo// ' where ' //quo// 'entity' //quo// ' is the name of the ' // &
                                     'entity and the integer ' //quo// 'x' //quo// ' is its id.' )
            return
        end if
        
        if (.not. str2Num(strVal(posSep+2:),entityID)) then
            call loeErr__print( err, 'Cannot read numbered entity in assignment ' // &
                                     quo// ident // ' = ...' //quo// endl // &
                                     ' Numbered entities need to be given in the form: ' // endl // &
                                     ' ' //quo// 'entity__x' //quo// ' where ' //quo// 'entity' //quo// ' is the name of the ' // &
                                     'entity and the integer ' //quo// 'x' //quo// ' is its id.' )
            return
        end if
        
        entity = strVal(:posSep-1)
        
        loeFile__readNumberedEntity = .true.
    END FUNCTION
    
    
    !***************************************************************************
    ! Read vector
    !***************************************************************************
    logical FUNCTION loeFile__readVector(this,vector,nElems,ident)
        ! TODO error handling. try to enter vector without identifier and see
        ! what you get. we should get illegal assignment op. error, instead
        ! we get cannot read vector "xxx".
        implicit none
        
        type(loeFile),  intent(inout) :: this           ! loeFile structure
        real(prec),     intent(inout) :: vector(nElems) ! Vector to be read. Size is set automatically
        integer,        intent(in)    :: nElems         ! Length of vector to be read
        character(*),   intent(in)    :: ident          ! Expected identifier string
        integer                       :: ioErr          ! I/O status
        type(loeErr)                  :: err            ! Error handling class
        character(64)                 :: identRead      ! Actually read identifier string
        character(8)                  :: assOp          ! Actually read assignment operator
        
        err                 = loeErr__init('loeFile__readVector',modName)
        loeFile__readVector = .false.
        identRead           = ''
        
        if (.not. loeFile__readLn(this)) return
        
        read (this%lStr,*, iostat = ioErr) identRead, assOp, vector
        
        if (ioErr == 0) then
            if (trim(assOp) /= '=') then
                call loeErr__print(err, 'Illegal assignment operator: ' //quo// trim(assOp) //quo// endl // &
                                        ' Please use a ''='' sign separated by whitespaces')
                return
            end if
            
            if (trim(identRead) /= ident) then
                call loeErr__print(err, 'Illegal identifier: ' //quo// trim(identRead) //quo // endl // &
                                        ' Expected: ' //quo// ident //quo// endl // ' ' // errStr_Identifier)
                return
            end if
        
        else
            if (identRead /= '' .and. trim(identRead) /= ident) then
                call loeErr__print(err, 'Illegal identifier: ' //quo// trim(identRead) //quo // endl // &
                                        ' Expected: ' //quo// ident //quo// endl // ' ' // errStr_Identifier)
                return
            end if
            
            call loeErr__print( err, 'Cannot read Vector.' // endl //&
                                ' Expected vector: ' // quo // ident // quo // endl // &
                                ' Expected length: ' // trim(num2Str(nElems)) // endl // &
                                ' ' // errStr_FloatingPoint // ' Error code: ' // num2str(ioErr))
            return
        end if
        
        loeFile__readVector = .true.
    END FUNCTION
    
    
    !***************************************************************************
    ! Read matrix (real)
    !***************************************************************************
    logical FUNCTION loeFile__readMatrix_Real(this, matrix)
        implicit none
        
        type(loeFile),           intent(inout) :: this          ! loeFile structure
        real(prec), allocatable, intent(inout) :: matrix(:,:)   ! Matrix to be read. Size is set automatically
        integer                                :: ioErr         ! IO operations status flag
        integer                                :: nRows, nCols  ! Numbers  of rows and cols to be read
        integer                                :: iRow          ! Row currently being read
        type(loeErr)                           :: err           ! Error handling class
        
        err                      = loeErr__init('loeFile__readMatrix',modName)
        loeFile__readMatrix_Real = .false.
        
        
        if (.not. loeFile__readVal(this, nRows, 'nRows')) return
        if (.not. loeFile__readVal(this, nCols, 'nCols')) return
        
        if (allocated(matrix)) deallocate(matrix)
        allocate(matrix(nRows,nCols))
        
        do iRow = 1,nRows
            if (.not. loeFile__readLn(this)) return
            
            read (this%lStr,*, iostat = ioErr) matrix(iRow,:)
            if (ioErr /= 0) then
                call loeErr__print(err, &
                    'Cannot read floating point value in row number ' // num2Str(iRow) // endl // ' ' // &
                    errStr_FloatingPoint // ' Error code: ' // num2str(ioErr))
                return
            end if
        end do
        
        loeFile__readMatrix_Real = .true.
    END FUNCTION
    
    
    !***************************************************************************
    ! Read matrix (integer)
    !***************************************************************************
    logical FUNCTION loeFile__readMatrix_Int(this, matrix)
        implicit none
        
        type(loeFile),        intent(inout) :: this          ! loeFile structure
        integer, allocatable, intent(inout) :: matrix(:,:)   ! Matrix to be read. Size is set automatically
        integer                             :: ioErr         ! IO operations status flag
        integer                             :: nRows, nCols  ! Numbers  of rows and cols to be read
        integer                             :: iRow          ! Row currently being read
        type(loeErr)                        :: err           ! Error handling class
        
        err                     = loeErr__init('loeFile__readMatrix',modName)
        loeFile__readMatrix_Int = .false.
        
        
        if (.not. loeFile__readVal(this, nRows, 'nRows')) return
        if (.not. loeFile__readVal(this, nCols, 'nCols')) return
        
        if (allocated(matrix)) deallocate(matrix)
        allocate(matrix(nRows,nCols))
        
        do iRow = 1,nRows
            if (.not. loeFile__readLn(this)) return
            
            read (this%lStr,*, iostat = ioErr) matrix(iRow,:)
            if (ioErr /= 0) then
                call loeErr__print(err, &
                    'Cannot read floating point value in row number ' // num2Str(iRow) // endl // ' ' // &
                    errStr_FloatingPoint // ' Error code: ' // num2str(ioErr))
                return
            end if
        end do
        
        loeFile__readMatrix_Int = .true.
    END FUNCTION
    
    
    !***************************************************************************
    ! Load matrix from file (real)
    !***************************************************************************
    logical FUNCTION loeFile__loadMatrixFromFile_Real(matrix, fName)
        implicit none

        real(prec), allocatable, intent(inout) :: matrix(:,:) ! Matrix to be read. Size is set by function
        character(*),            intent(in)    :: fName       ! Name of file containing the matrix
        type(loeFile)                          :: inFile      ! Input file
        type(loeErr)                           :: err         ! Error handling class
        
        err                              = loeErr__init('loeFile__loadMatrixFromFile',modName)
        loeFile__loadMatrixFromFile_Real = .false.
        
        
        if (.not. loeFile__open(inFile,trim(fName))) then
            call loeErr__print(err, 'Cannot open file ' // fName)
            loeFile__loadMatrixFromFile_Real = .false.
            return
        end if
        
        if (.not. loeFile__readMatrix(inFile,matrix)) then
            call loeErr__print(err, 'Cannot read matrix from file ' // fName)
            call loeErr__print(err, 'Error in input file ' // fName // ' in line #' //&
                num2str(inFile%lineNum) // ':' // endl // ' ' // quo // trim(inFile%lStr) // quo)
            loeFile__loadMatrixFromFile_Real = .false.
            return
        end if
        
        close(inFile%iUnit)
        loeFile__loadMatrixFromFile_Real = .true.
        
    END FUNCTION loeFile__loadMatrixFromFile_Real
    
    
    !***************************************************************************
    ! Load matrix from file (integer)
    !***************************************************************************
    logical FUNCTION loeFile__loadMatrixFromFile_Int(matrix, fName)
        implicit none

        integer, allocatable, intent(inout) :: matrix(:,:) ! Matrix to be read. Size is set by function
        character(*),         intent(in)    :: fName       ! Name of file containing the matrix
        type(loeFile)                       :: inFile      ! Input file
        type(loeErr)                        :: err         ! Error handling class
        
        err                              = loeErr__init('loeFile__loadMatrixFromFile',modName)
        loeFile__loadMatrixFromFile_Int = .false.
        
        
        if (.not. loeFile__open(inFile,trim(fName))) then
            call loeErr__print(err, 'Cannot open file ' // fName)
            loeFile__loadMatrixFromFile_Int = .false.
            return
        end if
        
        if (.not. loeFile__readMatrix(inFile,matrix)) then
            call loeErr__print(err, 'Cannot read matrix from file ' // fName)
            call loeErr__print(err, 'Error in input file ' // fName // ' in line #' //&
                num2str(inFile%lineNum) // ':' // endl // ' ' // quo // trim(inFile%lStr) // quo)
            loeFile__loadMatrixFromFile_Int = .false.
            return
        end if
        
        close(inFile%iUnit)
        loeFile__loadMatrixFromFile_Int = .true.
        
    END FUNCTION loeFile__loadMatrixFromFile_Int

END MODULE liboe__File
