!*******************************************************************************
!   OCN-SIM Flex
!   Copyright (C) 2017 by Christoph Otto
!   christoph@ocnacademy.org
!   www.ocnacademy.org
!
!
!   MODULE :: Multibody model data
!
!   PURPOSE
!   Custom data type for the structural dynamic elements. Contains routines
!   to load data from file, check consistency and assemble the single elements.
!
!   
!     ___   ____ _   _    ____ ___ __  __    _____ _     _______  __
!    / _ \ / ___| \ | |  / ___|_ _|  \/  |  |  ___| |   | ____\ \/ /
!   | | | | |   |  \| |  \___ \| || |\/| |  | |_  | |   |  _|  \  / 
!   | |_| | |___| |\  |   ___) | || |  | |  |  _| | |___| |___ /  \
!    \___/ \____|_| \_|  |____/___|_|  |_|  |_|   |_____|_____/_/\_\
!
!
!   LICENSE
!   This file is part of OCN-SIM Flex.
!
!   OCN-SIM Flex is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   OCN-SIM Flex is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with OCN-SIM Flex. If not, see <http://www.gnu.org/licenses/>.
!
!*******************************************************************************

MODULE mbs__Data
    
    USE liboe__Conf
    USE liboe__Tools
    USE liboe__Types
    USE liboe__File
    USE liboe__Err
    USE mbs__Types
    USE flm__Types
    
    implicit none
    
    
    !***************************************************************************
    ! Module description                                                       !
    !***************************************************************************
    character(*), parameter, private :: modName = 'mbs__Data'
    
    
    !***************************************************************************
    ! Public entities                                                          !
    !***************************************************************************
    PUBLIC :: mbsData
    PUBLIC :: mbsData__importRaw
    PUBLIC :: mbsData__mergeSecConstraints
    PUBLIC :: mbsData__cleanup
    
    
PRIVATE
    
    !***************************************************************************
    ! Multibody-system data type                                               !
    !***************************************************************************
    TYPE :: mbsData
        
        ! CSYS0_________________________________________________________________
        ! ______________________________________________________________________
        integer                 :: nMaCS      ! Number of markers in CSYS0
        real(prec), allocatable :: rMaCS(:,:) ! Positions of markers in CSYS0. Dimensions: 3 x nMaCS
        
        
        ! Flexible body data____________________________________________________
        ! ______________________________________________________________________
        integer                    :: nNodes ! Total number of nodes in the system
        real(prec),    allocatable :: vFB(:) ! Volumes of nodes. Amounts to half the volume of the bar element. Dimension(nNodes)
        real(prec),    allocatable :: mFB(:) ! Masses of nodes. Amounts to half the mass of the bar element. Dimension(nNodes)
        
        
        ! Rigid body data_______________________________________________________
        ! ______________________________________________________________________
        integer                              :: nRigids    ! Number of rigid bodies
        integer                              :: nMarkersRB ! Total number of markers on rigid bodies
        type(structElem__Rigid), allocatable :: rigids(:)  ! Rigid body elements
        
        
        ! Constraints___________________________________________________________
        ! ______________________________________________________________________
        integer                                :: nConstr      ! Number of primary constraints in system
        type(sysElem__Constr),     allocatable :: constr(:)    ! Primary constraints in implicit form. Dimension(nConstr)
        
        integer                                :: nConstrSec   ! Number of loop closing secondary constraints in system
        type(sysElem__ConstrSec), allocatable  :: constrSec(:) ! Loop closing secondary position constraints simulated by PID-controller
        
        
        ! Springs_______________________________________________________________
        ! ______________________________________________________________________
        integer                              :: nSprings   ! Number of springs in system
        type(forceElem__Spring), allocatable :: springs(:) ! Spring elements
        
        
        ! PID-elements__________________________________________________________
        ! ______________________________________________________________________
        integer                            :: nPIDs   ! Number of PID elements in system
        type(forceElem__PID), allocatable  :: pids(:) ! PID elements
        
        
        ! Hydrodynamic force elements___________________________________________
        ! ______________________________________________________________________
        type(forceElem__Bar),  allocatable :: fElems__bar(:) ! Bar-type force elements
        integer                            :: nBars
    END TYPE mbsData
    
 CONTAINS
    
    !***************************************************************************
    ! Import raw data
    !***************************************************************************
    LOGICAL FUNCTION mbsData__importRaw ( this, rMaFB, rMaFB_d, diam, rho, &
                                          fNameCS, fNameFB, fNameCO, fNameMVAdd )
        ! TODO: guess what?! Error handling!
        implicit none
        
        type(mbsData),                                intent(inout) :: this         ! Multibody data structure
        real(prec),                      allocatable, intent(out)   :: rMaFB(:,:)   ! Initial node positions
        real(prec),                      allocatable, intent(out)   :: rMaFB_d(:,:) ! Initial node velocities
        real(prec),                                   intent(in)    :: diam         ! Constant diameter for all elements
        real(prec),                                   intent(in)    :: rho          ! Constant density for all elements
        character(MAX_STRINGVAL_LENGTH),              intent(in)    :: fNameCS      ! File containing reference markers in CSYS0
        character(MAX_STRINGVAL_LENGTH),              intent(in)    :: fNameFB      ! File containing initial node positions
        character(MAX_STRINGVAL_LENGTH),              intent(in)    :: fNameCO      ! File containing constraint data
        character(MAX_STRINGVAL_LENGTH),              intent(in)    :: fNameMVAdd   ! File containing additional mass and volume
        
        real(prec), allocatable :: initPos(:,:)  ! Initial node positions
        real(prec), allocatable :: mvAdd(:,:)    ! Added masses and volumes. 1st col: mass, 2nd col: volume
        integer,    allocatable :: constrRd(:,:) ! Constraint reader. Used to read constraints in a simple integer matrix form.
                                                 ! Negative indices correspond to reference markers in CSYS0
        
        integer    :: iCo          ! Index of current constraint
        real(prec) :: r1(3), r2(3) ! Node positions for calculation of initial constraint length
        
        mbsData__importRaw = .false.
        
        
        ! --- Reference markers ---
        if (.not. loeFile__loadMatrixFromFile(initPos, fNameCS)) return
        this%nMaCS = size(initPos,1)
        
        allocate(this%rMaCS(3,this%nMaCS))
        this%rMaCS = transpose(initPos)
        deallocate(initPos)
        
        
        ! --- Initial node positions and velocities ---
        if (.not. loeFile__loadMatrixFromFile(initPos, fNameFB)) return
        this%nNodes = size(initPos,1)
        
        allocate (rMaFB(3, this%nNodes))
        rMaFB = transpose(initPos)
        deallocate(initPos)
        
        allocate (rMaFB_d(3, this%nNodes))
        rMaFB_d(:,:) = ZE
        
        
        ! --- Constraints ---
        if (.not. loeFile__loadMatrixFromFile(constrRd, fNameCO)) return
        
        this%nConstr = size(constrRd,1)
        allocate(this%constr(this%nConstr))
        
        do iCo = 1,this%nConstr
            if (constrRd(iCo,1) > 0) then
                this%constr(iCo)%k1   = constrRd(iCo,1)
                this%constr(iCo)%k1RT = MTYPE__FLEX
                
                r1 = rMaFB(:,this%constr(iCo)%k1)
            else if (constrRd(iCo,1) < 0) then
                this%constr(iCo)%k1   = -1*constrRd(iCo,1)
                this%constr(iCo)%k1RT = MTYPE__CSYS
            
                r1 = this%rMaCS(:,this%constr(iCo)%k1)
            else
                print *, 'Error in raw constraint file. Node k1 may not be 0.'
                return
            end if
            
            
            if (constrRd(iCo,2) > 0) then
                this%constr(iCO)%k2   = constrRd(iCo,2)
                this%constr(iCo)%k2RT = MTYPE__FLEX
                
                r2 = rMaFB(:,this%constr(iCo)%k2)
            else
                print *, 'Error in raw constraint file. Node k2 must always be > 0.'
                return
            end if
            
            this%constr(iCo)%length = sqrt(sum((r2-r1)**2))
        end do
        
        deallocate(constrRd)
        
        
        ! --- Force elements based on constraints ---
        ! All constraints are assumed to be bar elements with a homogeneous 
        ! diameter and density
        this%nBars = this%nConstr
        allocate(this%fElems__bar(this%nBars))
        
        this%fElems__bar(:)%k1    = this%constr(:)%k1
        this%fElems__bar(:)%k1RT  = this%constr(:)%k1RT
        
        this%fElems__bar(:)%k2    = this%constr(:)%k2
        this%fElems__bar(:)%k2RT  = this%constr(:)%k2RT
        
        this%fElems__bar(:)%d     = diam
        this%fElems__bar(:)%l     = this%constr(:)%length
        this%fElems__bar(:)%AProj = this%fElems__bar(:)%d*this%fElems__bar(:)%l
        
        this%fElems__bar(:)%IDCT = 1
        
        
        ! --- Mass and volume based on bar type force elements ---
        allocate (this%mFB(this%nNodes))
        allocate (this%vFB(this%nNodes))
        
        this%vFB(:) = ZE
        this%mFB(:) = ZE
        
        do iCo = 1,this%nBars
            associate (bar=>this%fElems__bar(iCo))
                
                if (bar%k1RT==MTYPE__FLEX) then
                    this%vFB(bar%k1) = this%vFB(bar%k1) + 0.125*PI*bar%d**2*bar%l
                    this%mFB(bar%k1) = this%mFB(bar%k1) + rho*this%vFB(bar%k1)
                end if
                
                this%vFB(bar%k2) = this%vFB(bar%k2) + 0.125*PI*bar%d**2*bar%l
                this%mFB(bar%k2) = this%mFB(bar%k2) + rho*this%vFB(bar%k2)
            end associate
        end do
        
        
        ! --- Mass and volume based on added values ---
        if (.not. loeFile__loadMatrixFromFile(mvAdd, fNameMVAdd)) return
        this%mFB = this%mFB + mvAdd(:,1)
        this%vFB = this%vFB + mvAdd(:,2)
        deallocate(mvAdd)
        
        
        ! --- Allocate unneeded dummies ---
        this%nConstrSec = 0
        this%nRigids    = 0
        this%nSprings   = 0
        this%nPIDs      = 0
        
        allocate(this%constrSec(this%nConstrSec))
        allocate(this%rigids(this%nRigids))
        allocate(this%springs(this%nSprings))
        allocate(this%pids(this%nPIDs))
        
        mbsData__importRaw = .true.
    END FUNCTION
    
    !***************************************************************************
    ! Merge nodes in secondary constraints
    !***************************************************************************    
    SUBROUTINE mbsData__mergeSecConstraints( this, rMaFB, rMaFB_d, map__full_to_Merged )
        implicit none
        
        type(mbsData),              intent(inout) :: this                   ! Multibody data structure
        real(prec),    allocatable, intent(inout) :: rMaFB(:,:)             ! Node positions
        real(prec),    allocatable, intent(inout) :: rMaFB_d(:,:)           ! Node velocities
        integer,       allocatable, intent(out)   :: map__full_to_Merged(:) ! Maps original node IDs to merged node IDs
        
        real(prec), allocatable :: rMaFBNew(:,:)
        real(prec), allocatable :: rMaFB_dNew(:,:)
        real(prec), allocatable :: mNew(:)
        real(prec), allocatable :: vNew(:)
        integer                 :: iNo
        integer                 :: iCo
        integer                 :: iCoSec
        
        
        allocate(rMaFBNew(3,this%nNodes-this%nConstrSec))
        allocate(rMaFB_dNew(3,this%nNodes-this%nConstrSec))
        allocate(mNew(this%nNodes-this%nConstrSec))
        allocate(vNew(this%nNodes-this%nConstrSec))
        allocate(map__full_to_Merged(this%nNodes))
        
        do iNo = 1,this%nNodes
            map__full_to_Merged(iNo) = iNo
        end do
        
        ! Adjust node IDs in constraints
        do iCoSec = 1,this%nConstrSec
            associate ( k1 => this%constrSec(iCoSec)%k1, &
                        k2 => this%constrSec(iCoSec)%k2)
            
                if (k2>k1) then
                    map__full_to_Merged(k1)  = map__full_to_Merged(k2)
                    map__full_to_Merged(k1:) = map__full_to_Merged(k1:)-1
                else
                    map__full_to_Merged(k2)  = map__full_to_Merged(k1)
                    map__full_to_Merged(k2:) = map__full_to_Merged(k2:)-1
                end if
            
            end associate
        end do
        
        ! Adapt node ids in constraints
        do iCo = 1,this%nConstr
            
            this%constr(iCo)%k1 = map__full_to_Merged(this%constr(iCo)%k1)
            this%constr(iCo)%k2 = map__full_to_Merged(this%constr(iCo)%k2)
        end do
        
        ! Copy coordinates and masses
        mNew(:) = ZE
        vNew(:) = ZE
        do iNo = 1,this%nNodes
            rMaFBNew  (:,map__full_to_Merged(iNo)) = rMaFB(:,iNo)
            rMaFB_dNew(:,map__full_to_Merged(iNo)) = rMaFB_d(:,iNo)
            
            mNew(map__full_to_Merged(iNo)) = mNew(map__full_to_Merged(iNo)) + &
                                             this%mFB(iNo)
                                             
            vNew(map__full_to_Merged(iNo)) = vNew(map__full_to_Merged(iNo)) + &
                                             this%vFB(iNo)
        end do
        
        this%nNodes     = this%nNodes - this%nConstrSec
        this%nConstrSec = 0
        
        deallocate(rMaFB, rMaFB_d)
        allocate  (rMaFB(3,this%nNodes), rMaFB_d(3,this%nNodes))
        rMaFB   = rMaFBNew
        rMaFB_d = rMaFB_dNew
        deallocate(rMaFBNew)
        deallocate(rMaFB_dNew)
        
        deallocate(this%mFB)
        allocate(this%mFB(this%nNodes))
        this%mFB = mNew
        deallocate(mNew)
        
        deallocate(this%vFB)
        allocate(this%vFB(this%nNodes))
        this%vFB = vNew
        deallocate(vNew)
        
        deallocate(this%constrSec)
        allocate(this%constrSec(this%nConstrSec))
        
    END SUBROUTINE
    
    
    !***************************************************************************
    ! Pseudo-destructor for mbsData
    !***************************************************************************
    SUBROUTINE mbsData__cleanup(this)
        implicit none
        
        type(mbsData), intent(inout) :: this
        
        integer :: iRi ! Index of current rigid body
        
        
        deallocate (this%vFB, this%mFB)
        deallocate (this%rMaCS, this%constrSec, this%springs, this%pids)
        deallocate (this%constr)        
        
        ! Deep deallocation of marker position matrices of rigid bodies
        do iRi = 1,this%nRigids
            deallocate(this%rigids(iRi)%markerPosns)
        end do
        
        ! Deallocate actual rigid body list
        deallocate(this%rigids)
        
    END SUBROUTINE mbsData__cleanup
    
END MODULE mbs__Data
