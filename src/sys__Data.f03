!*******************************************************************************
!   OCN-SIM Flex
!   Copyright (C) 2017 by Christoph Otto
!   christoph@ocnacademy.org
!   www.ocnacademy.org
!
!
!   MODULE :: System data
!
!   PURPOSE
!   Contains data type to describe combined system of flexible and rigid bodies
!   under the influence of hydrodynamic loads. Includes routines to load data
!   from file and consistency check.
!
!   
!     ___   ____ _   _    ____ ___ __  __    _____ _     _______  __
!    / _ \ / ___| \ | |  / ___|_ _|  \/  |  |  ___| |   | ____\ \/ /
!   | | | | |   |  \| |  \___ \| || |\/| |  | |_  | |   |  _|  \  / 
!   | |_| | |___| |\  |   ___) | || |  | |  |  _| | |___| |___ /  \
!    \___/ \____|_| \_|  |____/___|_|  |_|  |_|   |_____|_____/_/\_\
!
!
!   LICENSE
!   This file is part of OCN-SIM Flex.
!
!   OCN-SIM Flex is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   OCN-SIM Flex is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with OCN-SIM Flex. If not, see <http://www.gnu.org/licenses/>.
!
!*******************************************************************************

MODULE sys__Data
    
    USE osVar
    USE liboe__Conf
    USE liboe__Types
    USE liboe__Err
    USE liboe__Tools
    USE liboe__File
    USE mbs__Data
    USE mbs__Types
    USE rdr__RopeSys
    USE flm__Data
    USE sol__JAC
    
    implicit none
    
    !***************************************************************************
    ! Module description                                                       !
    !***************************************************************************
    character(*), parameter, private :: modName = 'sys__Data'
    

    !***************************************************************************
    ! Public entities                                                          !
    !***************************************************************************
    PUBLIC :: sysData__loadFromFile, sysData__checkConsistency
    PUBLIC :: sysData
    

PRIVATE
    
    !***************************************************************************
    ! System data type                                                         !
    !***************************************************************************
    TYPE :: sysData
                
        ! --- File name ---
        character(len=MAX_STRINGVAL_LENGTH) :: nameOfCase ! Name of the case. No trailing or leading slashes. Used e.g. for name of the output folder
        character(len=MAX_STRINGVAL_LENGTH) :: outDir     ! Output directory relative to current working directory. Includes trailing path separator character ('/' on linux, '\' on windows)
        
        ! --- Solver configuration ---
        integer         :: solType ! Type of solver
        integer         :: nSteps  ! Number of time-steps
        integer         :: nOut    ! Output every nTh time-step
        real(kind=prec) :: tEnd    ! Total integration time
        real(kind=prec) :: tStep   ! Size of timestep
        real(prec)      :: gVec(3) ! Gravity vector
        
        ! --- Water surface output ---
        logical    :: writeWaterSurface  ! Write water surface to output
        real(prec) :: waterSurfaceXMin   ! Starting point to write water surface
        real(prec) :: waterSurfaceXMax   ! End point to write water surface
        integer    :: waterSurfaceNDiv   ! Number of points where to write water surface
        
        ! --- PID-controlled constraints ---
        real(prec) :: constr_kp ! Linear coefficient
        real(prec) :: constr_ki ! Integral coefficient
        real(prec) :: constr_kd ! Derivative coefficient
        
        ! --- RRF-solver parameters ---
        real(prec) :: rrf_wtFac ! Weight factor for projections
        integer    :: rrf_nIter ! Number of iterations for projections
        
    END TYPE sysData
    
CONTAINS
    
    
    !***************************************************************************
    ! Load configuration from file                                             !
    !***************************************************************************
    logical FUNCTION sysData__loadFromFile( this, mbd, rMaFB, rMaFB_d, qRB, &
                                             q0, v0, constrExpl, nConstrExpl, fld, fName )
        ! Description
        !   Load system data, check data consistency, assemble system and
        !   initialise variables
        implicit none
        
        type(sysData),                          intent(out)   :: this         ! System data structure
        type(mbsData),                          intent(out)   :: mbd          ! Multibody data structure
        
        real(prec),                allocatable, intent(inout) :: rMaFB(:,:)   ! Dimension(3,nNodes)
        real(prec),                allocatable, intent(inout) :: rMaFB_d(:,:) ! Dimension(3,nNodes)
        
        real(prec),                allocatable, intent(inout) :: qRB(:,:)     ! Rigid body poses. Each pose is described by 3 translational coordinates plus 4 Euler parameters describing the body's rotation w.r.t. CSYS0. Dimension(7, mbd%nRigids).
        
        real(prec),                allocatable, intent(inout) :: q0(:)        ! Initial joint positions
        real(prec),                allocatable, intent(inout) :: v0(:)        ! Initial joint velocities
        
        type(sysElem__ConstrExpl), allocatable, intent(inout) :: constrExpl(:)
        integer,                                intent(out)   :: nConstrExpl
        
        type(flmData),                          intent(out)   :: fld     ! Fluid model data structure
        character(*),                           intent(in)    :: fName   ! Name of input file
        
        type(loeFile)                   :: inFile         ! Main input file
        logical                         :: allValuesRead  ! Status flag: .false. until all values are successfully read from file
        
        character(MAX_STRINGVAL_LENGTH) :: fNameFlm   ! Name of fluid model data file
        character(MAX_STRINGVAL_LENGTH) :: fNameMbd   ! Name of multibody model data file
        
        character(MAX_STRINGVAL_LENGTH) :: fNameCS    ! Raw multibody data import: File containing reference markers in CSYS0
        character(MAX_STRINGVAL_LENGTH) :: fNameFB    ! Raw multibody data import: File containing initial node positions
        character(MAX_STRINGVAL_LENGTH) :: fNameCO    ! Raw multibody data import: File containing constraint data
        character(MAX_STRINGVAL_LENGTH) :: fNameMVAdd ! Raw multibody data import: File containing constraint data
        
        integer                         :: boolInt        ! Dummy integer to read booleans
        
        type(solVars__JAC)              :: svj            ! Solver specific variables for non-recursive formulations. Needed for calculation of initial positions and velocities
        
        integer                         :: iRB            ! Counting index
        
        type(loeErr)                    :: err            ! Error handling class
        
        err = loeErr__init('sysData__loadFromRopesFile',modName)
        sysData__loadFromFile = .false.
        
        
        if (.not. loeFile__open(inFile,trim(fName))) return
        
        ! Read contents of file. Emulated try-finally block using do-loop. See
        ! programmer's guide.
        allValuesRead = .false.
        readVals: do
            ! --- Basic name of the load case ---
            if (.not. loeFile__readVal(inFile, this%nameOfCase, 'nameOfCase')) exit readVals
            this%outDir = '.' // os__pathSep // trim(this%nameOfCase) // '.out' // os__pathSep
            
            
            ! --- Solver configuration ----
            if (.not. loeFile__readVal(inFile, this%solType, 'solType')) exit readVals ! solType
            if (.not. loeFile__readVal(inFile, this%tEnd,    'tEnd' ))   exit readVals ! nSteps
            if (.not. loeFile__readVal(inFile, this%nOut,    'nOut'   )) exit readVals ! nOut
            if (.not. loeFile__readVal(inFile, this%tStep,   'tStep'  )) exit readVals ! tStep
            
            this%nSteps = nint(this%tEnd/this%tStep)
            
            
            ! --- Global configuration ----
            if (.not. loeFile__readVector(inFile,this%gVec, 3, 'g')) exit readVals ! g-vector
            
            
            ! --- Fluid model data ----
            if (.not. loeFile__readVal(inFile, fNameFlm, 'fluidModel')) exit readVals ! File name of fluid model data file
            if (.not. flmData__loadFromFile(fld,trim(fNameFlm)))        exit readVals ! Actual fluid model data from file
            
            if (.not. loeFile__readVal(inFile, boolInt, 'writeWaterSurface')) exit readvals
            
            if ( boolInt == 1 ) then
                this%writeWaterSurface = .true.
                
                if (.not. loeFile__readVal(inFile, this%waterSurfaceXMin, 'xMin')) exit readvals
                if (.not. loeFile__readVal(inFile, this%waterSurfaceXMax, 'xMax')) exit readvals
                if (.not. loeFile__readVal(inFile, this%waterSurfaceNDiv, 'nDiv')) exit readvals
            else
                this%writeWaterSurface = .false.
            end if
            
            
            ! --- Multibody system data ----
            if (.not. loeFile__readVal(inFile, boolInt, 'importRawData')) exit readvals
                
            if (boolInt /= 1) then
                if (.not. loeFile__readVal(inFile, fNameMbd, 'multibodyData'))    exit readVals ! File name of multibody system data file
                if (.not. rdrRopeSys__loadFromFile( mbd, q0, v0, constrExpl, &
                                                    nConstrExpl, trim(fNameMbd))) exit readVals ! Actual multibody data from file
                
                
                ! --- Flexible bodies Cartesian initial positions and velocities ---
                allocate (rMaFB(  3, mbd%nNodes))
                allocate (rMaFB_d(3, mbd%nNodes))
                
                call slvJAC__initSolVars( svj, mbd%mFB, mbd%nNodes, constrExpl, nConstrExpl, &
                                          q0, v0, mbd%nConstrSec )
                
                call slvJAC__updateSolVars( svj, rMaFB, rMaFB_d, mbd%nNodes, &
                                            mbd%rMaCS, mbd%nMaCS, &
                                            mbd%constrSec, mbd%nConstrSec, &
                                            ZE, ZE, ZE, ZE )
                
                call slvJAC__cleanupSolVars(svj)
            else
                fNameCS    = 'refPos.matr'
                fNameFB    = 'initialPos.matr'
                fNameCO    = 'constr.matr'
                fNameMVAdd = 'mvAdd.matr'
                
                if ( .not. mbsData__importRaw(mbd, rMaFB, rMaFB_d, 0.01_prec, 1500.0_prec, &
                     fNameCS, fNameFB, fNameCO, fNameMVAdd) ) exit readVals
                
                ! Allocate unneeded dummies
                nConstrExpl = 0
                allocate(constrExpl(nConstrExpl))
                allocate(q0(nConstrExpl))
                allocate(v0(nConstrExpl))
            end if
            
            
            ! --- Rigid bodies ---
            allocate(qRB(7,mbd%nRigids))
                        
            do iRB = 1,mbd%nRigids
                qRB(:,iRB)    = mbd%rigids(iRB)%rR0
            end do
            
            
            ! --- Parameters for secondary constraints ---
            if (.not. loeFile__readVal(inFile, this%constr_kp, 'constr_kp')) exit readVals ! Linear coefficient
            if (.not. loeFile__readVal(inFile, this%constr_ki, 'constr_ki')) exit readVals ! Linear coefficient
            if (.not. loeFile__readVal(inFile, this%constr_kd, 'constr_kd')) exit readVals ! Linear coefficient
            
            
            ! Parameters for RRF-type solvers
            if (.not. loeFile__readVal(inFile, this%rrf_wtFac, 'rrf_wtFac')) exit readVals ! Linear coefficient
            if (.not. loeFile__readVal(inFile, this%rrf_nIter, 'rrf_nIter')) exit readVals ! Linear coefficient
            
            
            allValuesRead = .true.
            exit readVals
        end do readVals
        
        
        call loeFile__close(inFile)
        if (.not. allValuesRead) then
            call loeErr__print(err, 'Error in input file ' // fName // ' in line #' // trim(num2Str(inFile%lineNum)) // ': ' // &
                                     endl // ' ' // quo // trim(inFile%lStr) // quo)
            return
        end if
        
        
        ! --- Consistency checks ---
        if (sysData__checkConsistency (this) > 0) then
            call loeErr__print(err, 'Terminating due to inconsistent system data')
            return
        end if

        if (flmData__checkConsistency(fld) > 0) then
            call loeErr__print(err, 'Terminating due to inconsistent fluid model data')
            return
        end if
        
        sysData__loadFromFile = .true.
    END FUNCTION sysData__loadFromFile
    
    
    !***************************************************************************
    ! Check multibody data consistency                                         !
    !***************************************************************************
    integer FUNCTION sysData__checkConsistency(this)
        ! Checks, whether all values are consistent.
        ! TODO: update continuously, as new data is introduced...
        implicit none

        type(sysData), intent(in) :: this ! System data structure
        
        type(loeErr) :: err   ! Error handling class
        type(datCHK) :: errCh ! Data error checking class
        
        err   = loeErr__init('sysData__checkConsistency',modName)
        errCh = datCHK__init('sysData__checkConsistency',modName)
        
        
        call datCHK__checkVal(errCh, 'solType', this%solType, '>=', 1         ) ! Solver type
        call datCHK__checkVal(errCh, 'solType', this%solType, '<=', SOL_MAX_T ) ! Solver type
        call datCHK__checkVal(errCh, 'nSteps',  this%nSteps,  '>=', 1         ) ! Number of timesteps
        call datCHK__checkVal(errCh, 'nOut',    this%nOut,    '>=', 1         ) ! Output every nOut-th timestep
        call datCHK__checkVal(errCh, 'tStep',   this%tStep,   '>',  ZE        ) ! Size of timestep
        
        call datCHK__checkVal(errCh, 'constr__kp', this%constr_kp, '>=', ZE) ! Linear coefficient of PID-controller
        call datCHK__checkVal(errCh, 'constr__ki', this%constr_ki, '>=', ZE) ! Linear coefficient of PID-controller
        call datCHK__checkVal(errCh, 'constr__kd', this%constr_kd, '>=', ZE) ! Linear coefficient of PID-controller
        
        call datCHK__checkVal(errCh, 'rrf_wtFac', this%rrf_wtFac, '>=', ZE) ! Linear coefficient of PID-controller
        call datCHK__checkVal(errCh, 'rrf_nIter', this%rrf_nIter, '>=', 0) ! Linear coefficient of PID-controller
        
        
        if (errCh%nErr>0) &
            call loeErr__print(err, 'System data check finished with a total number of ' // &
                                    trim(num2Str(errCh%nErr)) // ' errors.')
        
        sysData__checkConsistency = errCh%nErr
    END FUNCTION sysData__checkConsistency
    
    
END MODULE sys__Data
