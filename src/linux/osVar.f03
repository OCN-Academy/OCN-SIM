MODULE osVar
    
    character(*),parameter :: os__pathSep    = '/'
    
    ! Os-specific commands. Include trailing blank character.
    character(*),parameter :: os__del    = 'rm '
    character(*),parameter :: os__delDir = 'rm -r '
    character(*),parameter :: os__mkDir  = 'mkdir '
    
    
END MODULE osVar
