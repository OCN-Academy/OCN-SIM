!*******************************************************************************
!   OCN-SIM Flex
!   Copyright (C) 2017 by Christoph Otto
!   christoph@ocnacademy.org
!   www.ocnacademy.org
!
!
!   MODULE :: RRF-type solvers
!
!   PURPOSE
!   Reconstructed reaction forces (RRF) based solvers
!
!   
!     ___   ____ _   _    ____ ___ __  __    _____ _     _______  __
!    / _ \ / ___| \ | |  / ___|_ _|  \/  |  |  ___| |   | ____\ \/ /
!   | | | | |   |  \| |  \___ \| || |\/| |  | |_  | |   |  _|  \  / 
!   | |_| | |___| |\  |   ___) | || |  | |  |  _| | |___| |___ /  \
!    \___/ \____|_| \_|  |____/___|_|  |_|  |_|   |_____|_____/_/\_\
!
!
!   LICENSE
!   This file is part of OCN-SIM Flex.
!
!   OCN-SIM Flex is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   OCN-SIM Flex is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with OCN-SIM Flex. If not, see <http://www.gnu.org/licenses/>.
!
!*******************************************************************************

MODULE sol__RRF
    
    USE liboe__Conf
    USE liboe__Types
    USE liboe__Tools
    USE mbs__Types
    
    implicit none
    
    PUBLIC :: solRRF__calcProjectorStep, solRRF__calcCorrectorStep
    PUBLIC :: solRRF__satisfyConstraints
    
PRIVATE


CONTAINS


    !***************************************************************************
    ! Perform projector step
    !***************************************************************************
    PURE SUBROUTINE solRRF__calcProjectorStep( rMaFB, rMaFB_d, rMaFB_dd, rMaFB0, &
                                               nNodes, rMaCS, nMaCS, &
                                               fEFB, mFBInv, mFB, constr, nConstr, &
                                               constrSec, nConstrSec, wtFac, nIter, tStep )
        implicit none
        
        real(prec),               intent(inout) :: rMaFB(3,nNodes)    ! Marker accelerations: Flexible body nodes. Dimension(3,nNodes)
        real(prec),               intent(inout) :: rMaFB_d(3,nNodes)  ! Marker velocities: Flexible body nodes. Dimension(3,nNodes)
        real(prec),               intent(inout) :: rMaFB_dd(3,nNodes) ! Marker velocities: Flexible body nodes. Dimension(3,nNodes)
        
        real(prec),               intent(in)    :: rMaFB0(3,nNodes)   ! Marker accelerations: Flexible body nodes. Dimension(3,nNodes)
        
        integer,                  intent(in)    :: nNodes         ! Total number of nodes
        
        real(prec),               intent(in)    :: rMaCS(3,nMaCS) ! Marker positions: Reference coordinate systems
        integer,                  intent(in)    :: nMaCS          ! Number of markers in reference coordinate systems
        
        real(prec),               intent(in)    :: fEFB(3,nNodes) ! Flexible body external forces
        real(prec),               intent(in)    :: mFBInv(nNodes) ! Vector of reciprocal element masses. Dimension(nNodes)

        real(prec),               intent(in)    :: mFB(nNodes)
        
        type(sysElem__Constr),    intent(in)    :: constr(nConstr)
        integer,                  intent(in)    :: nConstr
        
        type(sysElem__ConstrSec), intent(in)    :: constrSec(nConstrSec)
        integer,                  intent(in)    :: nConstrSec
        
        real(prec),               intent(in)    :: wtFac    ! weight factor for projection
        integer,                  intent(in)    :: nIter    ! Number of iterations
        
        real(prec),               intent(in)    :: tStep          ! Size of timestep
        
        
        rMaFB_dd = matmulDiagRight(fEFB, mFBInv, 3, nNodes)        
        rMaFB    = (0.5_prec*tStep**2)*rMaFB_dd + tStep*rMaFB_d + rMaFB0
        
        call solRRF__satisfyConstraints( rMaFB, nNodes, rMaCS, nMaCS, mFB, &
                                         constr, nConstr, constrSec, nConstrSec, &
                                         wtFac, nIter )
        
        rMaFB_d  = (rMaFB-rMaFB0)*(1.0_prec/tStep)
        
    END SUBROUTINE solRRF__calcProjectorStep
    
    
    !***************************************************************************
    ! Perform corrector step
    !***************************************************************************
    SUBROUTINE solRRF__calcCorrectorStep( rMaFB, rMaFB_d, rMaFB_dd, rMaFB0, rMaFB_d0, &
                                          nNodes, rMaCS, nMaCS, fEDiff, mFBInv, mFB, &
                                          constr, nConstr, constrSec, nConstrSec, &
                                          wtFac, nIter, tStep )
        implicit none
        
        
        real(prec),               intent(inout) :: rMaFB(3,nNodes)    ! Marker accelerations: Flexible body nodes
        real(prec),               intent(inout) :: rMaFB_d(3,nNodes)  ! Marker velocities: Flexible body nodes
        real(prec),               intent(inout) :: rMaFB_dd(3,nNodes) ! Marker velocities: Flexible body nodes
        
        real(prec),               intent(inout) :: rMaFB0(3,nNodes)   ! Marker accelerations: Flexible body nodes
        real(prec),               intent(inout) :: rMaFB_d0(3,nNodes) ! Marker velocities at beginning of timestep: Flexible body nodes
        
        integer,                  intent(in)    :: nNodes             ! Total number of nodes
        
        real(prec),               intent(in)    :: rMaCS(3,nMaCS)     ! Marker positions: Reference coordinate systems
        integer,                  intent(in)    :: nMaCS              ! Number of markers in reference coordinate systems
        
        real(prec),               intent(in)    :: fEDiff(3,nNodes)   ! Difference of flexible body external forces at beginning and end of timestep fE-fE0
        real(prec),               intent(in)    :: mFBInv(nNodes)     ! Vector of reciprocal element masses. Dimension(nNodes)
        
        real(prec),               intent(in)    :: mFB(nNodes)
        
        type(sysElem__Constr),    intent(in)    :: constr(nConstr)
        integer,                  intent(in)    :: nConstr
        
        type(sysElem__ConstrSec), intent(in)    :: constrSec(nConstrSec)
        integer,                  intent(in)    :: nConstrSec
        
        real(prec),               intent(in)    :: wtFac    ! weight factor for projection
        integer,                  intent(in)    :: nIter    ! Number of iterations
        
        real(prec),               intent(in)    :: tStep    ! Size of timestep
        
        
        rMaFB_dd = matmulDiagRight(fEDiff, mFBInv, 3, nNodes)
        rMaFB    = (0.25_prec*tStep**2)*rMaFB_dd + rMaFB
        
        call solRRF__satisfyConstraints( rMaFB, nNodes, rMaCS, nMaCS, mFB, &
                                         constr, nConstr, constrSec, nConstrSec, &
                                         wtFac, nIter )
        
        ! TODO: why is the first solution not stable?
!        rMaFB_dd = (2.0_prec/(tStep**2))*(rMaFB - rMaFB0 - rMaFB_d0*tStep)
        rMaFB_d  = (rMaFB-rMaFB0)*(1.0/tStep)
        rMaFB_dd = (rMaFB_d-rMaFB_d0)*(1.0/tStep)
        
    END SUBROUTINE
    
    
    !***************************************************************************
    ! Project constraints iteratively
    !***************************************************************************
    PURE SUBROUTINE solRRF__satisfyConstraints( rN, nNodes, rCS, nMaCS, mFB, &
                                                constr, nConstr, constrSec, nConstrSec, &
                                                wtFac, nIter )
        implicit none
        
        real(prec),               intent(inout) :: rN(3,nNodes)    ! Vector of node positions. Dimension(3,nNodes)
        integer,                  intent(in)    :: nNodes
        
        real(prec),               intent(in)    :: rCS(3,nMaCS)   ! Vector of reference coordinate system marker positions. Dimension(3,nMarkersCSYS0)
        integer,                  intent(in)    :: nMaCS
        
        real(prec),               intent(in)    :: mFB(nNodes)
        
        type(sysElem__Constr),    intent(in)    :: constr(nConstr)
        integer,                  intent(in)    :: nConstr
        
        type(sysElem__ConstrSec), intent(in)    :: constrSec(nConstrSec)
        integer,                  intent(in)    :: nConstrSec
        
        real(prec),               intent(in)    :: wtFac    ! weight factor for projection
        integer,                  intent(in)    :: nIter    ! Number of iterations
        
        
        real(prec) :: constr_vec(3) ! Vectors k1->k2
        real(prec) :: constr_l      ! actual distance between nodes
        real(prec) :: lDiffRel      ! relative violation of the constraint length
        real(prec) :: proj_vec(3)   ! projection vector
        real(prec) :: m12           ! Combined mass of both nodes involved in constraint
        integer    :: iCo           ! ID of current constraint
        integer    :: iCoSec        ! ID of current secondary constraint
        integer    :: iIter         ! Current iteration
        
        
        do iIter = 1,nIter
            ! --- Explicit constraints ---
            do iCo = 1,nConstr
                associate(constr=>constr(iCo))
                    if (constr%k1RT == MTYPE__FLEX) then
                        constr_vec = rN(:,constr%k2) - rN(:,constr%k1)
                        constr_l   = sqrt(sum(constr_vec**2))
                        lDiffRel   = (constr_l - constr%length)/constr_l
                        proj_vec   = wtFac*lDiffRel*constr_vec
                        m12        = mFB(constr%k2) + mFB(constr%k1)

                        rN(:,constr%k1)  = rN(:,constr%k1) + &
                                                     (mFB(constr%k2)/m12)*proj_vec
                                                      
                        rN(:,constr%k2)  = rN(:,constr%k2) - &
                                                     (mFB(constr%k1)/m12)*proj_vec
                    else
                        constr_vec = rN(:,constr%k2) - rCS(:,constr%k1)
                        constr_l   = sqrt(sum(constr_vec**2))
                        lDiffRel   = (constr_l - constr%length)/constr_l
                        proj_vec   = wtFac*lDiffRel*constr_vec
                        
                        rN(:,constr%k2)  = rN(:,constr%k2) - proj_vec
                    end if
                end associate
            end do


            ! --- Implicit constraints ---
            do iCoSec = 1,nConstrSec
                associate ( iN1 => constrSec(iCoSec)%k1, &
                            iN2 => constrSec(iCoSec)%k2)
                    constr_vec   = rN(:,iN2) - rN(:,iN1)
                    proj_vec     = wtFac*constr_vec
                    m12          = mFB(iN1) + mFB(iN2)
                    
                    rN(:,iN1)    = rN(:,iN1) + (mFB(iN2)/m12)*proj_vec
                    rN(:,iN2)    = rN(:,iN2) - (mFB(iN1)/m12)*proj_vec
                end associate
            end do
        end do

    END SUBROUTINE solRRF__satisfyConstraints

END MODULE 
