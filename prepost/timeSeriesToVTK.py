#!/usr/bin/env python

# Need these in either case
import numpy as np
import sys
import vtkExporter as ve
import rigidBody as rb

# Only needed if no input file is specified
import tkFileDialog
import Tkinter
import os


# Configuration
# Offset in node IDs in constr file
offsetIDs = -1


# Select loadcase basefile and extract directories
if len(sys.argv)<2:
    # Show file dialog
    root = Tkinter.Tk()
    root.withdraw()
    commonFile = tkFileDialog.askopenfilename( 
        title='Select \'common\' file of loadcase output',
        filetypes = [('Loadcase common property files','common')])
    if not commonFile:
        print('No proper input file selected. Terminating script')
        quit()
else:
    commonFile = sys.argv[1]

caseDir = os.path.dirname(commonFile)
vtkDir  = os.path.join(caseDir,'vtk')


# Common property file
with open(commonFile,'r') as f:
    # Number of timesteps, total number of markers and number of markers in CSYS0
    nSteps          = int(f.readline().split()[1])
    nMarkers        = int(f.readline().split()[1])
    CSYS0__nMarkers = int(f.readline().split()[1])
    waterSurfnPos   = int(f.readline().split()[1])
    
    # Offset to add so that the lowest marker ID (marker in CSYS0) becomes zero.
    offset = CSYS0__nMarkers + offsetIDs


# Constraints
constrList = []
with open(os.path.join(caseDir,'constr'),'r') as f:
    for line in f:
        constrList.append([int(k12)+offset for k12 in line.split()])


# Rigid bodies
rigidBodies = []
with open(os.path.join(caseDir,'rigids'),'r') as f:
    nRigids = int(f.readline().split()[1])
    for iRB in range(0,nRigids):
        coordsLoc = []
        
        lX = float(f.readline().split()[1])
        lY = float(f.readline().split()[1])
        lZ = float(f.readline().split()[1])
        
        nMarkersRB = int(f.readline().split()[1])
    
        #  Marker positions
        for iMa in range(0,nMarkersRB):
            lineStr = f.readline()
            coordsLoc.append([float(pos_i) for pos_i in lineStr.split()])
        coordsLoc = np.array(coordsLoc).T
        
        body = rb.rigidBody(lX, lY, lZ, coordsLoc)
        rigidBodies.append(body)


# Export data per timestep
coords   = []     # Coordinates of flexible body nodes. Dimension (3,nMarkers)
coordsWS = []     # Coordinates of water surface points. Dimension (3,waterSurfnPos)
poses    = []     # Rigid body poses. Dimension (7,nRigids)
vExp = ve.exporter(vtkDir,nMarkers,CSYS0__nMarkers,constrList,rigidBodies,waterSurfnPos)
for iSt in range(0,nSteps+1):
    if nMarkers>0:
        coords = np.genfromtxt(os.path.join(caseDir,'massp.csv.'+str(iSt))).T
        sigmaN = np.genfromtxt(os.path.join(caseDir,'react.csv.'+str(iSt))).T
    
    if waterSurfnPos>0:
        coordsWS = np.genfromtxt(os.path.join(caseDir,'wsurf.csv.'+str(iSt))).T
    
    if nRigids>0:
        poses = np.genfromtxt(os.path.join(caseDir,'rigpo.csv.'+str(iSt))).T
        
    vExp.writeTimstep(iSt,coords,poses,sigmaN,coordsWS)


# Finalisations
print('Python: Timeseries was succesfully converted to series of vtk files')
