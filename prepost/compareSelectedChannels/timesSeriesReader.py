import numpy as np

sysDirChar = '/'

###############################################################################
# Functions                                                                   #
###############################################################################

# Read specified data__________________________________________________________
# _____________________________________________________________________________
def readTimeseries(dirNames, nSteps, iNode, iCh):
    # Determine number of load cases
    nDirs = len(dirNames)
    
    # initialise lists
    vals = np.zeros((nSteps,nDirs))
    time = np.zeros((nSteps,1))
    for iStep in range(0,nSteps):
        time[iStep] = float(iStep)
    finPos = []
    
    # Iterate over selected load cases
    for iDir in range(0,nDirs):    
        # Extract time history of specified channel in output file
        for iStep in range(0,nSteps):
            # current step file
            fName = dirNames[iDir] + 'massp.csv.' + str(iStep)
            
            rows = np.genfromtxt(fName)
            vals[iStep,iDir] = rows[iNode,iCh]    
        
        finPos.append(rows)
        
    return vals, time, finPos


def stripCaseNames(dirNames):
    caseNames = []
    
    for dName in dirNames:
        lName = len(dName)
        
        # Check for correct string termination by / or \ (OS dependant)
        if dName[lName-1] != sysDirChar:
            print ( 'Name of directory "', dName , '" is not concluded by ',
                    sysDirChar, '"')
            return -1
        
        # Find last / or \ before terminal / or \
        iCh   = lName-2
        while iCh > 0:
            # Return substring containing the name of the case
            if dName[iCh] == sysDirChar:
                caseNames.append(dName[iCh+1:-1])
                break
            iCh -= 1
        else:
            # if no / or \ is found, return the complete path
            caseNames.append(dName)
    
    return caseNames
            