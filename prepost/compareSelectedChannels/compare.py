import matplotlib.pyplot as plt
import timesSeriesReader as tsr
import sys


# Parameters___________________________________________________________________
# _____________________________________________________________________________
# Base name of step files
dirNames = [  './Floaters.LAG/',
              './Floaters.RRF/']

# number of steps
nSteps = 100

# Output node
iNode = 60

# Output channel
iCh = 1


# Initialisations______________________________________________________________
# _____________________________________________________________________________
cNames = tsr.stripCaseNames(dirNames)
if cNames==-1:
    print('--- Terminating script ---')
    sys.exit()

nDirs = len(dirNames)

if iCh == 0:
    title = 'Displacement in $x$-direction'
elif iCh == 1:
    title = 'Displacement in $y$-direction'
elif iCh == 2:
    title = 'Displacement in $z$-direction'

# Read specified channel from list of timeseries
vals, time, finPos = tsr.readTimeseries(dirNames, nSteps, iNode, iCh)


# Plot results_________________________________________________________________
# _____________________________________________________________________________
colormap = ['r','g','b','m']

fig = plt.figure()

# --- Plot selected channel ---
plt.subplot(2,1,1)

for iDir in range(0,nDirs):
    plt.plot(time,vals[:,iDir],'-',linewidth=1,color = colormap[iDir])

plt.legend(cNames, fontsize=14)
plt.title(title, fontsize=14)
plt.xlabel('time')
plt.ylabel('Displacement')


# --- Plot final configuration ---
plt.subplot(2,1,2)
for iDir in range(0,nDirs):
    plt.plot(finPos[iDir][:,1],finPos[iDir][:,2],'-',color = colormap[iDir])

plt.legend(cNames, fontsize=14)
plt.title('Final configuration', fontsize=14)
plt.xlabel('$y$-axis')
plt.ylabel('$z$-axis')

plt.tight_layout()
plt.show()

fig.savefig('compare.pdf')
