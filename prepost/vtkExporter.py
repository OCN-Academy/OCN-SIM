# -*- coding: utf-8 -*-
"""
Created on Fri Oct  7 10:22:51 2016

@author: Christoph Otto, christoph.otto@uni-rostock.de
"""

import vtk
import os
import glob
import math

class exporter:
    """Class for exporting set of points connected by lines to vtk timerseries
    files"""
    
    
    def __init__(self,outDir,nMarkers,nMarkersCSYS0,connections,rigidBodies,waterSurfNPos):
        # --- Copy parameters ---
        self.rigidBodies = rigidBodies
        self.nRigids = len(rigidBodies)
        self.nMarkers = nMarkers
        self.nMarkersCSYS0 = nMarkersCSYS0
        self.waterSurfNPos = waterSurfNPos
        self.outDir = outDir
        
        
        # Flexible bodies
        self.polyData = vtk.vtkPolyData()
        
        self.pts = vtk.vtkPoints()
        self.pts.SetNumberOfPoints(nMarkers)
        self.polyData.SetPoints(self.pts)
        
        # Flexible body internal forces
        self.nConstr = len(connections)
        self.sigmaN = vtk.vtkFloatArray()
        self.sigmaN.SetNumberOfComponents(1)
        self.sigmaN.SetNumberOfTuples(self.nConstr)
        self.sigmaN.SetName('SigmaN')

        self.lines = vtk.vtkCellArray()
        ln = vtk.vtkLine()
        iLine = 0
        for edge in connections:
            iLine += 1
            ln.GetPointIds().SetId(0, edge[0])
            ln.GetPointIds().SetId(1, edge[1])
            self.lines.InsertNextCell(ln)

        self.polyData.GetCellData().AddArray(self.sigmaN)
        self.polyData.SetLines(self.lines)

        self.writerFlex = vtk.vtkXMLPolyDataWriter()
        self.writerFlex.SetDataModeToBinary()
        self.writerFlex.SetInputData(self.polyData)
        
        
        # Water surface
        if waterSurfNPos>0:
            self.polyDataWS = vtk.vtkPolyData()
            
            self.ptsWS = vtk.vtkPoints()
            self.ptsWS.SetNumberOfPoints(waterSurfNPos)
            self.polyDataWS.SetPoints(self.ptsWS)
            
            self.linesWS = vtk.vtkCellArray()
            lnWS = vtk.vtkLine()
            for iPos in range(waterSurfNPos-1):
                lnWS.GetPointIds().SetId(0, iPos)
                lnWS.GetPointIds().SetId(1, iPos+1)
                self.linesWS.InsertNextCell(lnWS)
            
            self.polyDataWS.SetLines(self.linesWS)
            self.writerWS = vtk.vtkXMLPolyDataWriter()
            self.writerWS.SetDataModeToBinary()
            self.writerWS.SetInputData(self.polyDataWS)
        
        
        # Rigid bodies
        if self.nRigids>0:
            self.mergedPolyDataRB = vtk.vtkAppendPolyData()
            self.mergedPolyDataCS = vtk.vtkAppendPolyData()
            self.rigGeom = []; self.rigAxes = []; self.trans   = []; self.transFCS  = []; self.transFRB = []

            for iRB in range(self.nRigids):
                # Transformations
                self.trans.append(vtk.vtkTransform())

                # Spheres
                self.rigGeom.append(vtk.vtkCubeSource())
                self.rigGeom[iRB].SetXLength(rigidBodies[iRB].lX)
                self.rigGeom[iRB].SetYLength(rigidBodies[iRB].lY)
                self.rigGeom[iRB].SetZLength(rigidBodies[iRB].lZ)

                self.transFRB.append(vtk.vtkTransformFilter())
                self.transFRB[iRB].SetTransform(self.trans[iRB])
                self.transFRB[iRB].SetInputConnection(self.rigGeom[iRB].GetOutputPort())
                self.mergedPolyDataRB.AddInputConnection(self.transFRB[iRB].GetOutputPort())

                # Axes
                self.rigAxes.append(vtk.vtkAxes())

                self.transFCS.append(vtk.vtkTransformFilter())
                self.transFCS[iRB].SetTransform(self.trans[iRB])
                self.transFCS[iRB].SetInputConnection(self.rigAxes[iRB].GetOutputPort())
                self.mergedPolyDataCS.AddInputConnection(self.transFCS[iRB].GetOutputPort())

            self.writerRigid = vtk.vtkXMLPolyDataWriter()
            self.writerRigid.SetDataModeToBinary()
            self.writerRigid.SetInputConnection(self.mergedPolyDataRB.GetOutputPort())

            self.writerCS = vtk.vtkXMLPolyDataWriter()
            self.writerCS.SetDataModeToBinary()
            self.writerCS.SetInputConnection(self.mergedPolyDataCS.GetOutputPort())
            
        # Clear output dir
        if not os.path.isdir(self.outDir):
            os.mkdir(self.outDir)
        else:
            files = glob.glob(os.path.join(self.outDir,'*vtp*'))
            for f in files:
                os.remove(f)
    
    
    
    def writeTimstep(self,iStep,coords,poses,sigmaN,coordsWS):
        # Flexible bodies
        self.pts.SetPoint(0,coords[:,0])
        for iPt in range(1,self.nMarkers):
            self.pts.SetPoint(iPt,coords[:,iPt])
            
            #if sigmaN.ndim > 0 and iPt > self.nMarkersCSYS0-1:
            #    self.sigmaN.SetTuple1(iPt-self.nMarkersCSYS0,sig
        
        if sigmaN.ndim > 0:
            for iCo in range(0,self.nConstr):
                self.sigmaN.SetTuple1(iCo,sigmaN[iCo])
        
        
        vtpFBName = os.path.join(self.outDir,'step.vtp.' + str(iStep))
        self.writerFlex.SetFileName(vtpFBName)
        self.writerFlex.Write()
        
        
        # Water surface
        if self.waterSurfNPos>0:
            self.ptsWS.SetPoint(0,coordsWS[:,0])
            for iWS in range(1,self.waterSurfNPos):
                self.ptsWS.SetPoint(iWS,coordsWS[:,iWS])
                
            vtpWSName = os.path.join(self.outDir,'wsurf.vtp.' + str(iStep))
            self.writerWS.SetFileName(vtpWSName)
            self.writerWS.Write()
        
        
        # Rigid bodies        
        if self.nRigids>0:
            for iRB in range(self.nRigids):
                if len(self.rigidBodies) > 1:
                    pose = poses[:,iRB]
                else:
                    pose = poses
                
                self.trans[iRB].Identity()
                self.trans[iRB].Translate(pose[:3])

                if pose[3] != 1.0:
                    phi = math.acos(pose[3])*2.0
                    ux = pose[4]/math.sin(phi/2.0)
                    uy = pose[5]/math.sin(phi/2.0)
                    uz = pose[6]/math.sin(phi/2.0)
                    self.trans[iRB].RotateWXYZ(phi/math.pi*180,ux,uy,uz)
            
            vtpRBName = os.path.join(self.outDir,'rigd.vtp.' + str(iStep))
            self.writerRigid.SetFileName(vtpRBName)
            self.writerRigid.Write()

            vtpCSName = os.path.join(self.outDir, 'csys.vtp.' + str(iStep))
            self.writerCS.SetFileName(vtpCSName)
            self.writerCS.Write()
