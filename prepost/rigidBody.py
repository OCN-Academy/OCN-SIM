# -*- coding: utf-8 -*-
"""
Created on Tue Oct 25 13:39:26 2016

@author: christoph
"""

import numpy as np

class rigidBody:
    
    def __init__(self, lX, lY, lZ, coordsLoc):
        self.lX = lX
        self.lY = lY
        self.lZ = lZ
        
        self.coordsLoc    = coordsLoc
        self.nMarkers     = coordsLoc.shape[1]


def calcTransMat__XYZEulAng(q):
    """Calculate transformation matrix based on XYZ-Euler-angles.
    Compare e.g. https://en.wikipedia.org/wiki/Euler_angles#Rotation_matrix

    Parameters
    ----------
        q -- array-like
            (q)-vector of XYZ-Euler-angles

    Returns
    -------
        T -- numpy.ndarray
            (3x3) transformation matrix
    """
    sa = np.sin(q[0]); ca = np.cos(q[0])    # Sine and cosine of alpha
    sb = np.sin(q[1]); cb = np.cos(q[1])    # Sine and cosine of beta
    sg = np.sin(q[2]); cg = np.cos(q[2])    # Sine and cosine of gamma
    
    T = np.array( [
        [ cb*cg,            -cb*sg,            sb ],
        [ca*sg+sa*sb*cg,    ca*cg-sa*sb*sg, -sa*cb],
        [sa*sg-ca*sb*cg,    sa*cg+ca*sb*sg,  ca*cb]
        
    ])

def calcTransMat__EulPar(p):
    """Calculate transformation matrix based on Euler-parameters.
    
    Parameters
    ----------
        p -- array-like
            (4)-vector of Euler-parameters
    
    Returns
    -------
        T -- numpy.ndarray
            (3x3) transformation matrix
    """
    ps = p[0];
    px = p[1];
    py = p[2];
    pz = p[3];
    
    T = 2.0*np.array([ [ps**2+px**2-0.5, px*py-ps*pz,     px*pz+ps*py],
                       [px*py+ps*pz,     ps**2+py**2-0.5, py*pz-ps*px],
                       [px*pz-ps*py,     py*pz+ps*px,     ps**2+pz**2-0.5] ]);
    
    return T
